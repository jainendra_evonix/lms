<?php require_once("header.php");
	//require_once("addassignjs.php");
	require_once("mainFunctions.php");
	?>
	<nav>
		<ul>
			<?php getAllCoursesList("faculty_assignment.php"); 

	 /** CESAR JUAREZ - OPEN COMET **/

	if ($_GET['selectedCourse']==""){
	  if($_SESSION['courseID'] != "") $_GET['selectedCourse'] = $_SESSION['courseID'];
	} elseif ($_GET['selectedCourse']!=""){
	  $_SESSION['courseID'] = $_GET['selectedCourse'];
	}

?>
		</ul>
	</nav>
	<script>
		var assid=0;
		var option=2;
		function saveAssignment()
		{
			/** CESAR JUAREZ - OPEN COMET **/
			//alert($('#select_topics_assignment').val());
			if($('#assname').val() != "" && $('#assname2').val() != "" && $('#totalmarks').val() != "" && $('#assignno').val() != ""){
				$.ajax({
				url: 'faculty_handler.php?action=saveAssignment&courseid='+$('#course').val()+"&assname="+$('#assname').val()+"&assname2="+$('#assname2').val()+"&totalmarks="+$('#totalmarks').val()+"&assignno="+$('#assignno').val()+"&stopicid="+$('#select_topics_assignment').val(),
				success: function(data) {
					assid=data;

					window.location.replace('faculty_assignment.php');
					//loadAllQuestions();
					
				}
			});
			}else{
				   alert('must complete all fields');
					}
		}
		
		function deleteQuestion(id)
		{
			if(confirm('Do you want to delete the question permanently?')){
			$.ajax({
				url: 'faculty_handler.php?action=deleteQuestion&id='+id,
				success: function(data) {
					loadAllQuestions();
				}
			});
			}
		}
		
		function setAssignment(id)
		{
			assid=id;
			loadAllQuestions();
		}
		
		function updateAssignment()
		{
			/** CESAR JUAREZ - OPEN COMET **/
			
			if($('#assname').val() != "" && $('#assname2').val() != "" && $('#totalmarks').val() != "" && $('#assignno').val() != ""){
			$.ajax({
			  url: 'faculty_handler.php?action=updateAssignment&assid='+$('#assid').val()+'&courseid='+$('#course').val()+"&assname="+$('#assname').val()+"&assname2="+$('#assname2').val()+"&totalmarks="+$('#totalmarks').val()+"&assignno="+$('#assignno').val()+"&assignmentsYear="+$('#assignmentsYear').val()+"&stopicid="+$('#select_topics_assignment').val(),
				success: function(data) {
				  
				  window.location.replace('faculty_assignment.php');
				  //loadAllQuestions();
				  
				}
			});
			}else{
					alert('must complete all fields');
				  }
		}
		
		function loadAllAssignments()
		{
			//alert($("#selectedYear").val())

			var selectedYear = $("#selectedYear").val();

			$.ajax({
				url: 'faculty_handler.php?action=getAssignmentForMenu&selectedCourse=<?=$_GET['selectedCourse']?>&year='+selectedYear,
				success: function(data) {
					$('#leftmenu').html(data);
				}
			});
		}
		
		
		
		function loadAllQuestions()
		{
			$.ajax({
				url: 'faculty_handler.php?action=getQuestionForMenu&AssignmentID='+assid,
				success: function(data) {
					$('#leftmenu').html(data);
					loadQuestionForm(assid);
				}
			});
		}
		
		function editQuestion(id)
		{
			$.ajax({
				url: 'faculty_handler.php?action=editQuestion&questionID='+id+'&AssignmentID='+assid,
				success: function(data) {
					$('#mainform').html(data);
				}
			});
		}
		
		function loadQuestionForm(id)
		{
			$.ajax({
				url: 'faculty_handler.php?action=getQuestionForm&AssignmentID='+assid,
				success: function(data) {
					$('#mainform').html(data);
					option=2;
				}
			});
		}
		
		function loadNewForm()
		{
			$.ajax({
				url: 'faculty_handler.php?action=newAssignmentForm&selectedCourse=<?=$_GET['selectedCourse']?>',
				success: function(data) {
					$('#mainform').html(data);
				}
			});
			loadData();
		}
		
		function loadEditForm(id)
		{
			assid=id;
			$.ajax({
				url: 'faculty_handler.php?action=EditAssignmentForm&selectedCourse=<?=$_GET['selectedCourse']?>&assid='+id,
				success: function(data) {
					$('#mainform').html(data);
				}
			});
		}
		
		function loadDataBatches(id_assignment)
		{ 
			$('#gridcontainer3').html("<table id='list3'></table><div id='pager3'></div>");
				jQuery("#list3").jqGrid({
					url:'faculty_handler.php?action=get_all_active_batches&id_assignment=' + id_assignment,
					datatype: "json",
					colNames:['ID','Batch'],
					colModel:[
						{name:'id',index:'id', width:30},
						{name:'Batch',index:'Name', width:200}
					],
					sortname: 'id',
					loadonce : true,
					viewrecords: true,
					sortorder: "desc",
					multiselect: true,
					altRows: true,	
					width: 400,
					height: 100,
					//rownumbers: true,
					//rownumWidth: 40,
					//caption:"List of Ref. Topics",
					onSelectRow: function (id) 
					{
						$('#batchids').val($('#list3').jqGrid('getGridParam','selarrrow'));
						}
				});
				jQuery("#list3").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false});
		  
		}
		
function carga_topicref()
{
		  $.ajax({
			url: 'faculty_handler.php?action=getrefTopics_assignment&courseid=' + $('#course').val() + '&topicid=' + $('#select_topics_assignment').val(),
				success: function(data) {
				$('#select_reftopic').html(data);
			  }
			});
}

function carga_topicref_existe(id_assignment)
{
  $.ajax({
	url: 'faculty_handler.php?action=getrefTopics_assignment_existe&courseid=' + $('#course').val() + '&topicid=' + $('#select_topics_assignment').val() + '&assignmentid=' + id_assignment,
		success: function(data) {
		$('#select_reftopic').html(data);
	  }
	});
}

function loadData(id_topic, id_assignment)
		{
		  $.ajax({
			url: 'faculty_handler.php?action=getTopics_assignment&courseid=' + $('#course').val(),
				success: function(data) {
				$('#select_topic').html(data);
				$('#select_topics_assignment').val(id_topic);
				//document.forms['form_topics_assignment']['select_topics_assignment'].value = 17;
				if(id_topic && id_assignment)
				  carga_topicref_existe(id_assignment);
				else
				  carga_topicref();
			  }
			});

		  /*
			$('#gridcontainer').html("<table id='list2'></table><div id='pager2'></div>");
				jQuery("#list2").jqGrid({
					url:'faculty_handler.php?action=get_topics_all&courseid='+$('#course').val(),
					datatype: "json",
					colNames:['ID','Heading'],
					colModel:[
						{name:'id',index:'id', width:30},
						{name:'Heading',index:'Name', width:200}
					],
					sortname: 'id',
					loadonce : true,
					viewrecords: true,
					sortorder: "desc",
					multiselect: false,
					altRows: true,	
					width: 400,
					height: 100,
					//rownumbers: true,
					//rownumWidth: 40,
					//caption:"List of Topics",
					loadComplete: function(data) {
						jQuery("#list2").setSelection($('#stopicid').val(),true);
					},
					onSelectRow: function (id) 
					{
						$('#stopicid').val(id);
					}
				});
				jQuery("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false});
				
				$('#gridcontainer1').html("<table id='list3'></table><div id='pager3'></div>");
				jQuery("#list3").jqGrid({
					url:'faculty_handler.php?action=get_topics_all&courseid='+$('#course').val(),
					datatype: "json",
					colNames:['ID','Heading'],
					colModel:[
						{name:'id',index:'id', width:30},
						{name:'Heading',index:'Name', width:200}
					],
					sortname: 'id',
					loadonce : true,
					viewrecords: true,
					sortorder: "desc",
					multiselect: true,
					altRows: true,	
					width: 400,
					height: 100,
					//rownumbers: true,
					//rownumWidth: 40,
					//caption:"List of Ref. Topics",
					loadComplete: function(data) {
						var spl=$('#rtopicid').val().split(",");
						for(i=0;i<spl.length;i++)
						{
							if(spl[i]!="")
							{
								jQuery("#list3").setSelection(spl[i],true);
							}
						}
					},
					onSelectRow: function (id) 
					{
						$('#rtopicid').val($('#list3').jqGrid('getGridParam','selarrrow'));
					}
				});
				jQuery("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false});
				
				if(!$('#edit').val()=="yes")
				{
					$('#stopicid').val('');
					$('#rtopicid').val('');
					}**/
		}
		
		function loadAssignmentSummary()
		{
			$.ajax({
				url: 'faculty_handler.php?action=loadAssignmentSummary&AssignmentID='+assid,
				success: function(data) {
					$('#mainform').html(data);
				}
			});
		}
		
		function BatchForm(id)
		{
			$.ajax({
				url: 'faculty_handler.php?action=loadBatchForm&AssignmentID='+id,
				success: function(data) {
					$('#mainform').html(data);
				}
			});
		}

/** CESAR JUAREZ - OPEN COMET **/
function Load_BatchForm(id){

			$.ajax({
				url: 'faculty_handler.php?action=update_loadBatchForm&AssignmentID='+id,
				success: function(data) {
					$('#mainform').html(data);
				}
			});

}
var loaded=0;
function  update_loadDataBatches(){

	$('#gridcontainer3').html("<table id='list3'></table><div id='pager3'></div>");

	jQuery("#list3").jqGrid({
	  url:'faculty_handler.php?action=get_all_active_batches_update',
		  datatype: "json",
		  colNames:['ID','Name'],
		  colModel:[
					{name:'id',index:'id', width:30},
					{name:'Name',index:'Name', width:100},		
					],
		  rowNum:10,
		  rowList:[10,20,30],
		  pager: '#pager3',
		  sortname: 'id',
		  viewrecords: true,
		  sortorder: "desc",
		  multiselect: true,
		  altRows: true,	
		  width: 500,
		  rownumbers: false,
		  rownumWidth: 40,
		  //caption:"List of Faculties",
		  loadComplete: function(data) {
		  var userdata = $("#list3").getGridParam('userData');
		  if(userdata.selID!=null)
			{
			  var spl=userdata.selID.split("|");
									
			  for(i=0;i<spl.length;i++)
				{
				  if(spl[i]!="")
					{
					  jQuery("#list3").setSelection (spl[i], true);
					}
				}
			}
		  loaded=1;
		},
		  onSelectRow: function (id) 
		{
		  if(loaded!=0)
			{
			  $('#batchids').val($('#list3').jqGrid('getGridParam','selarrrow'));
			}
		},
		  });
	jQuery("#list3").jqGrid('navGrid','#pager3',{edit:false,add:false,del:false});

}

/******************************/
		
		function updateQuestion()
		{
			var str="";
			for(i=1;i<=option;i++)
			{
				var id='#option_'+i;
				var iid='#iscorrect_'+i;
				str+="option_"+i+":"+$(id).val()+";iscorrect_"+i+":"+$(iid).is(':checked')+"~";
			}
			
			 $.post( "./faculty_handler.php?action=updateQuestion", {questionID:$('#quesid').val() ,assid:assid, question:$('#question').val(), marks: $('#marks').val(), totalOption:option, Options: str } , function (data) {
			 	loadAllQuestions();
			 });
		}
		
		function assignBatches()
		{
			//alert($('#assst').val());
			//alert($('#assend').val());
			//alert($('#batchids').val());
			
			$.ajax({
				url: 'faculty_handler.php?action=saveBatches&AssignmentID='+$('#assid').val()+"&batchids="+$('#batchids').val()+"&startdt="+$('#assst').val()+"&enddt="+$('#assend').val(),
				success: function(data) {
					loadAllAssignments();
					loadNewForm();
				}
			});
		}
		
		function saveAddNew()
		{

			var str="";
			for(i=1;i<=option;i++)
			{
				var id='#option_'+i;
				var iid='#iscorrect_'+i;
				str+="option_"+i+":"+$(id).val()+";iscorrect_"+i+":"+$(iid).is(':checked')+"~";
			}
			
			 $.post( "./faculty_handler.php?action=saveQuestion", {assid:assid, question:$('#question').val(), marks: $('#marks').val(), totalOption:option, Options: str } , function (data) {

				 if(!($('#question').val() == "" || $('#marks').val() == "")){
				 loadAllQuestions();
				 }else{
				   alert('must complete all fields');
				 }
			 });
		}
		
		

function cancelAddNew()
{
  //loadAllQuestions();
  //loadAllAssignments();

loadAssignmentSummary();				   
				   $.ajax({
					 url: 'faculty_handler.php?action=getQuestionForMenu&AssignmentID='+assid,
						 success: function(data) {
						 $('#leftmenu').html(data);
					   }
					 });
}
		
		function saveQuestionExit()
		{
			var str="";
			for(i=1;i<=option;i++)
			{
				var id='#option_'+i;
				var iid='#iscorrect_'+i;
				str+="option_"+i+":"+$(id).val()+";iscorrect_"+i+":"+$(iid).is(':checked')+"~";
			}
			
			 $.post( "./faculty_handler.php?action=saveQuestion", {assid:assid, question:$('#question').val(), marks: $('#marks').val(), totalOption:option, Options: str } , function (data) {

				 if(!($('#question').val() == "" || $('#marks').val() == "")){
				   loadAssignmentSummary();				   
				   $.ajax({
					 url: 'faculty_handler.php?action=getQuestionForMenu&AssignmentID='+assid,
						 success: function(data) {
						 $('#leftmenu').html(data);
					   }
					 });
				   
				 }else{
				   alert('must complete all fields');
				 }
			 });
			
			
			
		}
		
		function addanotheroption()
		{
			option=option+1;

			$('#moreOptions').append("<input type='text' name='option_"+option+"' id='option_"+option+"' maxlength='200' style='width: 250px;'' /> <input type='radio' name='iscorrect_option' id='iscorrect_"+option+"' value='1' /> <label id='lbl_"+option+"'>Mark as correct</label> <a class='anchor-style' id='del_"+ option +"' onclick='deleteoption("+option+")'>(-) Delete option </a>");
		
		//	$('#moreOptions').append("<input type='text' name='option_"+option+"' id='option_"+option+"' /> <input type='checkbox' name='iscorrect_"+option+"' id='iscorrect_"+option+"' value='1' /> Mark as correct<br />");
		}

// CESAR JUAREZ - OPEN COMET 

function deleteoption(option_id){
  
  var option_id_eliminar = "option_"+option_id;
var mark_id_eliminar = "iscorrect_"+option_id;
var del_id_eliminar = "del_"+option_id;
var lbl_id_eliminar = "lbl_"+option_id;

question = document.getElementById(option_id_eliminar);
mark_co = document.getElementById(mark_id_eliminar);
del_co = document.getElementById(del_id_eliminar);
lbl = document.getElementById(lbl_id_eliminar);

if(confirm('Are you sure to delete this option?')){
if (!question || !mark_co || !del_co){
  alert("UPS! There is an error...");
} else {
  padre = question.parentNode;
	padre.removeChild(question);

  padre_1 = mark_co.parentNode;
	padre_1.removeChild(mark_co);

  padre_2 = del_co.parentNode;
  padre_2.removeChild(del_co);

padre_3 = lbl.parentNode;
  padre_3.removeChild(lbl);

  option = option-1;

}
  }

}

	</script>
	<div class="yui3-g" style="margin-top:25px;">
		<div id="leftmenu" class="yui3-u-1-5 box-shadow"  style="float:left;">
			<!--<div id="leftmenu" class="verticalContainer" style="height:95%">
			</div>-->
		</div>
		<div  style="width:2%;float:left;"> &nbsp; </div>
		<div id="mainform" name="mainform" class="box-shadow"  style="width:75%;float:left;">
			
		</div>
	</div>
	<script>
		loadAllAssignments();
		loadNewForm();
	</script>
	<script>
			$(document).ready(function(){
				$("li#menu-assignment a").addClass("active");
			});			
		</script>
	<?php
	require_once("footer.php");
?>