<?php

	@session_start();
	error_reporting(E_ALL & ~E_NOTICE);
	require_once("mainFunctions.php");



if(!$_SESSION['login']){?>

		<script>

			window.location='./index.php';

		</script>

<?php

			}

	?>
<!DOCTYPE html>
<!-- saved from url=(0072)http://seantheme.com/color-admin-v1.7/admin/html/ui_tabs_accordions.html -->
<html lang="en"><!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link href="cssep/jquery-ui.min.css" rel="stylesheet">
	<link href="cssep/bootstrap.min.css" rel="stylesheet">
	<link href="cssep/font-awesome.min.css" rel="stylesheet">
	<link href="cssep/animate.min.css" rel="stylesheet">
	<link href="cssep/style.min.css" rel="stylesheet">
	<link href="cssep/style-responsive.min.css" rel="stylesheet">
	<link href="cssep/orange.css" rel="stylesheet" id="theme">
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script async="" src="analytics.js"></script><script src="pace.min.js"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body class=" flat-black pace-done"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade in hide"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-header-fixed page-sidebar-fixed in">
		<!-- begin #header -->
		<div id="header" class="header navbar navbar-default navbar-fixed-top navbar-inverse">
			<!-- begin container-fluid -->
			<div class="container-fluid">
				<!-- begin mobile sidebar expand / collapse button -->
				<div class="navbar-header">
					<a href="#" class="navbar-brand"><span class="navbar-logo"></span> E - Portfolio</a>
					<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- end mobile sidebar expand / collapse button -->
				
				<!-- begin header navigation right -->
				<ul class="nav navbar-nav navbar-right">
					
					<li class="dropdown navbar-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<img src="imagesep/user-13.jpg" alt=""> 
							<span class="hidden-xs">Pooja Mane</span> <b class="caret"></b>
						</a>
						<ul class="dropdown-menu animated fadeInLeft">
							<li class="arrow"></li>
							<li><a href="javascript:;">Edit Profile</a></li>
							
							<li><a href="javascript:;">Setting</a></li>
							<li class="divider"></li>
							<li><a href="javascript:;">Log Out</a></li>
						</ul>
					</li>
				</ul>
				<!-- end header navigation right -->
			</div>
			<!-- end container-fluid -->
		</div>
		<!-- end #header -->
		
		<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar sidebar-grid">
			<!-- begin sidebar scrollbar -->
			<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><div data-scrollbar="true" data-height="100%" style="overflow: hidden; width: auto; height: 100%;">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<div class="image">
							<a href="javascript:;"><img src="imagesep/user-13.jpg" alt=""></a>
						</div>
						<div class="info">
							Pooja Mane
							<small>PGDHHM & PGDCR</small>
						</div>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header">Navigation</li>
					<li class="has-sub active">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="fa fa-laptop"></i>
						    <span>Notice Board</span>
					    </a>
						<ul class="sub-menu">
						    <li><a href="#">New Program</a></li>
						    <li><a href="#">Praesent convallis nibh</a></li>
						</ul>
					</li>
					<li class="has-sub">
						<a href="javascript:;">
							
							<i class="fa fa-inbox"></i> 
							<span>Study Material</span>
						</a>
						<ul class="sub-menu">
						    <li><a href="#">Reference Book</a></li>
						    <li><a href="#">Online Books/Materials</a></li>
						    <li><a href="#">Videos</a></li>
						    <li><a href="#">Model Question Papers</a></li>
						    <li><a href="#">Blog</a></li>
					            <li><a href="#">Project Guidance</a></li>

						</ul>
					</li>
					<li class="">
						<a href="student_assignment.php" target="_blank">
							
							<i class="fa fa-inbox"></i> 
							<span>Assignments</span>
						</a>
					</li>
					<li class="">
						<a href="student_marks.php"  target="_blank">
							
							<i class="fa fa-inbox"></i> 
							<span>Marks</span>
						</a>
					</li>
					
				</ul>
				<!-- end sidebar nav -->
			</div><div class="slimScrollBar ui-draggable" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 245.055650684932px; background: rgb(0, 0, 0);"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div></div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->
		
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Welcome Pooja<small>  &nbsp;this is your portfolio page.......</small></h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
 <div class="col-md-4">
			   <div class="panel panel-inverse" data-sortable-id="ui-media-object-1">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload" data-original-title="" title=""><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            
                            </div>
                            <h4 class="panel-title">Profile Information</h4>
                        </div>
                        <div class="panel-body">
							<div class="media media-sm">
								<a class="media-left" href="javascript:;">
									<img src="imagesep/user-13.jpg" alt="" class="media-object" style="width: 150px;">

								</a>
								<div class="media-body" style="padding-left: 3%">
<br>
								<button type="button" class="btn btn-warning m-r-5 m-b-5" style="width:140px;"><i class="fa fa fa-user"></i> Edit Profile   </button><br><br>
<button type="button" class="btn btn-warning m-r-5 m-b-5"><i class="fa fa-edit"></i>Edit Information</button>						
								</div>
							</div>
							<div class="media media-sm">
								
																
							</div>
							
						</div>
                    </div>
			    </div>
			    <!-- begin col-6 -->
			    <div class="col-md-8">
					<ul class="nav nav-tabs">
						<li class="active"><a href="ui_tabs_accordions.html#default-tab-1" data-toggle="tab" aria-expanded="true">Basic Information</a></li>
						<li class=""><a href="ui_tabs_accordions.html#default-tab-2" data-toggle="tab" aria-expanded="false">Courier Details</a></li>
						<li class=""><a href="ui_tabs_accordions.html#default-tab-3" data-toggle="tab" aria-expanded="false">Other Details</a></li>
					</ul>
					<p style="float:right; margin:10px;" class="btn btn-warning m-r-5 m-b-5"><a href="student_course.php" target="_blank" STYLE="color: #ffffff;text-decoration: none;">Click Here To Go LMS</a></p>
					<br /><br /><br />
					<div class="tab-content">
						<div class="tab-pane fade active in" id="default-tab-1">
						<button type="button" class="btn btn-warning m-r-5 m-b-5" style="float:right;"><i class="fa fa-edit"></i> Edit</button>	
<div class="panel panel-inverse" data-sortable-id="table-basic-1">
                      
                        <div class="panel-body">
                            <table class="table">
                                
                                <tbody>
                                    <tr>
                                       
                                        <td style="border-top: none;">Name</td>
                                        <td style="border-top: none;">Pooja Mane</td>
                                    </tr>
                                    <tr>
                                   
                                        <td>Age</td>
                                        <td>22</td>
                                    </tr>
                                    <tr>
                                        
                                        <td>Qualification</td>
                                        <td>BE Computer</td>
                                    </tr>
                                    <tr>
                                       
                                        <td>Present Employer</td>
                                        <td>TCS</td>
                                    </tr>
<tr>
                                     
                                        <td>Program Enrolled at SCHC</td>
                                        <td>PGDHHM & PGDCR</td>
                                    </tr>
<tr>
                                       
                                        <td>Prospectus No.</td>
                                        <td>1452</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

						</div>
						<div class="tab-pane fade" id="default-tab-2">

<div class="panel panel-inverse" data-sortable-id="table-basic-1">
                      
                        <div class="panel-body">
                            <table class="table">
                             
                                <tbody>
                                    <tr>
                                      
                                        <td style="border-top: none;">Company Name</td>
                                        <td style="border-top: none;">Blue Dart</td>
                                    </tr>
                                    <tr>
                                    
                                        <td>Doc No</td>
                                        <td>22</td>
                                    </tr>
                                    <tr>
                                      
                                        <td>Date</td>
                                        <td>25th March 2015 </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
							
						</div>
						<div class="tab-pane fade" id="default-tab-3">
	
<div class="panel panel-inverse" data-sortable-id="table-basic-1">
                      
                        <div class="panel-body">
                            <table class="table">
                                
                                <tbody>
                                    <tr>
                                     
                                        <td style="border-top: none;">Purchased On</td>
                                        <td style="border-top: none;">25th March 2015 </td>
                                    </tr>
                                    <tr>
                                      
                                        <td>Payment Mode</td>
                                        <td>Online</td>
                                    </tr>
                                    <tr>
                                      
                                        <td>Program Fee Paid </td>
                                        <td>2000 Rs. </td>
                                    </tr>
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>

		
						</div>
					</div>
					
				</div>
			    <!-- end col-6 -->
			    <!-- begin col-6 -->
			 
			    <!-- end col-6 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end #content -->
		
        <!-- begin theme-panel -->
       
        <!-- end theme-panel -->
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="jsep/jquery-1.9.1.min.js"></script>
	<script src="jsep/jquery-migrate-1.1.0.min.js"></script>
	<script src="jsep/jquery-ui.min.js"></script>
	<script src="jsep/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="assets/crossbrowserjs/html5shiv.js"></script>
		<script src="assets/crossbrowserjs/respond.min.js"></script>
		<script src="assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="jsep/jquery.slimscroll.min.js"></script>
	<script src="jsep/jquery.cookie.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="jsep/apps.min.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>
	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-53034621-1', 'auto');
      ga('send', 'pageview');
    </script>


</body></html>
