<?php
session_start();
require_once("./db.php");
require_once("mainFunctions.php");
?>
<html lang="en">
  <head>
  <title id='Description'>jQuery Chart Column Series Example</title>
  <link rel="stylesheet" href="chart/styles/jqx.base.css" type="text/css" />
  <script type="text/javascript" src="js/jquery-1.8.3.js"></script>
  <script type="text/javascript" src="chart/jqxcore.js"></script>
  <script type="text/javascript" src="chart/jqxchart.js"></script>
  <script type="text/javascript" src="chart/jqxdata.js"></script>
  <script type="text/javascript">
  $(document).ready(function () {
		
	  // prepare chart data
	  var  sampleData = [
						 <?php
						 $ser="";
						 $high=0;
						 $batchinformationr=mysql_query("
select * from assignment, assignment_batch 
where assignment.ID = assignment_batch.AssignmentID and 
assignment_batch.BatchID = ". $_SESSION['batchid'] ." and 
assignment.CourseID = ". $_SESSION['courseID'] . " ORDER BY assignment.ID ASC") or die(mysql_error());

						 while($batchinformation=mysql_fetch_array($batchinformationr))
						   {
							 $decide=getAssignmentStartingStringParameter($batchinformation['StartDate'],$batchinformation['EndDate'],$batchinformation['AssignmentID']);
							 if($decide=="Submitted" || $decide=="Expired")
							   {
								 $rs = mysql_query("select * from assignment where ID=".$batchinformation['AssignmentID']);
								 $rs = mysql_fetch_array($rs);
								
								 $rt = mysql_query("select sum(Marks) as marks from question where AssignmentID=".$batchinformation['AssignmentID']);
								 $rt=mysql_fetch_array($rt);
								
								 $obtainM=0;
								 $classAvg=0;
								
								 $per=$rt['marks'];
								
								 if($rt['marks']>$high)
								   {
									 $high=$rt['marks'];
								   }
								
								 $assatemp=mysql_query("select * from assignment_attempted where AssignmentID=".$batchinformation['AssignmentID']." and StudentID=".$_SESSION['userid']);
								 if(mysql_num_rows($assatemp)>0)
								   {
									 $assatemp=mysql_fetch_array($assatemp);
									 $obtainM=$assatemp['MarksObtained'];
									
									 if($obtainM>$high)
									   {
										 $high=$obtainM;
									   }
								   }
								
								 $avg = mysql_query("select avg(MarksObtained) as MO from assignment_attempted where StudentID in (select StudentID from batches_student where BatchID=".$_GET['batchid'].") and AssignmentID=".$batchinformation['AssignmentID']);
								 $avg=mysql_fetch_array($avg);
								 $classAvg=number_format($avg['MO'],0);
								 //echo $classAvg;
								 if($classAvg>$high)
								   {
									 $high=$classAvg;
								   }
								 if ($per=="") $per = 0;
								 $ser.="{ Day:'".$rs['AssignmentName']."', My:".$obtainM.", Class:" . $classAvg . ", Total:".$per."},";
								 //print();
							   }
						   }
						 if(strlen($ser)>=1)
						   {
							 $ser=substr($ser,0,strlen($ser)-1);
						   }
						 print($ser);
						 ?>
					
                    
						 ];
				
	  // prepare jqxChart settings
	  var settings = {
	  title: "",
	  description: "",
	  width: "600px",
	  padding: { left: 20, top: 20, right: 5, bottom: 5 },
	  titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
	  source: sampleData,
	  categoryAxis:
	  {
	  dataField: 'Day',
	  showGridLines: false
	  },
	  colorScheme: 'scheme01',
	  seriesGroups:
	  [
		{
		type: 'spline',
		columnsGapPercent: 30,
		seriesGapPercent: 0,
		valueAxis:
		{
		minValue: 0,
		maxValue: <?=$high+1?>,
		unitInterval: <?=$high/5?>,
		description: 'Scores'
		},
		series: [
		{ dataField: 'My', displayText: 'My'},
		{ dataField: 'Class', displayText: 'Class'},
		{ dataField: 'Total', displayText: 'Total'}
				 ]
		}
	   ]
	  };
            
	  // select the chartContainer DIV element and render the chart.
	  $('#chartContainer').jqxChart(settings);
	});
</script>
</head>
<body style="background:white;">

  <div id='chartContainer' style="width:100%; height: 400px"/>
  </body>
  </html>