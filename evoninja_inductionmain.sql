-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2013 at 01:02 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `evoninja_inductionmain`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

CREATE TABLE IF NOT EXISTS `assignment` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AssignmentName` varchar(255) NOT NULL,
  `TopicID` bigint(20) NOT NULL,
  `CourseID` bigint(20) NOT NULL,
  `EstDuration` varchar(100) NOT NULL,
  `update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `assignment`
--

INSERT INTO `assignment` (`ID`, `AssignmentName`, `TopicID`, `CourseID`, `EstDuration`, `update`, `Enabled`) VALUES
(7, 'Assignment 1', 4, 4, '0:30', '2013-12-23 09:14:23', 1),
(8, 'Assignment 1', 5, 3, '0:15', '2013-12-24 09:28:32', 1),
(9, 'Assignment 2', 5, 3, '0:20', '2013-12-25 10:17:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `assignment_attempted`
--

CREATE TABLE IF NOT EXISTS `assignment_attempted` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AssignmentID` bigint(20) NOT NULL,
  `MarksObtained` bigint(20) NOT NULL,
  `TotalMarks` bigint(20) NOT NULL,
  `CourseID` bigint(20) NOT NULL,
  `StudentID` bigint(20) NOT NULL,
  `assignmentST` datetime NOT NULL,
  `assignmentEN` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=107 ;

--
-- Dumping data for table `assignment_attempted`
--

INSERT INTO `assignment_attempted` (`ID`, `AssignmentID`, `MarksObtained`, `TotalMarks`, `CourseID`, `StudentID`, `assignmentST`, `assignmentEN`) VALUES
(97, 7, 3, 4, 4, 76, '2013-12-24 12:33:16', '2013-12-24 12:34:01'),
(96, 7, 1, 4, 4, 75, '2013-12-24 12:29:48', '2013-12-24 12:30:28'),
(98, 7, 3, 4, 4, 79, '2013-12-24 12:55:51', '2013-12-24 13:03:10'),
(99, 8, 3, 4, 3, 83, '2013-12-24 15:05:22', '2013-12-24 15:20:18'),
(106, 8, 2, 4, 3, 81, '2013-12-25 14:33:08', '2013-12-25 14:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `assignment_attempted_detail`
--

CREATE TABLE IF NOT EXISTS `assignment_attempted_detail` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AssignmentID` bigint(20) NOT NULL,
  `OptionID` varchar(100) NOT NULL,
  `IsCorrect` tinyint(1) NOT NULL,
  `AssignmentAttemptedID` bigint(20) NOT NULL,
  `QuestionID` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `assignment_attempted_detail`
--

INSERT INTO `assignment_attempted_detail` (`ID`, `AssignmentID`, `OptionID`, `IsCorrect`, `AssignmentAttemptedID`, `QuestionID`) VALUES
(12, 7, '183', 1, 96, 21),
(11, 7, '175', 0, 96, 20),
(10, 7, '178', 0, 96, 19),
(9, 7, '154', 0, 96, 18),
(13, 7, '152', 1, 97, 18),
(14, 7, '177', 1, 97, 19),
(15, 7, '173', 0, 97, 20),
(16, 7, '183', 1, 97, 21),
(17, 7, '152', 1, 98, 18),
(18, 7, '177', 1, 98, 19),
(19, 7, '174', 1, 98, 20),
(20, 8, '184', 1, 99, 22),
(21, 8, '189', 1, 99, 23),
(22, 8, '199', 1, 99, 25),
(23, 8, '184', 1, 106, 22),
(24, 8, '207', 0, 106, 24),
(25, 8, '199', 1, 106, 25);

-- --------------------------------------------------------

--
-- Table structure for table `assignment_batch`
--

CREATE TABLE IF NOT EXISTS `assignment_batch` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BatchID` bigint(20) NOT NULL,
  `AssignmentID` bigint(20) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `assignment_batch`
--

INSERT INTO `assignment_batch` (`ID`, `BatchID`, `AssignmentID`, `StartDate`, `EndDate`) VALUES
(13, 4, 7, '2013-12-23 04:53:00', '2013-12-30 13:07:08'),
(14, 3, 8, '2013-12-24 15:03:00', '2013-12-30 15:22:01');

-- --------------------------------------------------------

--
-- Table structure for table `assignment_reftopic`
--

CREATE TABLE IF NOT EXISTS `assignment_reftopic` (
  `AssignmentID` bigint(20) NOT NULL,
  `TopicID` bigint(20) NOT NULL,
  `Enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `batches`
--

CREATE TABLE IF NOT EXISTS `batches` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BatchName` varchar(255) NOT NULL,
  `Enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `batches`
--

INSERT INTO `batches` (`ID`, `BatchName`, `Enabled`) VALUES
(3, 'Induction 2013', 1),
(4, 'Induction 2014', 1);

-- --------------------------------------------------------

--
-- Table structure for table `batches_courses`
--

CREATE TABLE IF NOT EXISTS `batches_courses` (
  `BatchID` bigint(20) NOT NULL,
  `CourseID` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `batches_courses`
--

INSERT INTO `batches_courses` (`BatchID`, `CourseID`) VALUES
(4, 4),
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `batches_student`
--

CREATE TABLE IF NOT EXISTS `batches_student` (
  `BatchID` bigint(20) NOT NULL,
  `StudentID` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `batches_student`
--

INSERT INTO `batches_student` (`BatchID`, `StudentID`) VALUES
(3, 82),
(3, 81),
(3, 80),
(3, 83),
(4, 75),
(4, 76),
(4, 78),
(4, 79);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CourseName` varchar(255) NOT NULL,
  `Enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`ID`, `CourseName`, `Enabled`) VALUES
(3, 'System Analysis & Design', 1),
(4, 'Introduction to IT Systems', 1);

-- --------------------------------------------------------

--
-- Table structure for table `faculty_subject`
--

CREATE TABLE IF NOT EXISTS `faculty_subject` (
  `UserID` bigint(20) NOT NULL,
  `CourseID` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faculty_subject`
--

INSERT INTO `faculty_subject` (`UserID`, `CourseID`) VALUES
(69, 3),
(68, 4);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `QuestionID` bigint(20) NOT NULL,
  `Option` text NOT NULL,
  `IsCorrect` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=208 ;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`ID`, `QuestionID`, `Option`, `IsCorrect`) VALUES
(152, 18, '1', 1),
(153, 18, '2', 0),
(154, 18, '3', 0),
(155, 18, '4', 0),
(179, 19, '4', 0),
(178, 19, '3', 0),
(177, 19, '2', 1),
(176, 19, '1', 0),
(175, 20, '4', 0),
(174, 20, '3', 1),
(173, 20, '2', 0),
(172, 20, '1', 0),
(183, 21, '4', 1),
(182, 21, '3', 0),
(181, 21, '2', 0),
(180, 21, '1', 0),
(184, 22, 'a', 1),
(185, 22, 'b', 0),
(186, 22, 'c', 0),
(187, 22, 'd', 0),
(188, 23, 'a', 0),
(189, 23, 'b', 1),
(190, 23, 'c', 0),
(191, 23, 'd', 0),
(207, 24, 'd', 0),
(206, 24, 'c', 1),
(205, 24, 'b', 0),
(204, 24, 'a', 0),
(196, 25, 'a', 0),
(197, 25, 'b', 0),
(198, 25, 'c', 0),
(199, 25, 'd', 1);

-- --------------------------------------------------------

--
-- Table structure for table `queries`
--

CREATE TABLE IF NOT EXISTS `queries` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Question` text NOT NULL,
  `PosterID` bigint(20) NOT NULL,
  `Reply` text NOT NULL,
  `ReplyerID` bigint(20) NOT NULL,
  `PostDate` datetime NOT NULL,
  `ReplyDate` datetime NOT NULL,
  `CourseID` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AssignmentID` bigint(20) NOT NULL,
  `Question` text NOT NULL,
  `MoreThenOneCorrect` tinyint(1) NOT NULL,
  `Marks` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`ID`, `AssignmentID`, `Question`, `MoreThenOneCorrect`, `Marks`) VALUES
(18, 7, '1', 0, 1),
(19, 7, '2', 0, 1),
(20, 7, '3', 0, 1),
(21, 7, '4', 0, 1),
(22, 8, 'a', 0, 1),
(23, 8, 'b', 0, 1),
(24, 8, 'c', 0, 1),
(25, 8, 'd', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `student_topic_read`
--

CREATE TABLE IF NOT EXISTS `student_topic_read` (
  `UserID` bigint(20) NOT NULL,
  `TopicID` bigint(20) NOT NULL,
  `CourseID` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_topic_read`
--

INSERT INTO `student_topic_read` (`UserID`, `TopicID`, `CourseID`) VALUES
(75, 4, 4),
(78, 4, 4),
(79, 4, 4),
(76, 4, 4),
(83, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Heading` varchar(255) NOT NULL,
  `IsSubTopic` tinyint(1) NOT NULL,
  `TopicID` bigint(20) NOT NULL,
  `ContentData` text NOT NULL,
  `ActivateFrom` datetime NOT NULL,
  `CourseID` bigint(20) NOT NULL,
  `TopicBrief` text NOT NULL,
  `Enabled` tinyint(1) NOT NULL DEFAULT '1',
  `update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`ID`, `Heading`, `IsSubTopic`, `TopicID`, `ContentData`, `ActivateFrom`, `CourseID`, `TopicBrief`, `Enabled`, `update`) VALUES
(4, 'Database', 0, 0, '<p>&lt;p&gt;&lt;strong&gt;&lt;em&gt;Databases and Database Management Systems&lt;/em&gt;&lt;/strong&gt;&lt;/p&gt;</p>\r\n\r\n<p>&lt;p&gt;Adatabase is a collection of related data or facts. A database management system (DBMS) is a software tool that al&amp;shy;lows people to store, access, and process data or facts into useful information&lt;/p&gt;</p>\r\n\r\n<p>&lt;p&gt;Many small database programs that run on per&amp;shy;sonal computers are not referred to as databases or database management programs;instead they are&amp;nbsp;called personal information managers (PIMs), personal organizers, and other names&lt;/p&gt;</p>\r\n\r\n<p>&lt;p&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;A DBMS makes&amp;nbsp;it possible to do many routine tasks that would otherwise be tectious and time-consuming without a computer. For example, a DBMS can:&lt;/span&gt;&lt;/p&gt;</p>\r\n\r\n<p>&lt;p&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;Sort thousands of addresses by ZIP code.&lt;/span&gt;&lt;/p&gt;</p>\r\n\r\n<p>&lt;p&gt;&lt;span style=&quot;line-height: 1.6em;&quot;&gt;Find all records of people who live in a certain state.&lt;/span&gt;&lt;/p&gt;</p>\r\n\r\n<p>&lt;p&gt;Print a list of selected records, such as all real estate listings that closed escrow last month.&lt;/p&gt;</p>\r\n\r\n<p>&lt;p&gt;&lt;img alt=&quot;&quot; src=&quot;/induction/js/CKEditor/kcfinder/./upload/images/Database%20Report.JPG&quot; style=&quot;width: 500px;&quot; /&gt;&lt;/p&gt;</p>\r\n', '2013-12-23 13:06:00', 4, 'Introduction to database', 1, '2013-12-23 07:36:14'),
(5, 'Introduction to System Analysis and Design (SAD)', 0, 0, '<p style="color: rgb(34, 34, 34); font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 13px; line-height: 18px;">System are created to solve Problems. One can think of the systemsapproch as an organised way of dealing with a problem. In&nbsp;<span style="font-size: 0px;">this dynamic world</span>, the subject system analysis and design, mainly deals with the software development activities.<br />\r\n<br />\r\nThis post include:-</p>\r\n\r\n<ul style="padding-right: 2.5em; padding-left: 2.5em; margin: 0.5em 0px; line-height: 18px; color: rgb(34, 34, 34); font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 13px;">\r\n	<li style="padding: 0px; margin: 0px 0px 0.25em;">What is System?</li>\r\n	<li style="padding: 0px; margin: 0px 0px 0.25em;">What are diffrent Phases of System Development Life Cycle?</li>\r\n	<li style="padding: 0px; margin: 0px 0px 0.25em;">What are the component of system analysis?</li>\r\n	<li style="padding: 0px; margin: 0px 0px 0.25em;">What are the component of system designing?</li>\r\n</ul>\r\n\r\n<p style="color: rgb(34, 34, 34); font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 13px; line-height: 18px;">What is System?</p>\r\n\r\n<p style="color: rgb(34, 34, 34); font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 13px; line-height: 18px;">A collection of components that work together to realize some objectives<br />\r\nforms a system. Basically there are three major components in<br />\r\nevery system, namely input, processing and output.</p>\r\n\r\n<p style="color: rgb(34, 34, 34); font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 13px; line-height: 18px;"><span style="color: rgb(34, 34, 34); font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 13px; line-height: 18px;">In a system the different components are connected with each other and they are interdependent. For example, human body represents a complete natural system. We are also bound by many national systems such as political system, economic system, educational system</span><br style="color: rgb(34, 34, 34); font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 13px; line-height: 18px;" />\r\n<span style="color: rgb(34, 34, 34); font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 13px; line-height: 18px;">and so forth. The objective of the system demands that some output is produced as a result of processing the suitable inputs. A well-designed system also includes an additional element referred to as &lsquo;control&rsquo; that provides a feedback to achieve desired objectives</span><br style="color: rgb(34, 34, 34); font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 13px; line-height: 18px;" />\r\n<span style="color: rgb(34, 34, 34); font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 13px; line-height: 18px;">of the system.</span></p>\r\n', '2013-12-24 14:55:00', 3, 'Introduction to System Analysis and Design (SAD)', 1, '2013-12-24 09:25:24');

-- --------------------------------------------------------

--
-- Table structure for table `topics_comments`
--

CREATE TABLE IF NOT EXISTS `topics_comments` (
  `CourseID` bigint(20) NOT NULL,
  `TopicID` bigint(20) NOT NULL,
  `StudentID` bigint(20) NOT NULL,
  `Comments` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(20) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Gender` varchar(255) NOT NULL,
  `EmailID` varchar(255) NOT NULL,
  `ContactNo` varchar(100) NOT NULL,
  `Enabled` tinyint(1) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Role` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `UserID`, `Name`, `Gender`, `EmailID`, `ContactNo`, `Enabled`, `Password`, `Role`) VALUES
(1, 'ADM001', 'Administrator', 'male', 'admin@scit.edu', '123456789', 1, 'PreInd@#45', 'admin'),
(66, 'FAC000', 'Faculty Demo', 'male', 'niladri28@gmail.com', '7620369733', 1, 'faculty1!', 'faculty'),
(68, 'FAC001', 'Prof. Apoorva Kulkarni', 'female', 'apoorva@scit.edu', '9822318356', 1, '80apoo28@', 'faculty'),
(69, 'FAC002', 'Prof. Pradnya Purandare', 'female', 'pradnya@scit.edu', '9890658984', 1, 'preinduction2013', 'faculty'),
(70, 'FAC003', 'Prof. Sanjit Singh', 'male', 'sanjit@scit.edu', '9970470834', 1, 'qwerty19', 'faculty'),
(71, 'FAC004', 'Prof. SVK Bharathi', 'male', 'svkbharathi@scit.edu', '9960131463', 1, 'FacultyPassword', 'faculty'),
(74, 'fac005', 'Divya', 'female', 'divya@scit.edu', '42423', 1, 'div@#$23', 'faculty'),
(75, '75', 'Ram', 'male', 'ram@gmail.com', '9652415263', 1, '75ram@gmail.com', 'student'),
(76, 'STUD002', 'Shyam', 'male', 'shyam@gmail.com', '9663855241', 1, 'STUD002shyam@gmail.com', 'student'),
(77, 'FAC006', 'Prof. Surjit Sharma', 'male', 'surjitsharma@gmail.com', '9652415263', 1, 'FacultyPassword', 'faculty'),
(78, 'STUD003', 'John', 'male', 'john@yahoo.com', '9475848552', 1, 'STUD003john@yahoo.com', 'student'),
(79, 'STUD004', 'Smith', 'male', 'smith@yahoo.com', '9685744152', 1, 'STUD004smith@yahoo.com', 'student'),
(80, 'STUD006', 'b', 'male', 'b@gmail.com', '9856324145', 1, 'STUD006b@gmail.com', 'student'),
(81, 'STUD007', 'c', 'female', 'c@gmail.com', '9865744556', 1, 'STUD007c@gmail.com', 'student'),
(82, 'STUD008', 'd', 'female', 'd@gmail.com', '9652415263', 1, 'STUD008d@gmail.com', 'student'),
(83, 'STUD005', 'a', 'female', 'a@gmail.com', '9652417485', 1, 'STUD005a@gmail.com', 'student');

-- --------------------------------------------------------

--
-- Table structure for table `user_login_logout_status`
--

CREATE TABLE IF NOT EXISTS `user_login_logout_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(20) NOT NULL,
  `activitydate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=297 ;

--
-- Dumping data for table `user_login_logout_status`
--

INSERT INTO `user_login_logout_status` (`id`, `userid`, `activitydate`, `status`) VALUES
(228, '75', '2013-12-23 07:10:27', 1),
(229, '1', '2013-12-23 07:11:38', 1),
(230, '69', '2013-12-23 07:15:29', 1),
(231, '69', '2013-12-23 07:33:45', 0),
(232, '68', '2013-12-23 07:34:11', 1),
(233, '68', '2013-12-23 08:33:17', 0),
(234, '68', '2013-12-23 09:13:34', 1),
(235, '75', '2013-12-23 11:52:29', 0),
(236, '75', '2013-12-23 11:52:42', 1),
(237, '75', '2013-12-23 11:57:43', 0),
(238, '75', '2013-12-23 11:57:56', 1),
(239, '75', '2013-12-23 11:58:36', 0),
(240, '75', '2013-12-23 12:01:24', 1),
(241, '', '2013-12-23 14:14:13', 0),
(242, '68', '2013-12-23 14:15:24', 0),
(243, '68', '2013-12-23 14:15:35', 1),
(244, '75', '2013-12-23 15:02:51', 0),
(245, '68', '2013-12-23 15:03:08', 0),
(246, '75', '2013-12-24 04:52:23', 1),
(247, '68', '2013-12-24 04:52:43', 1),
(248, '75', '2013-12-24 06:11:35', 0),
(249, '78', '2013-12-24 06:11:58', 1),
(250, '78', '2013-12-24 06:57:53', 0),
(251, '79', '2013-12-24 06:58:13', 1),
(252, '79', '2013-12-24 06:58:46', 0),
(253, '75', '2013-12-24 06:59:35', 1),
(254, '75', '2013-12-24 07:02:22', 0),
(255, '76', '2013-12-24 07:02:48', 1),
(256, '76', '2013-12-24 07:19:44', 0),
(257, '79', '2013-12-24 07:20:09', 1),
(258, '228', '2013-12-24 09:05:22', 0),
(259, '69', '2013-12-24 09:05:57', 1),
(260, '83', '2013-12-24 09:06:35', 1),
(261, '69', '2013-12-24 09:55:04', 0),
(262, '68', '2013-12-24 09:55:27', 1),
(263, '83', '2013-12-24 09:55:31', 0),
(264, '76', '2013-12-24 09:55:46', 1),
(265, '76', '2013-12-24 11:07:50', 0),
(266, '68', '2013-12-24 11:07:53', 0),
(267, '75', '2013-12-24 11:10:04', 1),
(268, '68', '2013-12-24 11:10:19', 1),
(269, '75', '2013-12-25 05:45:34', 0),
(270, '75', '2013-12-25 05:45:47', 1),
(271, '75', '2013-12-25 06:37:40', 0),
(272, '75', '2013-12-25 06:38:11', 1),
(273, '75', '2013-12-25 06:38:25', 1),
(274, '75', '2013-12-25 06:48:28', 0),
(275, '75', '2013-12-25 06:48:59', 1),
(276, '75', '2013-12-25 06:52:01', 0),
(277, '75', '2013-12-25 06:52:17', 1),
(278, '75', '2013-12-25 06:53:27', 0),
(279, '75', '2013-12-25 06:53:42', 1),
(280, '75', '2013-12-25 06:55:36', 0),
(281, '75', '2013-12-25 06:55:59', 1),
(282, '75', '2013-12-25 06:56:50', 0),
(283, '75', '2013-12-25 06:57:10', 1),
(284, '75', '2013-12-25 06:59:02', 0),
(285, '75', '2013-12-25 06:59:26', 1),
(286, '75', '2013-12-25 07:03:09', 0),
(287, '75', '2013-12-25 07:03:20', 1),
(288, '75', '2013-12-25 07:20:53', 0),
(289, '75', '2013-12-25 07:21:14', 1),
(290, '75', '2013-12-25 07:25:55', 0),
(291, '75', '2013-12-25 07:27:37', 1),
(292, '75', '2013-12-25 08:37:18', 0),
(293, '81', '2013-12-25 08:37:32', 1),
(294, '81', '2013-12-25 10:10:43', 0),
(295, '69', '2013-12-25 10:11:18', 1),
(296, '1', '2013-12-25 10:15:52', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
