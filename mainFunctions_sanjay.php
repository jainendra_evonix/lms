﻿<?php require_once("./db.php");
function normalize_str($str)
{
$invalid = array('Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z',
'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A',
'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E',
'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y',
'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a',
'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e',  'ë'=>'e', 'ì'=>'i', 'í'=>'i',
'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y',  'ý'=>'y', 'þ'=>'b',
'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', "`" => "'", "´" => "'", "„" => ",", "`" => "'",
"´" => "'", "“" => "\"", "”" => "\"", "´" => "'", "&acirc;€™" => "'","&Acirc;" => "",
"{" => "", "~" => "", "–" => "-", "’" => "'");
 
$str = str_replace(array_keys($invalid), array_values($invalid), $str);
 
return $str;
}
function converToTz($time="",$toTz='',$fromTz='')
	{	
		// timezone by php friendly values
		$date = new DateTime($time, new DateTimeZone($fromTz));
		$date->setTimezone(new DateTimeZone($toTz));
		$time= $date->format('Y-m-d H:i:s');
		return $time;
	}

function addEscapeSeq($str)
{	$formatString = $str;
		
	if (!get_magic_quotes_gpc())
	{
		$formatString = addslashes($str);
	}
	
	return $formatString;
}

function stripEscapeSeq($str)
{	$formatString = $str;
		
	if (get_magic_quotes_gpc())
	{
		$formatString = stripslashes($str);
	}
	
	return $formatString;
}
function get_student_recent_course()
{
	$coursid="";
	$courseinfors=mysql_query("SELECT DISTINCT (batches_courses.CourseID) as CID FROM batches_student INNER JOIN batches_courses ON batches_student.BatchID = batches_courses.BatchID WHERE batches_student.StudentID = ".$_SESSION['userid']);
	while($courseinfo = mysql_fetch_array($courseinfors))
	{
		$coursid.=$courseinfo['CID'].",";
	}
	
	if(strlen($coursid)>=1)
	{
		$coursid=substr($coursid,0,strlen($coursid)-1);
	}
	
	return $coursid;
}
function limit_text($text, $limit) {
  if (str_word_count($text, 0) > $limit) {
	  $words = str_word_count($text, 2);
	  $pos = array_keys($words);
	  $text = substr($text, 0, $pos[$limit]) . '...';
  }
  return $text;
}

function get_Admin_Batches()
{
  $rec = mysql_query("select count(*) as totalBatch from batches where Enabled='1'") or die(mysql_error());
  $rec = mysql_fetch_array($rec);
  return $rec['totalBatch'];
			 
}
		
function get_Recent_Assignment_Admin()
{
  //$cr = mysql_query("select * from assignment inner join course on course.id=topics.CourseID  order by ID desc LIMIT 0 , 5");
  $cr = mysql_query("select assignment.*, course.CourseName from assignment inner join course on course.id= assignment.CourseID  order by ID desc LIMIT 0 , 5");
  ?>
  <table width="100%">
	 <?php
	while($c=mysql_fetch_array($cr))
	  {
		?>
				
		<tr class="ui-widget-content" valign="middle">
		<td height="35px" width="40%" style="padding:5px;vertical-align:middle;">
		<?=$c['AssignmentName']?>
		</td>
		<td class="grid-small-text" width="18%">
		<?=$c['CourseName']?>
		</td>
		<td class="grid-small-text" width="22%">Last Modified<br/>
		<?php if($c['update']!="0000-00-00 00:00:00") { print(date("d M, Y",strtotime($c['update']))); }?>
		</td>
		</tr>
		<?php
	  }
  ?>
  </table>
	  <?php
			
	  }
		
		
function get_Recent_Topics_Admin()
{
  //$cr = mysql_query("select * from topics inner join course on course.id=topics.CourseID order by topics.ID desc LIMIT 0 , 5");
  $cr = mysql_query("select * from topics inner join course on course.id=topics.CourseID where topics.Enabled = 1  order by topics.ID desc LIMIT 0 , 5");
  ?>
  <table width="100%">
	 <?php
	while($c=mysql_fetch_array($cr))
	  {
		?>
		<tr style="border-bottom: #ddd 1px solid;">
		<td height="35px" width="40%" style="padding:5px;vertical-align:middle;">
		<?=$c['Heading']?>
		</td>
		<td class="grid-small-text" width="18%">
		<?=$c['CourseName']?>
		</td>
		<td class="grid-small-text" width="22%">Last Modified<br/>
		<?php if($c['update']!="0000-00-00 00:00:00") { print(date("d M, Y",strtotime($c['update']))); }?>
		</td>
		</tr>
		<?php
	  }
  ?>
  </table>
	  <?php
			
	  }
		
function get_Recent_Assignment_Faculty()
{
  $cr = mysql_query("select * from assignment inner join course on course.ID=assignment.CourseID where CourseID in (select course.ID from faculty_subject inner join course on course.ID=faculty_subject.CourseID where faculty_subject.UserID=".$_SESSION['userid']." and course.Enabled='1') order by assignment.ID desc LIMIT 0 , 5");
  ?>
  <table width="100%">
	 <?php
	while($c=mysql_fetch_array($cr))
	  {
		?>
		<tr class="ui-widget-content" valign="middle">
		<td height="35px" width="50%" style="padding:5px;vertical-align:middle;">
		<?=$c['AssignmentName']?>
		</td>
		<td class="grid-small-text" width="25%">
		<?=$c['CourseName']?>
		</td>
		<td class="grid-small-text" width="22%">Last Modified <br/><!-- Cesar Juarez - OPEN COMET -->
		<?php if($c['update']!="0000-00-00 00:00:00") { print(date("d M, Y",strtotime($c['update']))); }?>
		</td>
		<!--<td class="grid-small-text" width="18%">
		<a href="./faculty_course.php?action=edit&selectedCourse=<?=$c['CourseID']?>&topicid=<?=$c['TopicID']?>" style="text-decoration:none;"><div class="grid-button-edit green-button">EDIT</div></a>
		</td>-->
		</tr>
		<?php
	  }
  ?>
  </table>
	  <?php
			
	  }
		
function get_Subject_Name($id)
{
	$cr = mysql_query("select CourseName from course where id=".$id);
	
	if(mysql_num_rows($cr) > 0){
		while($c=mysql_fetch_array($cr))
		{
			print $c["CourseName"]." - ";
		}
	}
	
}	
function get_Recent_Topics_Faculty()
{
  //$cr = mysql_query("select topics.*,course.CourseName from topics inner join course on course.id=topics.CourseID where topics.CourseID in (select course.ID from faculty_subject inner join course on course.ID=faculty_subject.CourseID where faculty_subject.UserID=".$_SESSION['userid']." and course.Enabled='1') order by topics.ID desc LIMIT 0 , 5");
  $cr = mysql_query("select topics.*,course.CourseName from topics inner join course on course.id=topics.CourseID where topics.Enabled=1 and topics.CourseID in (select course.ID from faculty_subject inner join course on course.ID=faculty_subject.CourseID where faculty_subject.UserID=".$_SESSION['userid']." and course.Enabled='1') order by topics.ID desc LIMIT 0 , 5");
  ?>
  <table width="100%">
	 <?php
	while($c=mysql_fetch_array($cr))
	  {
		?>
		<tr style="border-bottom: #ddd 1px solid;">
		<td height="35px" width="40%" style="padding:5px;vertical-align:middle;">
		<?=$c['Heading']?>
		</td>
		<td class="grid-small-text" width="18%">
		<?=$c['CourseName']?>
		</td>
		<td class="grid-small-text" width="22%">Last Modified <br /> <!-- CESAR JUAREZ - OPEN COMET -->
		<?php if($c['update']!="0000-00-00 00:00:00") { print(date("d M, Y",strtotime($c['update']))); }?>
		</td>
		<td class="grid-small-text" width="18%">
		<a href="./faculty_course.php?action=edit&selectedCourse=<?=$c['CourseID']?>&topicid=<?=$c['ID']?>" style="text-decoration:none;"><div class="grid-button-edit green-button">EDIT</div></a>
		</td>
		</tr>
		<?php
	  }
  ?>
  </table>
	  <?php
			
	  }
		
function printDaysToGo($startTimestamp,$endTimestamp)
{
  $diff=$endTimestamp-$startTimestamp;
			
  $w = intval($diff / 86400 / 7);
  $d = intval($diff / 86400 % 7);
  $h = intval($diff / 3600 % 24);
  $m = intval($diff / 60 % 60); 
  $s = intval($diff % 60);
			
  if($w>0)
	{
	  if(strlen($w)<=1)
		{
		  print("0");
		}
	  print($w."<br>WEEK");
	}
  else if($d>0)
	{
	  if(strlen($d)<=1)
		{
		  print("0");
		}
	  print($d."<br>DAY");
	}
  else if($h>0)
	{
	  if(strlen($h)<=1)
		{
		  print("0");
		}
	  print($h."<br>HRS");
	}
  else if($m>0)
	{
	  if(strlen($m)<=1)
		{
		  print("0");
		}
	  print($m."<br>MIN");
	}
  else
	{
	  if(strlen($s)<=1)
		{
		  print("0");
		}
	  print($s."<br>SEC");
	}
}
		
function getAllCoursesList($page) /** CESAR JUÁREZ DÍAZ - GRUPO COMET**/
{
  if ($_SESSION['userrole']=='student'){
  
  	$courses=mysql_query("SELECT DISTINCT (batches_courses.CourseID) as ID, (course.CourseName) as CourseName FROM batches_student INNER JOIN batches_courses ON batches_student.BatchID = batches_courses.BatchID inner join course on course.ID = batches_courses.CourseID WHERE batches_student.StudentID = ".$_SESSION['userid']);
  
	/*$courses=mysql_query("select * from course 
	inner join batches_courses on batches_courses.CourseID = course.ID 
	where Enabled=1 and BatchID = " . $_SESSION['batchid']);*/
  }elseif($_SESSION['userrole']=='faculty'){
	$courses=mysql_query("select * from course 
	inner join faculty_subject on faculty_subject.CourseID = course.ID	
	where Enabled=1 and UserID = " . $_SESSION['userid']);
  }
  while($course=mysql_fetch_array($courses))
	{
	  print("<li style='border-bottom:1px solid #ddd; padding:3px;margin-top:5px;'><a style='line-height:1.5em !important; text-indent:0px !important' href=./".$page."?selectedCourse=".$course['ID'].">".$course['CourseName']."</a></li>");
	}
}

function getAllCoursesListInDiv($page) /** CESAR JUÁREZ DÍAZ - GRUPO COMET**/
{
  if ($_SESSION['userrole']=='student'){
  
  	$courses=mysql_query("SELECT DISTINCT (batches_courses.CourseID) as ID, (course.CourseName) as CourseName FROM batches_student INNER JOIN batches_courses ON batches_student.BatchID = batches_courses.BatchID inner join course on course.ID = batches_courses.CourseID WHERE batches_student.StudentID = ".$_SESSION['userid']);
  
	/*$courses=mysql_query("select * from course 
	inner join batches_courses on batches_courses.CourseID = course.ID 
	where Enabled=1 and BatchID = " . $_SESSION['batchid']);*/
  }elseif($_SESSION['userrole']=='faculty'){
	$courses=mysql_query("select * from course 
	inner join faculty_subject on faculty_subject.CourseID = course.ID	
	where Enabled=1 and UserID = " . $_SESSION['userid']);
  }
  while($course=mysql_fetch_array($courses))
	{
	  /*print("<li style='border-bottom:1px solid #ddd; padding:3px;margin-top:5px;'><a style='line-height:1.5em !important; text-indent:0px !important' href=./".$page."?selectedCourse=".$course['ID'].">".$course['CourseName']."</a></li>");*/
	 ?>
	  <tr>
			<td style="border-bottom: #ddd 1px solid;   padding-left: 25px; font-size: 20pt;">
				<a class="list-link" style=" color: #3399FF !important; cursor:text;" href="#"><div width="100%" style='padding: 5px 15px;'><?=$course['CourseName']?></div></a>
			</td>
			<td style="border-bottom: #ddd 1px solid;width: 15%;padding: 5px 5px;">
				<a href='<?php echo $page;?>?selectedCourse=<?php echo $course['ID']; ?>' style='cursor:pointer; text-decoration:none;'>

					<div class='grid-button-edit green-button' style='width: 225px; padding: 5px 0px 2px 5px; height: 20px; text-align: center;'>
					<?php
					if($page == 'student_assignment.php')
					{
						echo 'Click Here To Assignment List';
					}
					else if($page == 'student_marks.php')
					{
						echo 'Click Here To See Marks';
					}
					?>
					</div>

				</a>
			</td>
		</tr><?php
	}
}
		
function getAssignmentStartingString($startingDate,$endingDate, $assid,$server_time_zone_text,$application_time_zone_text)
{
  $startingtime = strtotime($startingDate);
  
  $endingtime = strtotime($endingDate);
  $currenttime = strtotime(converToTz(date('Y-m-d H:i:s'),$application_time_zone_text,$server_time_zone_text));		
  $submitted = mysql_query("select * from assignment_attempted where AssignmentID=".$assid." and StudentID=".$_SESSION['userid']);

  if($currenttime>=$startingtime && $currenttime<=$endingtime && (!mysql_num_rows($submitted)>=1)) /** CESAR JUAREZ - GRUPO COMET **/
	{
	  //tiempo_restante = remaining time
	  $tiempo_restante = $endingtime - $currenttime;
	  //$asd = converToTz(date('Y-m-d H:i'),'Asia/Kolkata','America/Denver')."<br>";
	  date_default_timezone_set($application_time_zone_text);
	$asd = date('Y-m-d H:i:s');
	//echo "$asd<br>";
	//echo "$endingDate<br>";
	//echo "$currenttime<br>";
	//echo "$tiempo_restante<br>";
	
	$date1=date_create($asd);
	$date2=date_create($endingDate);
	$diff=date_diff($date1,$date2);
	//echo $diff->format("%a");
	if($diff->format("%a")=="0")
		echo $diff->format("%H hrs : %I mins");
	else
		echo $diff->format("%a days");
	  
	  if (date('d',$tiempo_restante) == "01"){
		//echo date('H:i',$tiempo_restante) . "<br/>HRS";
	  }else{
		//echo date('d',$tiempo_restante) . "<br/>DAY";
	  }
	  //print("Start Exam"); 
	}
  else if($currenttime<$startingtime)
	{
	  printDaysToGo($currenttime,$startingtime);
	}
  else
	{
	  $submitted = mysql_query("select * from assignment_attempted where AssignmentID=".$assid." and StudentID=".$_SESSION['userid']);
	  if(mysql_num_rows($submitted)>=1)
		{
		  ?>
		  <img src="images/tick.png" width="16" height="16" />
			<?php
			}
	  else
		{
		  ?>
		  <img src="images/wrong.png" width="16" height="16" />
			<?php
			}
	}
  //print ("Starting ".$startingtime.", Ending ".$endingtime.", Current ".$currenttime);
}
		
function printDaysToGoParameter($startTimestamp,$endTimestamp)
{
  $diff=$endTimestamp-$startTimestamp;
			
  $w = intval($diff / 86400 / 7);
  $d = intval($diff / 86400 % 7);
  $h = intval($diff / 3600 % 24);
  $m = intval($diff / 60 % 60); 
  $s = intval($diff % 60);
			
  $rtn="";
			
  if($w>0)
	{
	  if(strlen($w)<=1)
		{
		  $rtn = "0";
		}
	  $rtn .= $w."<br>WEEK";
	}
  else if($d>0)
	{
	  if(strlen($d)<=1)
		{
		  $rtn ="0";
		}
	  $rtn .=$d."<br>DAY";
	}
  else if($h>0)
	{
	  if(strlen($h)<=1)
		{
		  $rtn ="0";
		}
	  $rtn .=$h."<br>HRS";
	}
  else if($m>0)
	{
	  if(strlen($m)<=1)
		{
		  $rtn ="0";
		}
	  $rtn .=$m."<br>MIN";
	}
  else
	{
	  if(strlen($s)<=1)
		{
		  $rtn ="0";
		}
	  $rtn .=$s."<br>SEC";
	}
			
  return $rtn;
}
		
function getAssignmentStartingStringParameter($startingDate,$endingDate, $assid, $server_time_zone_text, $application_time_zone_text)
{
  $startingtime = strtotime($startingDate);
  $endingtime = strtotime($endingDate);
			
  $currenttime = strtotime(converToTz(date('Y-m-d H:i:s'),$application_time_zone_text,$server_time_zone_text));
  
  //$submitted = mysql_query("select * from assignment_attempted where AssignmentID=".$assid." and StudentID=".$_SESSION['userid']);
  $submitted = mysql_query("select * from student_marks where AssignmentID=".$assid." and StudID=".$_SESSION['userid']);
		
  if($currenttime>=$startingtime && $currenttime<=$endingtime && (!mysql_num_rows($submitted)>=1))
	{
	  return "Started";
	}
  else if($currenttime<$startingtime)
	{
	  return printDaysToGoParameter($currenttime,$startingtime);
	}
  else
	{
	  $submitted = mysql_query("select * from assignment_attempted where AssignmentID=".$assid." and StudentID=".$_SESSION['userid']);
	  if(mysql_num_rows($submitted)>=1)
		{
		  return "Submitted";
		}
	  else
		{
		  return "Expired";
		}
	}
			
}
		
function getAssignmentMarks($assid)
{
  $rs=mysql_query("select * from student_marks where AssignmentID=".$assid." and StudID=".$_SESSION['userid']);
  //echo "select * from assignment_attempted where AssignmentID=".$assid." and StudentID=".$_SESSION['userid'];
  if(mysql_num_rows($rs)==0)
	{
	  print("0");
	}
  else
	{
	  $rs=mysql_fetch_array($rs);
	  print($rs['Marks']);
	}
}

function get_Courses_Assignment_Select($courseId)
{
  $recs = mysql_query("SELECT ID,AssignmentName FROM assignment where CourseID=".$courseId." and Enabled=1") or die(mysql_error());
	?>
	<option value="-1">Choose Assignment</option>
  <?php
  while($rec=mysql_fetch_array($recs))
	{
	  ?>
	  <option value="<?=$rec['ID']?>"><?=$rec['AssignmentName']?></option>
					
		<?php
		}
}		
function get_Batches_Assigned_Select($assignmentid)
{
  $recs = mysql_query("SELECT b.ID, b.BatchName FROM batches b inner join assignment_batch ab on b.id=ab.batchid where ab.AssignmentId=".$assignmentid) or die(mysql_error());
	?>
	<option value="-1">Choose Batch</option>
  <?php
  while($rec=mysql_fetch_array($recs))
	{
	  ?>
	  <option value="<?=$rec['ID']?>"><?=$rec['BatchName']?></option>
					
		<?php
		}
}
function get_Batches_Assigned_Select_Result($assignmentid)
{
  $recs = mysql_query("SELECT b.ID, b.BatchName FROM batches b inner join assignment_batch ab on b.id=ab.batchid where ab.AssignmentId=".$assignmentid) or die(mysql_error());
	?>
  <?php
  while($rec=mysql_fetch_array($recs))
	{
		$recid = $rec['ID'];
		echo "<input type='radio' name='AssignmentName' id='$recid' onclick='getBatchMarksResult($assignmentid,$recid)' />".$rec['BatchName'];
	?>
	<?php
	}?>
	<?php
}
function get_Batch_Marks_Select($assignmentid,$batchid)
{
  /*$recs = mysql_query("SELECT u.name, bs.batchid, u.userid, u.id,ifnull(aa.assignmentid,0) assignmentid, aa.marksobtained FROM batches_student bs inner join user u on bs.studentid=u.id left outer join assignment_attempted aa on u.id=aa.studentid and aa.assignmentid = ".$assignmentid." where bs.batchid=".$batchid) or die(mysql_error());*/
  
  $recs = mysql_query("SELECT student_marks.*, user.Name from student_marks INNER JOIN user ON student_marks.StudID = user.ID where student_marks.AssignmentID = ".$assignmentid." and student_marks.BatchID = ".$batchid);
?>
<table width="100%" align="center" border="1">
<tr>
	<th>Student Id</th>
	<th>Name</th>
	<th>Marks</th>
	<th>&nbsp;</th>
</tr>
<?php  
	while($rec=mysql_fetch_array($recs))
	{
	  ?>
	<tr>
		<td style="width:15%;text-align:center;padding:2px;">
			<?=$rec['StudID']?><input type="hidden" id="studid" value="<?=$rec['StudID']?>" />
		</td>
		<td style="padding:2px;">
			<?=$rec['Name']?><a href="<?=$rec['UploadedAssignment']?>" target="blank" style="float:right;">Download PDF</a>
		</td>
		<?php
			//$marks = mysql_query("SELECT * from student_marks where StudID = '".$rec["StudID"]."'") or die(mysql_error());
			//$rmarks=mysql_fetch_array($marks);
		?>
		<td style="width:20%;text-align:center;padding:2px;">
			<?php if(is_null($rec['Marks']))
					print "Not Attempted";
				else
					/*echo '<a class="fancybox fancybox.ajax btn btn-mini " href="checkstudentanswers.php?val1='.$assignmentid.'&val2='.$rec['id'].'">' . ($rec['Marks']) . '</a>';*/
					echo '<input type="text" id="studmarks_'.$rec["StudID"].'" value="'.$rec['Marks'].'">';
					?><?php
			?>
		</td>
		<td style="width:10%;padding:2px;">
		<?php if(is_null($rec['Marks']))
				{
					print " ";
				}
				else
				{
				?>
					<input id="btnFilter" type="button" value="Submit" onclick="submitBatchMarks(<?php echo $rec["StudID"]; ?>);" class="grid-button-edit yellow-button" style="cursor:pointer;"/><?php
			}
			?>
		</td>
	</tr>
		<?php
		}
		?>
	</table>		
		<?php
}
function get_Batch_Marks_Select_Result($assignmentid,$batchid)
{
  /*$recs = mysql_query("SELECT u.name, bs.batchid, u.userid, u.id,ifnull(aa.assignmentid,0) assignmentid, aa.marksobtained FROM batches_student bs inner join user u on bs.studentid=u.id left outer join assignment_attempted aa on u.id=aa.studentid and aa.assignmentid = ".$assignmentid." where bs.batchid=".$batchid) or die(mysql_error());*/
  
  $recsquery = "SELECT student_marks.*, user.Name, user.Role from student_marks INNER JOIN user ON student_marks.StudID = user.ID where user.Role = 'student' and student_marks.AssignmentID = ".$assignmentid." and student_marks.BatchID = ".$batchid;
  
  /*if($studid != '')
  {
	 $recsquery .= " and student_marks.StudID = ".$studid;
  }*/
  //echo "$recsquery";
  $recs = mysql_query($recsquery);
  
?>
<table width="100%" align="center" border="0">
	<tr>
		<td style="width:25%;">
			ID: <input type="text" id="studid" name="studid" size="12" />
		</td>
		<td style="width:25%;">
			Name: <input type="text" id="studname" name="studname" size="12" />
		</td>
		<td style="width:20%;">
			<select name='studstatus' id='studstatus'>
				<option value='1'>Submitted</option>
				<option value='0'>Not Submitted</option>
				<option value='2'>Both</option>
			</select>
		</td>
		<td style="width:20%;">
			<select name='studmarks' id='studmarks'>
				<option value='1'>Marks Given</option>
				<option value='0'>Marks Not Given</option>
				<option value='2'>Both</option>
			</select>
		</td>
		<td style="width:10%;">
			<input id="" type="button" value="Filter" onclick="getBatchMarksResultFilter(<?php echo $assignmentid; ?>,<?php echo $batchid; ?>)" class="" style="cursor:pointer;">
		</td>
	</tr>
</table><br>
<!-- before filter -->
<!--<table width="100%" align="center" border="1">
<tr>
	<th>Student Id</th>
	<th>Name</th>
	<th>Marks</th>
	<th>&nbsp;</th>
</tr>
<?php  
	while($rec=mysql_fetch_array($recs))
	{
	  ?>
	<tr>
		<td style="width:15%;text-align:center;padding:2px;">
			<?=$rec['StudID']?><input type="hidden" id="studid" value="<?=$rec['StudID']?>" />
		</td>
		<td style="padding:2px;">
			<?=$rec['Name']?><a href="<?=$rec['UploadedAssignment']?>" target="blank" style="float:right;">Download PDF</a>
		</td>
		<?php
			//$marks = mysql_query("SELECT * from student_marks where StudID = '".$rec["StudID"]."'") or die(mysql_error());
			//$rmarks=mysql_fetch_array($marks);
		?>
		<td style="width:20%;text-align:center;padding:2px;">
			<?php if(is_null($rec['Marks']))
			{
					print "Not Attempted";
			}
				else
			{
					/*echo '<a class="fancybox fancybox.ajax btn btn-mini " href="checkstudentanswers.php?val1='.$assignmentid.'&val2='.$rec['id'].'">' . ($rec['Marks']) . '</a>';*/
					echo '<input type="text" id="studmarks_'.$rec["StudID"].'" value="'.$rec['Marks'].'">';
			}
					?><?php
			?>
		</td>
		<td style="width:10%;padding:2px;">
		<?php if(is_null($rec['Marks']))
				{
					print " ";
				}
				else
				{
				?>
					<input id="btnFilter" type="button" value="Submit" onclick="submitBatchMarks(<?php echo $rec["StudID"]; ?>,<?php echo $assignmentid; ?>,<?php echo $batchid; ?>);" class="grid-button-edit yellow-button" style="cursor:pointer;"/><?php
			}
			?>
		</td>
	</tr>
		<?php
		}
		?>
	</table>-->
		<?php
}
function get_Batch_Marks_Select_Result_Filter($assignmentid,$batchid,$studid,$studname,$studmarks,$studstatus)
{
  /*$recs = mysql_query("SELECT u.name, bs.batchid, u.userid, u.id,ifnull(aa.assignmentid,0) assignmentid, aa.marksobtained FROM batches_student bs inner join user u on bs.studentid=u.id left outer join assignment_attempted aa on u.id=aa.studentid and aa.assignmentid = ".$assignmentid." where bs.batchid=".$batchid) or die(mysql_error());*/
  
  if($studstatus == 1)
  {
	  $recsquery = "SELECT student_marks.*, user.Name, user.Role from student_marks INNER JOIN user ON student_marks.StudID = user.ID where user.Role = 'student' and student_marks.AssignmentID = ".$assignmentid." and student_marks.BatchID = ".$batchid;
	  
	  if($studid != '')
	  {
		 $recsquery .= " and student_marks.StudID = ".$studid;
	  }
	  if($studname != '')
	  {
		 $recsquery .= " and user.Name like '%".trim($studname)."%'";
	  }
	  if($studmarks == '0' || $studmarks == '1')
	  {
		 $recsquery .= " and student_marks.Markstatus = ".$studmarks;
	  }
	  
	  //echo "$recsquery";
	  $recs = mysql_query($recsquery);
  }
  else if($studstatus == 0)
  {
	  $recsquery = "SELECT `user`.ID as User_ID, `user`.Name FROM user WHERE (user.ID NOT IN (SELECT StudID FROM student_marks)) and user.Role = 'student'";
	  
	  if($studid != '')
	  {
		 $recsquery .= " and student_marks.StudID = ".$studid;
	  }
	  if($studname != '')
	  {
		 $recsquery .= " and user.Name like '%".trim($studname)."%'";
	  }
	  if($studmarks == '0' || $studmarks == '1')
	  {
		 $recsquery .= " and student_marks.Markstatus = ".$studmarks;
	  }
	  
	  //echo "$recsquery";
	  $recs = mysql_query($recsquery);
  }
  else
  {
		$recsquery = "SELECT `user`.ID as User_ID, `user`.Name, `student_marks`.* FROM `user` LEFT OUTER JOIN `student_marks` ON `user`.`ID` = `student_marks`.`StudID` where `user`.`Role` = 'student' and student_marks.AssignmentID = ".$assignmentid." and student_marks.BatchID = ".$batchid;
		
		if($studid != '')
	  {
		 $recsquery .= " and student_marks.StudID = ".$studid;
	  }
	  if($studname != '')
	  {
		 $recsquery .= " and user.Name like '%".trim($studname)."%'";
	  }
	  if($studmarks == '0' || $studmarks == '1')
	  {
		 $recsquery .= " and student_marks.Markstatus = ".$studmarks;
	  }
	  
		//echo "$recsquery";
	    $recs = mysql_query($recsquery);
  }
  
  
?>
<!--<form action="" method="POST" style="text-align: center; padding: 15px;">
  ID: <input type="text" name="studid" />
  Name: <input type="text" name="studname" />
  
  <input type="submit" name="submit" value="Search" onclick="getBatchMarksResultFilter(87)" />
  </form>-->
<table width="100%" align="center" border="0">
	<tr>
		<td style="width:25%;">
			ID: <input type="text" id="studid" name="studid" size="12" value="<?php echo $studid; ?>" />
		</td>
		<td style="width:25%;">
			Name: <input type="text" id="studname" name="studname" size="12" value="<?php echo $studname; ?>" />
		</td>
		<td style="width:20%;">
			<select name='studstatus' id='studstatus'>
				<option value='1' <?php if($studstatus == 1) echo 'selected="selected"'; ?>>Submitted</option>
				<option value='0' <?php if($studstatus == 0) echo 'selected="selected"'; ?>>Not Submitted</option>
				<option value='2' <?php if($studstatus == 2) echo 'selected="selected"'; ?>>Both</option>
			</select>
		</td>
		<td style="width:20%;">
			<select name='studmarks' id='studmarks'>
				<option value='1' <?php if($studmarks == 1) echo 'selected="selected"'; ?>>Marks Given</option>
				<option value='0' <?php if($studmarks == 0) echo 'selected="selected"'; ?>>Marks Not Given</option>
				<option value='2' <?php if($studmarks == 2) echo 'selected="selected"'; ?>>Both</option>
			</select>
		</td>
		<td style="width:10%;">
			<input id="" type="button" value="Filter" onclick="getBatchMarksResultFilter(<?php echo $assignmentid; ?>,<?php echo $batchid; ?>)" class="" style="cursor:pointer;">
		</td>
	</tr>
</table><br>
<?php
if(mysql_num_rows($recs) > 0)
{?>
<table width="100%" align="center" border="1">
<tr>
	<th>Student Id</th>
	<th>Name</th>
	<th>Marks</th>
	<th>&nbsp;</th>
</tr>
<?php  
	while($rec=mysql_fetch_array($recs))
	{
	  ?>
	<tr>
		<td style="width:15%;text-align:center;padding:2px;">
		<?php if(is_null($rec['StudID']))
			{?>
					<?=$rec['User_ID']?><input type="hidden" id="studid" value="<?=$rec['User_ID']?>" /><?php
			}
			else
			{?>
			<?=$rec['StudID']?><input type="hidden" id="studid" value="<?=$rec['StudID']?>" /><?php
			}
			?>
		</td>
		<td style="padding:2px;">
			<?php if(is_null($rec['UploadedAssignment']))
			{?>
					<?=$rec['Name']?><?php
			}
			else
			{?>
				<?=$rec['Name']?><a href="<?=$rec['UploadedAssignment']?>" target="blank" style="float:right;">Download PDF</a><?php
			}
			?>
		</td>
		<?php
			//$marks = mysql_query("SELECT * from student_marks where StudID = '".$rec["StudID"]."'") or die(mysql_error());
			//$rmarks=mysql_fetch_array($marks);
		?>
		<td style="width:20%;text-align:center;padding:2px;">
			<?php if(is_null($rec['Marks']))
			{
					print "Not Attempted";
			}
			else
			{		
					/*echo '<a class="fancybox fancybox.ajax btn btn-mini " href="checkstudentanswers.php?val1='.$assignmentid.'&val2='.$rec['id'].'">' . ($rec['Marks']) . '</a>';*/
					echo '<input type="text" id="studmarks_'.$rec["StudID"].'" value="'.$rec['Marks'].'">';
			}
					?><?php
			?>
		</td>
		<td style="width:10%;padding:2px;">
		<?php if(is_null($rec['Marks']))
				{
					print " ";
				}
				else
				{
				?>
					<input id="btnFilter" type="button" value="Submit" onclick="submitBatchMarks(<?php echo $rec["StudID"]; ?>,<?php echo $assignmentid; ?>,<?php echo $batchid; ?>);" class="grid-button-edit yellow-button" style="cursor:pointer;"/><?php
				}
			?>
		</td>
	</tr>
		<?php
		}
		?>
	</table>	
		<?php
		}
		else
		{?>
			<table width="100%" align="center" border="1">
			<tr>
				<th>Student Id</th>
				<th>Name</th>
				<th>Marks</th>
				<th>&nbsp;</th>
			</tr>
			<tr>
				<td colspan="4">0 Results Found</td>
			</tr>
			</table><?php
		}
}
function submit_Batch_Marks_Select($assignmentid,$batchid,$studid,$studmarks)
{
  //echo "value".$assignmentid.$batchid.$studid.$studmarks;
  /*$recs = mysql_query("update student_marks set AssignmentID = '".$assignmentid."', BatchID = '".$batchid."', Marks = '".$studmarks."', MarksUpdateDate = now(), Markstatus = 1 where StudID='".$studid."'");*/
  
  $recs = mysql_query("update student_marks set AssignmentID = '".$assignmentid."', BatchID = '".$batchid."', Marks = '".$studmarks."', MarksUpdateDate = now(), Markstatus = 1 where StudID='".$studid."' and AssignmentID = '".$assignmentid."'");
  
  /*echo "update student_marks set AssignmentID = '".$assignmentid."', BatchID = '".$batchid."', Marks = '".$studmarks."', MarksUpdateDate = now(), Markstatus = 1 where StudID='".$studid."'";*/
  /*echo "<script type='text/javascript'> window.location.replace('faculty_handler.php?action=getBatchMarks&assignmentid=".$assignmentid."&batchid=".$batchid."')</script>";*/
	if($recs)
		echo "true";
	else
		echo "false";
	

}	
		
function get_Courses_Select()
{
  $recs = mysql_query("select * from faculty_subject inner join course on course.ID=faculty_subject.CourseID where faculty_subject.UserID=".$_SESSION['userid']." and course.Enabled='1'") or die(mysql_error());
  while($rec=mysql_fetch_array($recs))
	{
	  ?>
	  <option value="<?=$rec['ID']?>"><?=$rec['CourseName']?></option>
					
		<?php
		}
}
		
function get_Faculty_Subjects()
{
  $recs = mysql_query("select * from faculty_subject inner join course on course.ID=faculty_subject.CourseID where faculty_subject.UserID=".$_SESSION['userid']." and course.Enabled='1'") or die(mysql_error());
  while($rec=mysql_fetch_array($recs))
	{
	  ?>
	  <tr>
		<td class='ui-widget-content batchitem '>					
		<a class='menulinknormal' href='./faculty_course.php?selectedCourse=<?=$rec["CourseID"]?>&action=addNew'><?=$rec['CourseName']?></a>
		</td>
		</tr>
		<?php
		}
}
		
function get_Student_Subjects()
{
  /** CESAR JUAREZ **/
  if($_SESSION['courseids']){
	$recs = mysql_query("select * from course where ID in (".get_student_recent_course().") and course.Enabled='1'") or die(mysql_error());
	while($rec=mysql_fetch_array($recs))
	  {
		?>
		<a class="list-link" href="./student_topics.php?selectedCourse=<?=$rec['ID']?>">
		  <div class="form-row" style="padding: 0px 15px;"><?=$rec['CourseName']?></div>
		</a>
		  <?php
		  }
  }
}
		
function get_Number_Of_Lessons_Index($courseid)
{
  $rs = mysql_query("select count(*) as totalAssignment from topics where Enabled=1 and CourseID=".$courseid);
  $rs = mysql_fetch_array($rs);
  return $rs['totalAssignment'];
		
}
		
function get_Number_Of_Lessons($courseid)
{
  $rs = mysql_query("select count(*) as totalAssignment from topics where Heading != 'Student Handbook' and Enabled=1 and CourseID=".$courseid);
  $rs = mysql_fetch_array($rs);
  print $rs['totalAssignment'];
}
		
function get_Number_Of_Lessons_Attempted($courseid)
{
  $rs = mysql_query("select count(*) as attend from student_topic_read where TopicID in (select ID from topics where Enabled=1 and CourseID=".$courseid.") and UserID=".$_SESSION['userid']);
  $rs=mysql_fetch_array($rs);
  print($rs['attend']);
}
		
function get_Number_Of_Lessons_Attempted_Index($courseid)
{
  $rs = mysql_query("select count(*) as attend from student_topic_read where TopicID in (select ID from topics where Enabled=1 and CourseID=".$courseid.") and UserID=".$_SESSION['userid']);
  $rs=mysql_fetch_array($rs);
  return $rs['attend'];
}
		
function get_Number_Of_Assignment_Attempted($courseid)
{
  $rs = mysql_query("select count(*) as attend from assignment_attempted where CourseID=".$courseid." and StudentID=".$_SESSION['userid']);
  $rs=mysql_fetch_array($rs);
  print($rs['attend']);
}
		
function get_Number_Of_Assignment_Attempted_Index($courseid)
{
  $rs = mysql_query("select count(*) as attend from assignment_attempted where CourseID=".$courseid." and StudentID=".$_SESSION['userid']);
  $rs=mysql_fetch_array($rs);
  return $rs['attend'];
}
		
function get_Number_Of_Assignment($courseid)
{
  $rs = mysql_query("select count(*) as totalAssignment from assignment where CourseID=".$courseid);
  $rs = mysql_fetch_array($rs);
  print $rs['totalAssignment'];
}
		
function get_Number_Of_Assignment_Index($courseid)
{
  $rs = mysql_query("select count(*) as totalAssignment from assignment where CourseID=".$courseid);
  $rs = mysql_fetch_array($rs);
  return $rs['totalAssignment'];
}
		
function get_First_Course_ID()
{
  $rs = mysql_query("SELECT * FROM course order by id LIMIT 0 , 1 ");
  $rs = mysql_fetch_array($rs);
  return $rs['ID'];
}
		
/** Cesar Juarez - OPEN COMET **/
function get_Student_exams()
{
  $recs = mysql_query("SELECT * 
	FROM  `assignment_attempted` 
	INNER JOIN  `course` ON course.ID = assignment_attempted.CourseID,  `assignment` 
	INNER JOIN  `topics` ON topics.ID = assignment.TopicID
	WHERE
	assignment_attempted.AssignmentID = assignment.ID AND assignment_attempted.StudentID = ". $_SESSION['userid']) or die(mysql_error());
  ?>
  <tr>
	 <td width="20%" style="padding-left: 25px;font-size: 12pt; color: #3399FF;">
	 Course Name
	 </td>
	 <td width="20%" style="padding-left: 25px;font-size: 12pt; color: #3399FF;">
	 Assignment Name
	 </td>
	 <td width="20%" style="padding-left: 25px;font-size: 12pt; color: #3399FF;">
	 Associated Topic
	 </td>
	 <td width="20%" style="padding-left: 25px;font-size: 12pt; color: #3399FF;">
	 Marks Obtained
	 </td>
	 <td width="20%" style="padding-left: 25px;font-size: 12pt; color: #3399FF;">
	 Total Marks
	 </td>
	 </tr>
	 <?php
	while($rec=mysql_fetch_array($recs))
	  {
		?>
		<tr class="ui-widget-content">
		<td width="20%" style="padding-left: 25px;font-size: 10pt; color: 0000;">
		<?=$rec['CourseName']?>
		</td>
		<td width="20%" style="padding-left: 25px;font-size: 10pt; color: 0000;">
		<?=$rec['AssignmentName']?>
		</td>
		<td width="20%" style="padding-left: 25px;font-size: 10pt; color: 0000;">
		<?=$rec['Heading']?>
		</td>
		<td width="20%" style="padding-left: 25px;font-size: 10pt; color: 0000;">
		<?=$rec['MarksObtained']?>
		</td>
		<td width="20%" style="padding-left: 25px;font-size: 10pt; color: 0000;">
		<?=$rec['TotalMarks']?>
		</td>

		</tr>
		<?php
	  }
}
function get_Faculty_Course_Subjects()
{
  $recs = mysql_query("select * from faculty_subject inner join course on course.ID=faculty_subject.CourseID where faculty_subject.UserID=".$_SESSION['userid']." and course.Enabled='1'") or die(mysql_error());
  while($rec=mysql_fetch_array($recs))
	{
	  ?>
	  <tr class="form-row" style="line-height:1.5em !important;">
		<td width="60%" style="padding-left: 25px;font-size: 20pt; color: #3399FF;">
		<?=$rec['CourseName']?>
		</td>
		<td style="text-align:center;width:18%">
		Modules: <?=get_Number_Of_Lessons($rec['ID'])?><br />
		<a href="./faculty_course.php?selectedCourse=<?=$rec['ID']?>&action=addNew" style="text-decoration:none;"><div class="grid-button-edit green-button" style="height: 25px ! important; width: 60px !important; margin:5px auto; ">Manage</div></a>
		</td>
		<td style="text-align:center;width:18%">
		Assignments:  <?=get_Number_Of_Assignment($rec['ID'])?><br />
		<a href="./faculty_assignment.php?selectedCourse=<?=$rec['ID']?>&action=manage" style="text-decoration:none;"><div class="grid-button-edit green-button" style="height: 25px ! important; width: 60px !important; margin:5px auto; ">Manage</div></a>
		</td>
	</tr>
		<?php
		}
}
function get_login_logout_name_report($val_search)
{

  
	$recs = mysql_query("SELECT u.Name, u.UserId, date_format(ul.activitydate,'%W, %D %M, %Y %H:%i') activitydate, ul.status FROM user_login_logout_status ul inner join user u on u.Id=ul.userid where ul.status in (1,2) and u.UserId = '".$val_search."' order by ul.activitydate desc") or die(mysql_error());
  

	while($rec=mysql_fetch_array($recs))
	  {
		?>
		<tr class="form-row" style="line-height:1.5em !important;">
			<td style="font-size: 10pt;">
				<?=$rec['UserId']?>
			</td>
			<td style="font-size: 10pt;width:40%;">
			<?=$rec['Name']?>
			</td>
			<td style="font-size: 10pt;text-align:center;">
		  <?php if($rec['activitydate']!="0000-00-00 00:00:00" && $rec['status']=="1") 
					print('Logged In: '.$rec['activitydate']);
				else if($rec['activitydate']!="0000-00-00 00:00:00" && $rec['status']=="2") 
					print('Password changed: '.$rec['activitydate']);
			?>
			</td>
			
		</tr>
		  <?php
		  }
}

function get_login_logout_report($val_search)
{

  if($val_search!=""){
	$recs = mysql_query("SELECT u.Name, u.UserId, date_format(ul.activitydate,'%W, %D %M, %Y %H:%i') activitydate, ul.status, b.batchid FROM user_login_logout_status ul inner join user u on u.Id=ul.userid left outer join batches_student b on ul.userid = b.StudentID where ul.status in (1,2) and b.batchid=".$val_search." order by ul.activitydate desc limit 0,500") or die(mysql_error());
	}
	else{
	$recs = mysql_query("SELECT u.Name, u.UserId, date_format(ul.activitydate,'%W, %D %M, %Y %H:%i') activitydate, ul.status FROM user_login_logout_status ul inner join user u on u.Id=ul.userid where ul.status in (1,2) order by ul.activitydate desc limit 0,500") or die(mysql_error());
  }

	while($rec=mysql_fetch_array($recs))
	  {
		?>
		<tr class="form-row" style="line-height:1.5em !important;">
			<td style="font-size: 10pt;">
				<?=$rec['UserId']?>
			</td>
			<td style="font-size: 10pt;width:40%;">
			<?=$rec['Name']?>
			</td>
			<td style="font-size: 10pt;text-align:center;">
				<?php 	if($rec['activitydate']!="0000-00-00 00:00:00" && $rec['status']=="1") 
							print('Logged In: '.$rec['activitydate']);
						else if($rec['activitydate']!="0000-00-00 00:00:00" && $rec['status']=="2") 
							print('Password changed: '.$rec['activitydate']);
				?>
			</td>
		</tr>
		  <?php
		  }
}
function get_Admin_Topics($val_search)
{

  if($val_search!=""){
	$recs = mysql_query("select * from topics, course where topics.Enabled=1 and topics.CourseId = course.ID
and course.CourseName LIKE '%". $val_search ."%'
 Order By topics.Heading ASC ") or die(mysql_error());
  }else{
$recs = mysql_query("select * from topics, course where topics.Enabled=1 and topics.CourseId = course.ID
 Order By topics.Heading ASC ") or die(mysql_error());
  }

	while($rec=mysql_fetch_array($recs))
	  {
		?>
		<tr class="form-row" style="line-height:1.5em !important;">
<td style="font-size: 10pt;">
				<?=$rec['Heading']?>
			</td>
<td style="font-size: 10pt;">
<?=$rec['CourseName']?>
</td>
			<td style="font-size: 10pt;">
		  <?php if($rec['ActivateFrom']!="0000-00-00 00:00:00") print(date("d M, Y",strtotime($rec['ActivateFrom'])));?>
			</td>
		</tr>
		  <?php
		  }
}


function get_Admin_Assignment($val_search)
{

  if($val_search!=""){
	$recs = mysql_query("select * from assignment, course where assignment.CourseId = course.ID
and course.CourseName LIKE '%". $val_search ."%'
 Order By assignment.AssignmentName ASC ") or die(mysql_error());
  }else{
$recs = mysql_query("select * from assignment, course where assignment.CourseId = course.ID
 Order By assignment.AssignmentName ASC ") or die(mysql_error());
  }

	while($rec=mysql_fetch_array($recs))
	  {
		?>
		<tr class="form-row" style="line-height:1.5em !important;">
<td style="font-size: 10pt;">
				<?=$rec['AssignmentName']?>
			</td>
<td style="font-size: 10pt;">
<?=$rec['CourseName']?>
</td>
		</tr>
		  <?php
		  }
}


function get_Student_Course_Subjects()
{
  if($_SESSION['courseids']){
	$recs = mysql_query("select * from course where course.ID in (".get_student_recent_course().") and course.Enabled='1'") or die(mysql_error());
	while($rec=mysql_fetch_array($recs))
	  {
		?>
		<tr class="form-row" style="line-height:1.5em !important;">
			<td width="60%" style="padding-left: 25px;font-size: 20pt;">
				<a class="list-link" style=" color: #3399FF !important;" href="./student_topics.php?selectedCourse=<?=$rec['ID']?>"><div width="100%"><?=$rec['CourseName']?></div></a>
			</td>
			<td style="text-align:center;width:18%">
				Modules:  <?=get_Number_Of_Lessons($rec['ID'])?><br />
				Attended: <?=get_Number_Of_Lessons_Attempted($rec['ID'])?>
			</td>
			<td style="text-align:center;width:18%">
				Assignments:  <?=get_Number_Of_Assignment($rec['ID'])?><br />
				Attended: <?=get_Number_Of_Assignment_Attempted($rec['ID'])?>
			</td>
		</tr>
		  <?php
		  }
  }


}

/* for without collapsible pane */
function get_Student_Course_SubjectsList()
{
  if($_SESSION['courseids']){
	$recs = mysql_query("select * from course where course.ID in (".get_student_recent_course().") and course.Enabled='1'") or die(mysql_error());
	while($rec=mysql_fetch_array($recs))
	  {
		?>
		<tr>
			<td style="border-bottom: #ddd 1px solid;   padding-left: 25px; font-size: 20pt;">
				<a class="list-link" style=" color: #3399FF !important; cursor:text;" href="#"><div width="100%" style='padding: 5px 15px;'><?=$rec['CourseName']?></div></a>
			</td>
			<td style="border-bottom: #ddd 1px solid;width: 15%;padding: 5px 5px;">
				<a href='./student_topics.php?selectedCourse=<?=$rec['ID']?>' style='cursor:pointer; text-decoration:none;'>

					<div class='grid-button-edit green-button' style='width: 195px; padding: 5px 0px 2px 10px; height: 20px;'>

					Click Here To Module List

					</div>

				</a>
			</td>
		</tr>
		  <?php
		  }
  }


}
		
function printAllQuestions()
{
			
  $recs=mysql_query("select * from queries where Reply='' and CourseID in (select course.ID from faculty_subject inner join course on course.ID=faculty_subject.CourseID where faculty_subject.UserID=".$_SESSION['userid']." and course.Enabled='1') order by ID desc");
  while($rec=mysql_fetch_array($recs))
	{
	  $usr=mysql_query("select * from user where ID=".$rec['PosterID']);
	  $usr=mysql_fetch_array($usr);
	  ?>
	 <tr style="border-bottom: #ddd 1px solid;">
		<td style="font-weight: bold; font-size: 10pt;" colspan="2">
			<a id="inline" href="#data" onClick="qid=<?=$rec['ID']?>;"><?=$rec['Question']?></a>
			<br />
		</td>
	</tr>
	<tr style="border-bottom: #ddd 1px solid;">
		<td width="60%" style="font-size: 10pt;">
			By: <?=$usr['Name']?>
		</td> 
		<td style="font-size: 10pt;float:right;">
			<?=date('d M, Y',strtotime($rec['PostDate']))?>
		<br/>
		</td>
	</tr>
		 <?php
		 }
}

function printAllQuestions_admin()
{
			
  $recs=mysql_query("select * from queries order by ID desc LIMIT 0, 10");
  while($rec=mysql_fetch_array($recs))
	{
	  $usr=mysql_query("select * from user where ID=".$rec['PosterID']);
	  $usr=mysql_fetch_array($usr);
	  ?>
	  <tr style="border-bottom: #ddd 1px solid;">
		 <td style="font-weight: bold; font-size: 10pt;"><?=$rec['Question']?><br /></td>
</tr>
<tr style="border-bottom: #ddd 1px solid;" >		 
		 <td width="60%" style="font-size: 10pt;">By: <?=$usr['Name']?></td> <td style="font-size: 10pt;"><?=date('d M, Y',strtotime($rec['PostDate']))?></td>
</tr>
		 <?php
		 }
}

function printAllNotices_admin()
{
			
  $recs=mysql_query("select * from notice order by ID desc LIMIT 0, 10");
  while($rec=mysql_fetch_array($recs))
	{
	?>
	  <li style="list-style-type:none; border-bottom: #ddd 1px solid; font-weight: bold; font-size: 10pt;"><?=$rec['NoticeName']?><br /></li>
	<?php
	}
}


function notificationStudentAssignment(){ /** CESAR JUAREZ - OPEN COMET **/

  $notass = mysql_query ("select CourseName, AssignmentName, StartDate
from
assignment, course, assignment_batch, batches_student
where
assignment.CourseID = course.ID and
assignment.ID = assignment_batch.assignmentID and
assignment_batch.BatchID = '" . $_SESSION['batchid'] . "' and
batches_student.BatchID = assignment_batch.BatchID and
batches_student.StudentID = '" . $_SESSION['userid'] . "' and
assignment_batch.StartDate > CONVERT_TZ(NOW(),'".$server_time_zone."','".$application_time_zone."') ORDER BY StartDate DESC LIMIT 0,3");?>

<table width=100%>

<?php
  while($rec = mysql_fetch_array($notass))
	{

	  $starttime = strtotime($rec['StartDate']);
	  $currenttime = strtotime(converToTz(date('Y-m-d H:i:s'),$application_time_zone_text,$server_time_zone_text));

	  $diferencia = $starttime - $currenttime;

	  ?>
<tr>
	  <td width="30%" style="font-size: 15px; text-align: center; vertical-align: middle; color: red;  border-bottom: 1px solid #ddd; font-weight: bold;"><?php echo date('d',$diferencia) . "<br /> DAYS<br />" . date('H', $diferencia) . "<br /> HRS"?></td>
<td style="vertical-align: middle; color: red;  border-bottom: 1px solid #ddd; font-size: 10pt;">to go for the <?= $rec['CourseName']?> Assignment on <?=$rec['AssignmentName']


?></td>
</tr>
		<?php
	}
?>
</table>
<?php
}

function notificationStudentSubject(){

  $notass = mysql_query ("SELECT *
FROM topics, course, batches_courses
WHERE topics.Enabled=1 and
topics.CourseID = course.ID and
course.ID = batches_courses.CourseID and
batches_courses.BatchID = '". $_SESSION['batchid'] ."'
ORDER BY ActivateFrom DESC
LIMIT 0, 3");

?>

<table width=100%>

<?php
  while($rec = mysql_fetch_array($notass))
	{
	  ?>
	  <tr>
<td colspan="2" style="padding: 5px; color: orange; font-weight: bold; font-size: 10pt; text-align: left;  border-bottom: 1px solid #ddd;"><?= $rec['Heading'] ?></td>
</tr>
<tr >
	  <td style="padding: 5px; color: blue; text-align: left;  border-bottom: 1px solid #ddd; font-size: 10pt;" width="49%">
	  Subject: <?= $rec['CourseName'] ?> </td><td style="padding: 5px; color: blue;  border-bottom: 1px solid #ddd; font-size: 10pt;text-align: right;"> DateAdded: <? if($rec['ActivateFrom']!="0000-00-00 00:00:00" || $rec['ActivateFrom']!=null) { print(date('d M, y',strtotime($rec['ActivateFrom']))); }?>
	  
</td>

</tr>
		<?php
	}
?>
</table>
<?php
	}


function get_Student_Course_Subjects_Index()
{
  /** César Juárez **/
  if($_SESSION['courseids']){

	$tl=0;
	$atl=0;
			
	$ta=0;
	$ata=0;
	$recs = mysql_query("select * from course where course.ID in (".get_student_recent_course().") and course.Enabled='1'") or die(mysql_error());
	while($rec=mysql_fetch_array($recs))
	  {
		$tl+=get_Number_Of_Lessons_Index($rec['ID']);
		$atl+=get_Number_Of_Lessons_Attempted_Index($rec['ID']);
		$ta+=get_Number_Of_Assignment_Index($rec['ID']);
		$ata+=get_Number_Of_Assignment_Attempted_Index($rec['ID']);
				
	  }
	?>
	<div style="font-size:14pt;padding: 20px 0px;">
		<div style="color: red;padding: 5px 15px;">
		Modules read: <div style="float:right;"><?= $atl ?></div>
		</div>
		<div style="color: blue;padding: 5px 15px;border-bottom:solid 1px #ddd;">
		Total Modules: <div style="float:right;"><?= $tl?></div>
		</div>
		<div style="color: red;padding: 5px 15px;">
		Assignments Attempted: <div style="float:right;"><?= $ata ?></div>
		</div>
		<div style="color: blue;padding: 5px 15px;">
		Total Assignments: <div style="float:right;"><?= $ta ?></div>
		</div>
	</div>
	   
	   <?php
	   }

}
		
function get_Student_Submitted_Assignements()
{
  /** LAST VAL = ASSRS **/
  $cr=mysql_query("select * from assignment_attempted inner join course on course.ID=assignment_attempted.CourseID 
inner join assignment on assignment.ID = assignment_attempted.AssignmentID
inner join assignment_batch on assignment_batch.AssignmentID = assignment_attempted.AssignmentID
where StudentID=".$_SESSION['userid']." order by assignmentEN desc LIMIT 0 , 5");
  ?>
  <table width="100%"> <!-- CESAR JUAREZ - OPEN COMET -->
	 <?php
	while($c=mysql_fetch_array($cr))
	  {
		?>
		<tr valign="middle" style="border-bottom:solid 1px #ddd;">
			<td height="35px" width="100%" style="padding:5px 5px 5px 20px;vertical-align:middle;">
				<div><?= $c['AssignmentName']?></div>
				<div style="font-size:10pt;color:#b7b7b7;padding-top:5px;">
					<div style="width:60%;float:left;">Subject: <?=$c['CourseName']?></div>
					<div style="width:40%;float:right;">Date Added: 
				<?php if($c['StartDate']!="0000-00-00 00:00:00" || $c['StartDate']!=null) { print(date("d M, Y",strtotime($c['StartDate']))); }?></div>
				</div>
			</td>
		</tr>
		<?php
	  }
  ?>

  </table>
	  <?php	
	  }
		
function get_Admin_Subjects()
{
  $rec = mysql_query("select count(*) as totalCourse from course where Enabled='1'") or die(mysql_error());
  $rec = mysql_fetch_array($rec);
  return $rec['totalCourse'];
			 
}
function get_Admin_Faculties()
{
  $rec = mysql_query("select count(*) as totalFaculties from user where Enabled='1' and Role='faculty'") or die(mysql_error());
  $rec = mysql_fetch_array($rec);
  return $rec['totalFaculties'];
			 
}
function get_Admin_Students()
{
  $rec = mysql_query("select count(*) as totalStudents from user where Enabled='1' and Role='student'") or die(mysql_error());
  $rec = mysql_fetch_array($rec);
  return $rec['totalStudents'];
}
function get_All_Batches_Admin()
{
  $recs = mysql_query("select * from batches
where Enabled = 1
ORDER BY ID DESC") or die(mysql_error());
  while($rec = mysql_fetch_array($recs))
	{
	  $stdcount=mysql_query("select count(*) as totalStudent from batches_student inner join user on user.ID=batches_student.StudentID where user.role='student' and user.Enabled=1 and batches_student.BatchID=".$rec['ID']) or die(mysql_error());
	  $stdcount=mysql_fetch_array($stdcount);
				
	  /*if($_GET['response']=="batchcreated" && $rec['ID']==$_SESSION['batchid'])
		{
		  print("<tr class='ui-widget-header batchitem '><td><a class='menulinknormal' href=./admin_batches.php?action=setbatch&id=".$rec['ID'].">".$rec['BatchName']." (".$stdcount['totalStudent'].")</a></td><td width='10%'><img src=\"images/tick-small.png\"></td>
</tr>");
		}
	  else
		{
		  print("<tr class='ui-widget-content batchitem '><td><a class='menulinknormal' href=./admin_batches.php?action=setbatch&id=".$rec['ID'].">".$rec['BatchName']." (".$stdcount['totalStudent'].")</a></td><td width='10%'><img src=\"images/tick-small.png\"></td>
</tr>");
		}*/
		if($_GET['response']=="batchcreated" && $rec['ID']==$_SESSION['batchid'])
		{
		  print("<tr class='ui-widget-header batchitem '><td><a class='menulinknormal' href=./admin_batches.php?action=setbatch&id=".$rec['ID'].">".$rec['BatchName']." </a></td><td width='10%'><img src=\"images/tick-small.png\"></td>
</tr>");
		}
	  else
		{
		  print("<tr class='ui-widget-content batchitem '><td><a class='menulinknormal' href=./admin_batches.php?action=setbatch&id=".$rec['ID'].">".$rec['BatchName']." </a></td><td width='10%'><img src=\"images/tick-small.png\"></td>
</tr>");
		}
	}

$recs = mysql_query("select * from batches
where Enabled = 0") or die(mysql_error());

while($rec = mysql_fetch_array($recs))
	{
	  $stdcount=mysql_query("select count(*) as totalStudent from batches_student inner join user on user.ID=batches_student.StudentID where user.role='student' and user.Enabled=1 and batches_student.BatchID=".$rec['ID']) or die(mysql_error());
	  $stdcount=mysql_fetch_array($stdcount);
				
	  if($_GET['response']=="batchcreated" && $rec['ID']==$_SESSION['batchid'])
		{
		  print("<tr class='ui-widget-header batchitem '><td><a class='menulinknormal' href=./admin_batches.php?action=setbatch&id=".$rec['ID'].">".$rec['BatchName']." (".$stdcount['totalStudent'].")</a></td><td width='10%'><img src=\"images/wrong.png\"></td></tr>");
		}
	  else
		{
		  print("<tr class='ui-widget-content batchitem '><td><a class='menulinknormal' href=./admin_batches.php?action=setbatch&id=".$rec['ID'].">".$rec['BatchName']." (".$stdcount['totalStudent'].")</a></td><td width='10%'><img src=\"images/wrong.png\"></td></tr>");
		}
	}

}
function get_All_Assignments_Faculty()
{
  $qry = mysql_query("SELECT distinct a.id,a.assignmentname FROM `assignment` a inner join faculty_subject f on a.CourseId=f.CourseId where f.userid=".$_SESSION['userid']);
  /*print("<tr class='ui-widget-content'><td style='vertical-align:middle; padding:7px 20px;font-weight:bold;'>Assignment<br/><select name='assignment' id='selAssignmentResult' title='Choose Assignment' style='width:200px' onchange='loadBatchesAssignedResult();'><option value='-1'>Choose Assignment</option>");
  while($rqry = mysql_fetch_array($qry))
	{
	  print("<option value='".$rqry['id']."'>".$rqry['assignmentname']."</option>");
	}
   print("</select></td><td width='10%'></td></tr>");*/
   
	print("<tbody>");
	while($rqry = mysql_fetch_array($qry))
	{
	  print("<tr><td name='assignment' id='selAssignmentResult' onclick='loadBatchesAssignedResult(".$rqry['id'].");' style='border-bottom:1px solid #ddd; padding:3px; cursor: pointer;'>".$rqry['assignmentname']."</td></tr>");
	}
	print("</tbody>");
	
}
function get_All_Batches_Admin_Notice()
{
  $recs = mysql_query("select * from batches
where Enabled = 1
ORDER BY ID DESC") or die(mysql_error());
  while($rec = mysql_fetch_array($recs))
	{
	  $stdcount=mysql_query("select count(*) as totalStudent from batches_student inner join user on user.ID=batches_student.StudentID where user.role='student' and user.Enabled=1 and batches_student.BatchID=".$rec['ID']) or die(mysql_error());
	  $stdcount=mysql_fetch_array($stdcount);
				
	  /*if($_GET['response']=="batchcreated" && $rec['ID']==$_SESSION['batchid'])
		{
		  print("<tr class='ui-widget-header batchitem '><td><a class='menulinknormal' href=./admin_batches.php?action=setbatch&id=".$rec['ID'].">".$rec['BatchName']." (".$stdcount['totalStudent'].")</a></td><td width='10%'><img src=\"images/tick-small.png\"></td>
</tr>");
		}
	  else
		{
		  print("<tr class='ui-widget-content batchitem '><td><a class='menulinknormal' href=./admin_batches.php?action=setbatch&id=".$rec['ID'].">".$rec['BatchName']." (".$stdcount['totalStudent'].")</a></td><td width='10%'><img src=\"images/tick-small.png\"></td>
</tr>");
		}*/
		  print("<tr class='ui-widget-content batchitem '><td width='10%'><input type='checkbox'  style='width:20px;height:20px' name='batchid' id='batchid' value='2'></td><td>".$rec['BatchName']."</td>
</tr>");
	}

}

if($_GET['action']=="filter_all_student_batch")
{
	//echo "<script type='text/javascript'> window.location.replace('admin_students.php?response=batchcreated&id=5')</script>";
	echo "<script type='text/javascript'> window.location.replace('admin_students.php?response=batchcreated&studid=".$_POST['studid']."&studname=".$_POST['studname']."&studemailid=".$_POST['studemailid']."&batchid=".$_SESSION['batchid']."')</script>.";
}

function get_All_Batches_Students()
{
  $recs = mysql_query("select * from batches
where Enabled = 1
ORDER BY ID DESC") or die(mysql_error());
  while($rec = mysql_fetch_array($recs))
	{
	  $stdcount=mysql_query("select count(*) as totalStudent from batches_student inner join user on user.ID=batches_student.StudentID where user.role='student' and user.Enabled=1 and batches_student.BatchID=".$rec['ID']) or die(mysql_error());
	  $stdcount=mysql_fetch_array($stdcount);
				
	  if($_GET['response']=="batchcreated" && $rec['ID']==$_SESSION['batchid'])
		{
		  print("<tr class='ui-widget-header batchitem '><td><a class='menulinknormal' href=./admin_students.php?action=setbatch&id=".$rec['ID'].">".$rec['BatchName']." (".$stdcount['totalStudent'].")</a></td><td width='10%'><img src=\"images/tick-small.png\"></td>
</tr>");
		}
	  else
		{
		  print("<tr class='ui-widget-content batchitem '><td><a class='menulinknormal' href=./admin_students.php?action=setbatch&id=".$rec['ID'].">".$rec['BatchName']." (".$stdcount['totalStudent'].")</a></td><td width='10%'><img src=\"images/tick-small.png\"></td>
</tr>");
		}
	}

$recs = mysql_query("select * from batches
where Enabled = 0") or die(mysql_error());

while($rec = mysql_fetch_array($recs))
	{
	  $stdcount=mysql_query("select count(*) as totalStudent from batches_student inner join user on user.ID=batches_student.StudentID where user.role='student' and user.Enabled=1 and batches_student.BatchID=".$rec['ID']) or die(mysql_error());
	  $stdcount=mysql_fetch_array($stdcount);
				
	  if($_GET['response']=="batchcreated" && $rec['ID']==$_SESSION['batchid'])
		{
		  print("<tr class='ui-widget-header batchitem '><td><a class='menulinknormal' href=./admin_students.php?action=setbatch&id=".$rec['ID'].">".$rec['BatchName']." (".$stdcount['totalStudent'].")</a></td><td width='10%'><img src=\"images/wrong.png\"></td></tr>");
		}
	  else
		{
		  print("<tr class='ui-widget-content batchitem '><td><a class='menulinknormal' href=./admin_students.php?action=setbatch&id=".$rec['ID'].">".$rec['BatchName']." (".$stdcount['totalStudent'].")</a></td><td width='10%'><img src=\"images/wrong.png\"></td></tr>");
		}
	}

}

function get_All_Courses_Admin()
{/** CESAR JUAREZ - OPEN COMET **/
  $recs = mysql_query("select * from course where Enabled = 1 Order By ID DESC") or die(mysql_error());
  while($rec = mysql_fetch_array($recs))
	{
	  if($_GET['response']=="coursecreated" && $rec['ID']==$_SESSION['courseid'])
		{
		  print("<tr class='ui-widget-header batchitem '><td><a class='menulinknormal' href=./admin_subjects.php?action=setcourse&id=".$rec['ID'].">".$rec['CourseName']." </a></td><td width='10%'><img src=\"images/tick-small.png\"></td></tr>");
		}
	  else
		{
		  print("<tr class='ui-widget-content batchitem '><td><a class='menulinknormal' href=./admin_subjects.php?action=setcourse&id=".$rec['ID'].">".$rec['CourseName']." </a></td><td width='10%'><img src=\"images/tick-small.png\"></td></tr>");
		}
	}
  $recs = mysql_query("select * from course where Enabled = 0") or die(mysql_error());
  while($rec = mysql_fetch_array($recs))
	{
	  if($_GET['response']=="coursecreated" && $rec['ID']==$_SESSION['courseid'])
		{
		  print("<tr class='ui-widget-header batchitem '><td><a class='menulinknormal' href=./admin_subjects.php?action=setcourse&id=".$rec['ID'].">".$rec['CourseName']." </a></td><td width='10%'><img src=\"images/wrong.png\"></td></tr>");
		}
	  else
		{
		  print("<tr class='ui-widget-content batchitem '><td><a class='menulinknormal' href=./admin_subjects.php?action=setcourse&id=".$rec['ID'].">".$rec['CourseName']." </a></td><td width='10%'><img src=\"images/wrong.png\"></td></tr>");
		}
	}


}

function get_All_Notices_Admin()
{/** CESAR JUAREZ - OPEN COMET **/
  $recs = mysql_query("select * from notice where Enabled = 1 Order By ID DESC") or die(mysql_error());
  while($rec = mysql_fetch_array($recs))
	{
	  if($_GET['response']=="noticecreated" && $rec['ID']==$_SESSION['noticeid'])
		{
		  print("<tr class='ui-widget-header batchitem '><td><a class='menulinknormal' href=./admin_notices.php?action=setnotice&id=".$rec['ID'].">".$rec['NoticeName']." </a></td><td width='10%'><img src=\"images/tick-small.png\"></td></tr>");
		}
	  else
		{
		  print("<tr class='ui-widget-content batchitem '><td><a class='menulinknormal' href=./admin_notices.php?action=setnotice&id=".$rec['ID'].">".$rec['NoticeName']." </a></td><td width='10%'><img src=\"images/tick-small.png\"></td></tr>");
		}
	}
  $recs = mysql_query("select * from notice where Enabled = 0") or die(mysql_error());
  while($rec = mysql_fetch_array($recs))
	{
	  if($_GET['response']=="noticecreated" && $rec['ID']==$_SESSION['noticeid'])
		{
		  print("<tr class='ui-widget-header batchitem '><td><a class='menulinknormal' href=./admin_notices.php?action=setnotice&id=".$rec['ID'].">".$rec['NoticeName']." </a></td><td width='10%'><img src=\"images/wrong.png\"></td></tr>");
		}
	  else
		{
		  print("<tr class='ui-widget-content batchitem '><td><a class='menulinknormal' href=./admin_notices.php?action=setnotice&id=".$rec['ID'].">".$rec['NoticeName']." </a></td><td width='10%'><img src=\"images/wrong.png\"></td></tr>");
		}
	}


}

function get_All_Batches_Admin_As_Option()
{
  $recs = mysql_query("select * from batches") or die(mysql_error());
  while($rec = mysql_fetch_array($recs))
	{
	  $stdcount=mysql_query("select count(*) as totalStudent from batches_student inner join user on user.ID=batches_student.StudentID where user.role='student' and user.Enabled=1 and batches_student.BatchID=".$rec['ID']) or die(mysql_error());
	  $stdcount=mysql_fetch_array($stdcount);

	  $extbat = mysql_query("SELECT * FROM batches_student");

	  print("<option value=".$rec['ID'].">".$rec['BatchName'] . $id . " </option>");
				
	}
}
/*function get_All_Subjects_Admin_As_Option()
{
  $recs = mysql_query("select * from course") or die(mysql_error());
  
  while($rec = mysql_fetch_array($recs))
	{
	  //print("<option value=".$rec['ID'].">".$rec['CourseName']." </option>");
	  print("<option value=".$rec['ID'].">".$rec['CourseName']." </option>");
				
	}
}*/

function get_All_Subjects_Admin_As_Option()
{
  print("<option value=''>No faculty selected</option>");
}
		
function get_All_Topics($courseid) /** CESAR JUAREZ - GRUPO COMET **/
{
  if (!isset($courseid)) return;

  $recs = mysql_query("select * from topics where topics.Enabled=1 and CourseID=".$courseid." and IsSubTopic=0 and Enabled=1") or die(mysql_error());
  if(mysql_num_rows($recs)>0)
  {
  while($rec = mysql_fetch_array($recs))
	{
	  if($_SESSION['userrole']=="student"){
		//print("<tr class='ui-widget-content'><td>".$rec['Heading']."</td><td align='right'><a href='./student_subtopics.php?action=view&selectedCourse=".$courseid."&topicid=".$rec['ID']."'>View</a> </td></tr>");
		print("<tr>
					<td style='border-bottom:1px solid #ddd; padding:3px;'>
						<a class='menulinknormal' href='./student_subtopics.php?action=view&selectedCourse=".$courseid."&topicid=".$rec['ID']."'>".$rec['Heading']."</a>
					</td>
					<!--<td align='right'>
						<a href='./faculty_course.php?action=addNew&selectedCourse=".$courseid."' style='padding-right: 5px'>New</a> 
						<a href='./faculty_course.php?action=edit&selectedCourse=".$courseid."&topicid=".$rec['ID']."'>Manage</a> 
					</td>-->
				</tr>");
	  }else{
		//print("<tr class='ui-widget-content'><td><a class='menulinknormal' href='#' onclick=\"topid(".$rec['ID'].");\">".$rec['Heading']."</a></td><td align='right'><a href='./faculty_course.php?action=addNew&selectedCourse=".$courseid."' style='padding-right: 5px'>New</a> <a href='./faculty_course.php?action=edit&selectedCourse=".$courseid."&topicid=".$rec['ID']."'>Manage</a> </td></tr>");
		print("<tr>
					<td style='border-bottom:1px solid #ddd; padding:3px;'>
						<a class='menulinknormal' href='#' onclick=\"topid(".$rec['ID'].");\">".$rec['Heading']."</a>
					</td>
					<!--<td align='right'>
						<a href='./faculty_course.php?action=addNew&selectedCourse=".$courseid."' style='padding-right: 5px'>New</a> 
						<a href='./faculty_course.php?action=edit&selectedCourse=".$courseid."&topicid=".$rec['ID']."'>Manage</a> 
					</td>-->
				</tr>");
	  }
	}
	}
	else{
		print("<tr>
					<td style='text-align:center; font-size:11pt; color:red;'>There are no modules added to the course yet, please add new modules by clicking on Create Button</td>
				</tr>");
	}
}
		
function get_All_Topics_Student($courseid)
{
  //$recs = mysql_query("select * from topics where CourseID=".$courseid." and IsSubTopic=0") or die(mysql_error());
  $recs = mysql_query("select * from topics where Enabled=1 and CourseID=".$courseid." and IsSubTopic=0 order by sortorder") or die(mysql_error());

 ?>
 <table width="100%">
 <?php

$i = 1;
    while($rec = mysql_fetch_array($recs))
  	{
 ?>
 <tr>
 <td style="border-bottom: #ddd 1px solid;">
 <a  class='list-link' href='#'>

  <?php /*if($rec['Heading']!='Student Handbook') { echo 'Module '. $i; } ?><div style='padding: 5px 15px;'><?php echo $rec['Heading'];*/ ?>
  <?php if(isset($rec['Prefix'])) echo $rec['Prefix']; ?><div style='padding: 5px 15px;'><?php echo $rec['Heading']; ?></div>
 			</a>
 </td>
 <td style="border-bottom: #ddd 1px solid;width: 15%;padding: 5px 5px;">
 <?php 
 /** CESAR JUAREZ - OPEN COMET **/
 $read = mysql_query("SELECT *
 FROM student_topic_read
 WHERE
 UserID = '". $_SESSION['userid'] ."' and
 TopicID = '". $rec['ID'] . "' and
 CourseID = '". $courseid ."'");
 /*if(mysql_num_rows($read)==0){
 ?><img src="images/wrong.png"><?php
 }else{
 ?><img src="images/tick-small.png"><?php
 }*/
 ?>
 <a href='flipbook.php?topicid=<?php echo $rec['ID']; ?>' style='cursor:pointer; text-decoration:none;' target='_blank'>

	<div class='grid-button-edit green-button' style='width: 50px; padding: 5px 0px 2px 10px; height: 20px;'>

		Read

	</div>

</a>
 </td>
 	</tr>			
 <?php
$i++;
} ?>
 </table>
 
 <?php

}

/*** CESAR JUAREZ - OPEN COMET ***/
function get_All_Queries_Student($courseid)
{
  $recs = mysql_query ("select * from queries 
	where CourseID = '" . $courseid . "' and PosterID= '" . $_SESSION['userid'] . "' order by ID desc")  or die(mysql_error());

  while($rec = mysql_fetch_array($recs)){?>
	<tr style="border-top:solid 1px #ccc;">
	  <td style="padding: 10px;font-size: 12pt; color: #E49114;">
	  <?=$rec['Question']?><br /><p style="color: black; font-size: 8pt;"><?=date('d M, Y',strtotime($rec['PostDate']));?></p>
	  </td>
	</tr>
	  <?php if($rec['Reply'] != ""){ 
	  $rep=mysql_query("select * from user where ID=".$rec['ReplyerID']);
	  $rep=mysql_fetch_array($rep);?>
	  <tr class="form-row" style="line-height:1.2em !important;">
	  <td style="padding: 10px; padding-left:25px;font-size: 10pt; color: #3399FF;">
	  <?=$rec['Reply'];?> <br /> <p style="color: black; font-size: 8pt;"><?=date('d M, Y',strtotime($rec['ReplyDate']));?> By <?=$rep['Name'];?></p>
	  </td>
	  <?php } ?>
	</tr>
		<?php 
		}
}

function get_All_Queries_Admin($val_search)
{

  if($val_search == "") $recs = mysql_query ("select * from queries, course where queries.CourseID = course.ID order by queries.ID desc")  or die(mysql_error());
  else $recs = mysql_query("select * from queries, course where queries.CourseID = course.ID and course.CourseName LIKE '%".$val_search."%' order by queries.ID desc") or die(mysql_error());

  while($rec = mysql_fetch_array($recs)){?>
	<tr style="border-top:solid 1px #ccc;">
	  <td style="padding: 10px;font-size: 12pt; color: #E49114;">
	  <?=$rec['Question'] . " subject: " . $rec['CourseName']?><br /><p style="color: black; font-size: 8pt;"><?=date('d M, Y',strtotime($rec['PostDate']));?></p>
	  </td>
	</tr>
	  <?php if($rec['Reply'] != ""){ 
	  $rep=mysql_query("select * from user where ID=".$rec['ReplyerID']);
	  $rep=mysql_fetch_array($rep);?>
	  <tr class="form-row" style="line-height:1.2em !important;">
	  <td style="padding: 10px; padding-left:25px;font-size: 10pt; color: #3399FF;">
	  <?=$rec['Reply'];?> <br /> <p style="color: black; font-size: 8pt;"><?=date('d M, Y',strtotime($rec['ReplyDate']));?> By <?=$rep['Name'];?></p>
	  </td>
	  <?php } ?>
	</tr>
		<?php 
		}
}

?>
