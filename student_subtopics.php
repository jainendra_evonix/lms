<?php
	require_once("header.php");
	require_once("mainFunctions.php");
?>
	<nav>
		<ul>
			<?php getAllCoursesList("student_subtopics.php"); ?>
		</ul>
	</nav>

<?php
	if($_GET['action']=="")
	{
		?>
		<script>
			function topid(id)
			{
				$.ajax({
						url: 'faculty_handler.php?action=getTopic&topicID='+id,
						success: function(data) {
							$('#brief').html(data);
						}
					});
					
				$.ajax({
						url: 'faculty_handler.php?action=getSubTopics&selectedCourse=<?=$_GET['selectedCourse']?>&topicID='+id,
						success: function(data) {
							$('#subtopic').html(data);
						}
					});
					
				$.ajax({
						url: 'faculty_handler.php?action=getAssignments&topicID='+id,
						success: function(data) {
							$('#assignment').html(data);
						}
					});
				
			}
		</script>
		
		<table width="100%">
			<tr>
				<td width="30%">
					<div class="verticalContainer">
						<table width="100%">
							<tr class="ui-widget-header"><td colspan="2">Topic List</td></tr>
							<?php get_All_Topics($_GET['selectedCourse']) ?>
						</table>
					</div>
				</td>
				<td>
					<table width="100%">
						<tr><td colspan="2" style="padding-left:20px"><div id="brief"></div></td></tr>
						<tr>
							<td width="40%" style="padding-left: 5%; padding-right:5% "><div id="subtopic"></div></td>
							<td width="40%" style="padding-left: 5%; padding-right:5% "><div id="assignment"></div></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<?php
	}
if($_GET['action']=="view")
	{
		?>
		<script src="js/CKEditor/ckeditor.js"></script>
		<script>
			function checkbox()
			{
				if($('#issubtopic').is(':checked'))
				{
					loadData();
				}
				else
				{
					$('#gridcontainer').html("<table id='list2'></table><div id='pager2'></div>");
				}
			}
			function loadData()
			{
				if(!$('#issubtopic').is(':checked'))
				{
					return;
				}
				$('#gridcontainer').html("<table id='list2'></table><div id='pager2'></div>");
				jQuery("#list2").jqGrid({
					url:'faculty_handler.php?action=get_topics_all&courseid='+$('#course').val(),
					datatype: "json",
					colNames:['ID','Heading'],
					colModel:[
						{name:'id',index:'id', width:55},
						{name:'Heading',index:'Name', width:90}
					],
					rowNum:10,
					rowList:[10,20,30],
					pager: '#pager2',
					sortname: 'id',
					loadonce : false,
					viewrecords: true,
					sortorder: "desc",
					multiselect: false,
					altRows: true,	
					width: 400,
					height: 50,
					rownumbers: true,
					rownumWidth: 40,
					caption:"List of Topics",
					onSelectRow: function (id) 
					{
						$('#stopicid').val(id);
					}
				});
				jQuery("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false});			
			}
		</script>
		
		<input type="hidden" id="course" value="<?=$_GET['selectedCourse']?>"  />
		<table width="100%">
			<tr>
				<td width="20%">
					<div class="verticalContainer">
						<table width="100%">
							<tr class="ui-widget-header"><td align="center">Topic List</td></tr>
							<tr><td>
								<div id="demo1" class="demo" style="height:100px;">
									
								</div>
								<script type="text/javascript" class="source below">
								var treeloaded=0;
								$(function () {
									$("#demo1")
										.jstree({
											"json_data" : {
												"ajax" : { "url" : "./student_handler.php?action=get_tree_topics&selectedCourse=<?=$_GET['selectedCourse']?>" }
											},
											"plugins" : ["themes","json_data","ui"],
										})
										.bind("select_node.jstree", function (e, data) {
											if(treeloaded)
											{	
												window.location='./student_subtopics.php?action=view&selectedCourse=<?=$_GET['selectedCourse']?>&topicid='+data.rslt.obj.attr("id");
											}
										});
										
										$('#6').addClass('jstree-open');
								});
								
								setTimeout(function () { $("#demo1").jstree("set_focus"); }, 500);
								setTimeout(function () { $.jstree._focused().select_node("#<?=$_GET['topicid']?>"); treeloaded=1; }, 1000);
								</script>
							</td></tr>			
						</table>
					</div>
				</td>
				<td>
					<?php
						$topic=mysql_query("select * from topics where ID=".$_GET['topicid']);
						$topic=mysql_fetch_array($topic);
					?>

					<table width="100%">
					
						<tr class="ui-widget-header"><td align="center" colspan="2"><?php echo $topic['Heading'] ?></td></tr>
						<tr class="ui-widget-content"><td>
	<div class="topics_subtopics">
					   <?php echo $topic['ContentData'];?>
</div>
						</td></tr>
					   <?php $assignment_topic = mysql_query("select * from assignment where TopicID=".$_GET['topicid']);
					   $assignment_topic=mysql_num_rows($assignment_topic); /** Cargando Assignments **/

					   $chk = mysql_query("select * from student_topic_read where UserID ='" . $_SESSION['userid'] . "' and TopicID = '".$_GET['topicid']."' and CourseID = '".$topic['CourseID']."'");
										  if(mysql_num_rows($chk)==0){
					   mysql_query("insert into student_topic_read (UserID, TopicID, CourseID) values ('".$_SESSION['userid']."','".$_GET['topicid']."','". $topic['CourseID'] ."')");/** Guardando como leido **/
										  }
?>
						 <tr class="ui-widget-content"><td><center><input type="checkbox" checked disabled/><small>Mark this topic as Read | Assignments: <?php echo $assignment_topic; ?> | <a href="student_queries.php?selectedCourse=<?=$topic['CourseID']?>"><img src="images/icon_interrogation.png" width="15px"></a> Queries</small></center></td></tr>
					</table>					
				</td>
<td>
<table width="100%">
<tr class="ui-widget-header"><td align="center" colspan="2">Session Notes</td></tr>
<tr class="ui-widget-content"><td>
	<div class="topics_subtopics_notes">
<?php 
$notes = mysql_query("select * from topics_comments where StudentID ='" . $_SESSION['userid'] . "' and TopicID = '".$_GET['topicid']."' and CourseID = '".$topic['CourseID']."'");
if(isset($_POST['save_notes']))
  {
	if(mysql_num_rows($notes)==0){
	  mysql_query("insert into topics_comments (CourseID, TopicID, StudentID, Comments) values ('".$topic['CourseID']."','".$_GET['topicid']."','". $_SESSION['userid'] ."', '". $_POST['save_notes'] ."')");
	}else{
	  $notes = mysql_fetch_array($notes);
	  mysql_query("update topics_comments set Comments = '". $_POST['save_notes'] ."' where StudentID ='" . $_SESSION['userid'] . "' and TopicID = '".$_GET['topicid']."' and CourseID = '".$topic['CourseID']."'");
	}
  }
$notes = mysql_fetch_array($notes);
?>
<form name="show_notes" action="student_subtopics.php?action=view&selectedCourse=<?=$_GET['selectedCourse']?>&topicid=<?=$_GET['topicid']?>" method="POST">
<textarea name="save_notes" title="notes" cols="23" rows="26"><?php echo $notes['Comments']; ?></textarea><br />
<input type="submit" value="Save Notes"></input>
</form>
</div></td></tr>
</table>
</td>
			</tr>
		</table>
		</form>
		<?php
	}
	require_once("footer.php");
?>