<?php include "eportfoli_header.php";   
   require_once("mainFunctions.php");
   ?>
<style type="text/css">
   .liststyle li{  list-style: none;
   line-height:33px;
   font-size: 14px;
   color: #333;}
   .liststyle {  margin: 0;
   padding: 0;
   font-family: roboto;}
   .ui-accordion .ui-accordion-icons {
   padding-left: 2.2em;
   color: rgb(74, 39, 39);
   font-weight: bold;
   font-size: 14px;
   }
   #ui-id-2{ height:400px !important}
   #accordion-resizer {
   padding: 10px;
   width: 300px;
   height:150px;
   }
</style>
<?php 
   if ($_GET['selectedCourse']==""){
   
     if($_SESSION['courseID'] != "") $_GET['selectedCourse'] = $_SESSION['courseID'];
   
   } elseif ($_GET['selectedCourse']!=""){
   
     $_SESSION['courseID'] = $_GET['selectedCourse'];
   
   }
   
   
   ?>
<link rel="stylesheet" href="/resources/demos/style.css">
<div id="content" class="content">
   <div class="row">
      <!-- begin col-6 -->
      <div class="col-md-12 " style="min-height:500px;background: #fff;  border-radius: 3px;">
         <?php
            if($_GET['selectedCourse']=="")
            
               	{
            
            ?>
         <div class="box-shadow">
            <div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;float: left; width: 100%;">
               <!--Please select a course from the collapsible pane on the left of the screen.-->
               Program List
            </div>
            <div class="yui3-g">
               <table width="100%">
                  <?php getAllCoursesListInDiv("assignmentMark.php"); ?>
               </table>
            </div>
         </div>
         <?php
            }
            
            else
            
            {
            
            ?>
         <div id="leftmenu" class="col-md-3 list-group"  style="float:left;border-right: 1px solid #ccc;margin-left: -11px;"> </div>
         <div id="mainform" class="col-md-9"  >
            <div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">Marks
               <a href="unsetmarksdata.php" class="grid-button-edit yellow-button" style="float: right; font-size: 15px; cursor: pointer; text-decoration:none;"><span style="padding:10px;">Click Here To Change Program</span></a>
            </div>
            <div class="yui3-g" class="box-header" style="height:450px;overflow:auto;">
               <div class="box-header" style="height:150px">
                  Click on the Assignment list on the left to check your performance in it.
               </div>
            </div>
         </div>
         <?php
            }
             ?>       
      </div>
   </div>
</div>
<!-- end row -->
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
<i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->
<!-- ================== BEGIN BASE JS ================== -->
<script src="jsep/jquery-1.9.1.min.js"></script>
<script src="jsep/jquery-migrate-1.1.0.min.js"></script>
<script src="jsep/jquery-ui.min.js"></script>
<script src="jsep/bootstrap.min.js"></script>
<!--[if lt IE 9]>
<script src="assets/crossbrowserjs/html5shiv.js"></script>
<script src="assets/crossbrowserjs/respond.min.js"></script>
<script src="assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="jsep/jquery.slimscroll.min.js"></script>
<script src="jsep/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="jsep/apps.min.js"></script>
<script>
   $(document).ready(function() {
   	App.init();
   	
       $("#assingment_id").addClass('activeMenu');         	
   	
   });
   
   
   
   
   function loadAllEndedAssignments()
   
    {
         
   
   $.ajax({
   
   	url: 'student_handler.php?action=getEndedAssignmentForMenu&selectedCourse=<?=$_GET['selectedCourse']?>',
   
   	success: function(data) {
   
   		$('#leftmenu').html(data);
   
   	}
   
   });
   
   }
   
   function getAssignmentDetails(batchid,assid)
   
   {
   
   $.ajax({
   
     url: 'student_handler.php?action=getAssignmentDetails&assid='+assid+'&batchid='+batchid,
   
   	success: function(data) {
   
   		$('#mainform').html(data);
   
   	}
   
   });
   
   }
   
</script>
<?php 
   if($_GET['selectedCourse']=="")
       	{
      		
       	}
       	
      	 else 
      	   {
      	   	
      	   	 	?>
<script>  loadAllEndedAssignments();  </script>
<?php  }  ?>
</body>
</html>