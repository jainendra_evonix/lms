 <?php include "eportfoli_header.php"; ?>
<link rel="stylesheet" href="cssep/colorbox.css" />

 
  <style type="text/css">
  .clr{  border: 1px solid #ccc;
  padding: 4px;
  vertical-align: top;
  font-size: 13px;
  font-weight: bold;}
  
  .lk{font-family: Cambria, serif;}
  
  .borde{border: 1px solid #ccc;  vertical-align: top;}
  td{padding: 5px;}
  </style>

 
 
          <div id="content" class="content">
            <!-- begin breadcrumb -->
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Videos</h1>
            <!-- end page-header -->
            <!-- begin row -->
            <div class="row">
               <!-- begin col-6 -->
               <div class="col-md-12 " style="min-height:500px;background: #fff;  border-radius: 3px;">
               
               <br><br><br>
               
              

                
         <table border="0" cellpadding="0" align="left" style="width:95%;line-height: 21.299999237060547px;  border-collapse: collapse">
                
                
                
                
			<tbody style="line-height: 21.299999237060547px;">
				<tr style="line-height: 21.299999237060547px; height: 46.5pt;">
				
					<td class="borde">
				
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">1</font></span></p></td>
					<td class="borde" style="width: 40% !important;">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Dr. Rajiv Yeravdekar,<br style="line-height: 21.299999237060547px;">
					Dean, Faculty of Health &amp; Biomedical Sciences, Symbiosis 
					International University</font></span></p></td>
					
					<td class="clr" style="width: 43% !important;">
					
					<a  class="lk youtube cboxElement" target="_blank" href="https://www.youtube.com/embed/QOP3b-1wFck">Key Note Address: Pre Conference Symposium</a>
					
					</td>
					
					<td class="clr"> 
					<a  class="youtube cboxElement" href="https://www.youtube.com/embed/QOP3b-1wFck" 
					rel="prettyPhoto" >
					
					   <img src="http://img.youtube.com/vi/QOP3b-1wFck/1.jpg" width="130" height="95" alt="" /></a>
					
					 </td>
				</tr>
				
				
				
				
				
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
				
					<td width="33" rowspan="2" class="borde">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">2</font></span></p></td>
					
					
					<td rowspan="2" class="borde" style="width:200px !important">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Dr. Harish Pillai<br style="line-height: 21.299999237060547px;">
					CEO, Aster DM Healthcare</font></span></p></td>
					
					<td align="left" valign="top" class="borde">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a target="_blank" class="lk youtube cboxElement"  href="https://www.youtube.com/embed/sHcT2tx8iPs">Successful Healthcare Models: Hospital- Part 
					1</a></font></span></u></b></p></td>
					
					
						
					<td class="clr"> <a  class="lk youtube cboxElement"  href="https://www.youtube.com/embed/sHcT2tx8iPs" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/sHcT2tx8iPs/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
				
					<td class="borde">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					
					<a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/Kfm2_AXDJJI">Successful Healthcare Models: Hospital- Part 
					2</a></font></span></u></b></p>
					
					</td>
						
					<td class="clr"> <a class="lk youtube cboxElement"  href="https://www.youtube.com/embed/Kfm2_AXDJJI" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/Kfm2_AXDJJI/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
				</tr>
				
				
				
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td width="33" class="borde" valign="top" align="left">
					
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">3</font></span></p></td>
					
					
					<td class="borde" align="left" valign="top">
					
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Mr. Sudarshan Jain<br style="line-height: 21.299999237060547px;">
					&nbsp;MD, Healthcare Solutions, Abott India</font></span></p>
					
					</td>
					<td class="borde" align="left" valign="top">
					
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					
					<a class="lk youtube cboxElement"  href="https://www.youtube.com/embed/k5jyqsM7_I4">Successful Healthcare Models: Pharma</a></font></span></u></b></p></td>
					
					<td class="clr"> <a  class="lk youtube cboxElement"   href="https://www.youtube.com/embed/k5jyqsM7_I4" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/k5jyqsM7_I4/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
				
					<td width="33"  valign="top" align="left" class="borde">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">4</font></span></p></td>
					
					
					<td class="borde" align="left" valign="top">
					
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Mr. Sadananda Reddy,<br style="line-height: 21.299999237060547px;">
					MD, Goldstar Healthcare Private Limited</font></span></p></td>
					
					<td class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/_uACrcTSd0A">Successful Healthcare Models: IT</a></font></span></u></b></p></td>
					
					<td class="clr"> <a class="lk youtube cboxElement"  href="https://www.youtube.com/embed/_uACrcTSd0A" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/_uACrcTSd0A/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td width="33" class="borde" valign="top" align="left">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">5</font></span></p></td>
					
					<td class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Dr. G.S.K. Velu<br style="line-height: 21.299999237060547px;">
					Managing Director, Trivitron Group</font></span></p>
					</td>
					
					<td class="borde"align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement"  href="https://www.youtube.com/embed/xnTsd0HMLW8">Successful Healthcare Models: Medical Devices</a></font></span></u></b></p>
					</td>
					
					<td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/xnTsd0HMLW8" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/xnTsd0HMLW8/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td width="33" class="borde" valign="top" align="left">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">6</font></span></p></td>
					
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Mr. Daljit&nbsp; Singh, President , 
					Fortis Healthcare Ltd.</font></span></p></td>
					
					<td class="borde"align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/kqjDl7ePSSY">Strategic Management in Healthcare</a></font></span></u></b></p></td>
					
     					<td class="clr"> <a  class="lk youtube cboxElement"  href="https://www.youtube.com/embed/kqjDl7ePSSY" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/kqjDl7ePSSY/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td width="33" class="borde" valign="top" align="left">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">7</font></span></p>
					</td>
					
					<td class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Dr. Gautam Sen<br style="line-height: 21.299999237060547px;">
					Chairman Wellspring Healthcare &amp;<span class="Apple-converted-space">&nbsp;</span><br style="line-height: 21.299999237060547px;">
					Mr.Kaushik Sen<br style="line-height: 21.299999237060547px;">
					Co-founder, Wellspring&nbsp; Healthcare</font></span></p></td>
					
					
					
					<td class="borde"align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					
					<a class="lk youtube cboxElement" href="https://www.youtube.com/watch?v=mWcA6zenyvQ">Game Changers in Healthcare: Primary 
					Healthcare</a></font></span></u></b></p></td>
					
					<td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/watch?v=mWcA6zenyvQ" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/mWcA6zenyvQ/1.jpg" width="130" height="95" alt="" /></a>
					 </td>
					 
					 
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td width="33" class="borde" valign="top" align="left">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">8</font></span></p></td>
					
					
					<td class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Dr. Om Manchanda,CEO, Dr Lal 
					PathLabs</font></span></p></td>
					
					<td class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement"  href="https://www.youtube.com/embed/gRyE_-heXIk">Game Changers in Healthcare: Diagnostics</a></font></span></u></b></p></td>
					
						<td class="clr"> <a  class="lk youtube cboxElement"  href="https://www.youtube.com/embed/gRyE_-heXIk" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/gRyE_-heXIk/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td width="33" class="borde" valign="top" align="left">
					
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">9</font></span></p></td>
					
					
					<td class="borde" align="left" valign="top">
					
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Dr. Rajiv Yeravdekar,<br style="line-height: 21.299999237060547px;">
					Dean, Faculty of Health &amp; Biomedical Sciences, Symbiosis 
					International University</font></span></p></td>
					
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/MfHLPUsWBBA">Healthcare Delivery Systems</a></font></span></u></b></p></td>
					
					<td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/MfHLPUsWBBA" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/MfHLPUsWBBA/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td width="33" rowspan="2" class="borde" valign="top" align="left">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">10</font></span></p></td>
					
					<td rowspan="2" class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Padmashri Dr. Azad Moopen<br style="line-height: 21.299999237060547px;">
					Chairman, Aster DM Healthcare</font></span></p></td>
					
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/K4T8KNVKw88">Challenges in Workforce Management in 
					Hospitals - Part 1</a></font></span></u></b></p></td>
					
					<td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/K4T8KNVKw88" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/K4T8KNVKw88/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/J4Bv5xwFHus">Challenges in Workforce Management in 
					Hospitals - Part 2</a></font></span></u></b></p></td>
					
					<td class="clr"> <a  target="blank" href="https://www.youtube.com/embed/J4Bv5xwFHus" rel="prettyPhoto"><img src="http://img.youtube.com/vi/J4Bv5xwFHus/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td width="33" rowspan="4" class="borde" valign="top" align="left">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">11</font></span></p></td>
					
					<td rowspan="4" class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Mr. Pradeep Thukral&nbsp;<span class="Apple-converted-space">&nbsp;</span><br style="line-height: 21.299999237060547px;">
					Founder &amp; CEO&nbsp; SafeMedTrip Consultant Pvt.Ltd.</font></span></p></td>
					<td  align="left" valign="top" class="borde">
					
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/3dXppHM1eYw">Medical Tourism: Present and Future - Part 1</a></font></span></u></b></p></td>
					
					<td class="clr"> <a  class="lk youtube cboxElement" target="blank" href="https://www.youtube.com/embed/3dXppHM1eYw" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/3dXppHM1eYw/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
					
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/vCB3t-XRTok">Medical Tourism: Present and Future - Part 2</a></font></span></u></b></p></td>
					
	 <td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/vCB3t-XRTok" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/vCB3t-XRTok/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
									
  					
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a target="_blank" class="lk youtube cboxElement" href="https://www.youtube.com/embed/u6WcyuOuVWA">Medical Tourism: Present and Future - Part 3</a></font></span></u></b></p></td>
					
					 <td class="clr"> <a  class="lk youtube cboxElement" target="blank" href="https://www.youtube.com/embed/u6WcyuOuVWA" rel="prettyPhoto" >
					 <img src="http://img.youtube.com/vi/u6WcyuOuVWA/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/oR4V2XMt_Qo">Medical Tourism: Present and Future - Part 4</a></font></span></u></b></p></td>
					
   <td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/oR4V2XMt_Qo" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/oR4V2XMt_Qo/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>					
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td width="33" rowspan="3" class="borde" valign="top" align="left">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">12</font></span></p></td>
					
					
					<td rowspan="3" class="borde" align="left" valign="top">
					
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Dr. Yash Paul Bhatia<br style="line-height: 21.299999237060547px;">
					MD Astron Healthcare&nbsp;</font></span></p></td>
					
					
					<td class="borde" align="left" valign="top">
					
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a  class="lk youtube cboxElement"  href="https://www.youtube.com/embed/0O-AqLvlkQE">Quality &amp; Accreditation of Hospital &amp; 
					Healthcare - Part 1</a></font></span></u></b></p></td>
					
					 <td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/0O-AqLvlkQE" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/0O-AqLvlkQE/1.jpg" width="130" height="95" alt="" /></a>
					 </td>	
				</tr>
				
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/VWVtusm1UDo">Quality &amp; Accreditation of Hospital &amp; 
					Healthcare - Part 2</a></font></span></u></b></p></td>
					 <td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/VWVtusm1UDo" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/VWVtusm1UDo/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/P_wZ3j9-esw">Quality &amp; Accreditation of Hospital &amp; 
					Healthcare - Part 3</a></font></span></u></b></p></td>
					
					 <td class="clr"> <a   class="lk youtube cboxElement" href="https://www.youtube.com/embed/P_wZ3j9-esw" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/P_wZ3j9-esw/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td width="33" rowspan="3" class="borde" valign="top" align="left">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin-left: 0px; margin-right: 0px; margin-top: 0px; margin-bottom: 1.35em">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">13<br style="line-height: 21.299999237060547px;">
					<br style="line-height: 21.299999237060547px;">
					<br style="line-height: 21.299999237060547px;">
					<br style="line-height: 21.299999237060547px;">
					14</font></span></p></td>
					<td rowspan="3" class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Dr. Shubnum Singh<br style="line-height: 21.299999237060547px;">
					Chairperson, Healthcare Sector Skill Council, Advisory 
					Committee<br style="line-height: 21.299999237060547px;">
					<br style="line-height: 21.299999237060547px;">
					Dr Sandip Ahuja<br style="line-height: 21.299999237060547px;">
					MD VLCC &amp; Chairman, FICCI National Committee</font></span></p></td>
					
					<td class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" target="_blank" href="https://www.youtube.com/embed/0vRFaX5Fm78">Innovation in addressing Capacity Gap &amp;<br style="line-height: 21.299999237060547px;">
					Introduction to HSSC - Part 1</a></font></span></u></b></p></td>
					
               <td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/0vRFaX5Fm78" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/0vRFaX5Fm78/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>					
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/RYyaHGdhGsg">Innovation in addressing Capacity Gap &amp;<br style="line-height: 21.299999237060547px;">
					Introduction to HSSC - Part 2</a></font></span></u></b></p></td>
					
					 <td class="clr"> <a class="lk youtube cboxElement" href="https://www.youtube.com/embed/RYyaHGdhGsg" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/RYyaHGdhGsg/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					 
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 45pt;">
					<td class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/_X-Bx7LGXXs">Innovation in addressing Capacity Gap &amp;<br style="line-height: 21.299999237060547px;">
					Introduction to HSSC - Part 3</a></font></span></u></b></p></td>
					
						 <td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/_X-Bx7LGXXs" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/_X-Bx7LGXXs/1.jpg" width="130" height="95" alt="" /></a>
					 </td>
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td width="33" rowspan="4" class="borde" valign="top" align="left">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">15</font></span></p></td>
					<td rowspan="4" class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Dr. Shreeraj Deshpande,Head, 
					Health Insurance<br style="line-height: 21.299999237060547px;">
					Future Generali India Insurance Company Ltd</font></span></p></td>
					
					<td class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/OpxHGoMh3WY">Health Insurance: Opportunities and 
					Challenges - Part 1</a></font></span></u></b></p></td>
					
					 <td class="clr"> <a   class="lk youtube cboxElement" href="https://www.youtube.com/embed/OpxHGoMh3WY" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/OpxHGoMh3WY/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/-zVURFZzFjU">Health Insurance: Opportunities and 
					Challenges - Part 2</a></font></span></u></b></p></td>
					
					 <td class="clr"> <a   class="lk youtube cboxElement" href="https://www.youtube.com/embed/-zVURFZzFjU" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/-zVURFZzFjU/1.jpg" width="130" height="95" alt="" /></a>
					 </td>
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/Y1Y5luWZ0jI">Health Insurance: Opportunities and 
					Challenges - Part 3</a></font></span></u></b></p></td>
					
					 <td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/Y1Y5luWZ0jI" rel="prettyPhoto" ><img src="images/c12891.png" width="130" height="95"  alt="" /></a>
					 </td>
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/p0VkqwxrgmM">Health Insurance: Opportunities and 
					Challenges - Part 4</a></font></span></u></b></p></td>
              <td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/p0VkqwxrgmM" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/p0VkqwxrgmM/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>					
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td width="33" rowspan="4" class="borde" valign="top" align="left">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font size="2">16</font></span></p></td>
					<td rowspan="4" class="borde" align="left" valign="top">
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: black;">
					<font style="font-size: 11pt">Dr. Gopinath N. Shenoy<br style="line-height: 21.299999237060547px;">
					Medico Legal Consultant</font></span></p></td>
					
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/iOpuLS_ey2U">Legal Aspects of Healthcare: Landmark 
					Judgements - Part 1</a></font></span></u></b></p></td>
					
					 <td class="clr"> <a   class="lk youtube cboxElement" href="https://www.youtube.com/embed/iOpuLS_ey2U" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/iOpuLS_ey2U/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					
					
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/4Ccjywajlns">Legal Aspects of Healthcare: Landmark 
					Judgements - Part 2</a></font></span></u></b></p></td>
					
					 <td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/4Ccjywajlns" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/4Ccjywajlns/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/5_TCQjFggMA">Legal Aspects of Healthcare: Landmark 
					Judgements - Part 3</a></font></span></u></b></p></td>
					
					 <td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/5_TCQjFggMA" rel="prettyPhoto" ><img src="http://img.youtube.com/vi/5_TCQjFggMA/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
					
				</tr>
				<tr style="line-height: 21.299999237060547px; height: 40.5pt;">
					<td class="borde" align="left" valign="top">
					<p class="ecxMsoNormal" style="line-height: 21.299999237060547px; margin: 0px 0px 1.35em;">
					<b style="line-height: 21.299999237060547px; font-weight: bold;">
					<u style="line-height: 21.299999237060547px;">
					<span style="line-height: 21.299999237060547px; font-family: Cambria, serif; color: rgb(0, 112, 192);">
					<font size="2">
					<a class="lk youtube cboxElement" href="https://www.youtube.com/embed/S6iJ0SGNEVY">Legal Aspects of Healthcare: Landmark 
					Judgements - Part 4</a></font></span></u></b></p></td>
					
						 <td class="clr"> <a  class="lk youtube cboxElement" href="https://www.youtube.com/embed/S6iJ0SGNEVY" rel="prettyPhoto"><img src="http://img.youtube.com/vi/S6iJ0SGNEVY/1.jpg" width="130" height="95"  alt="" /></a>
					 </td>
					
				</tr>
		</tbody></table>
                
                  </div>
               </div>
               <!-- end col-6 -->
               <!-- begin col-6 -->
               <!-- end col-6 -->
            </div>
            <!-- end row -->
         </div>
         <!-- end #content -->
         <!-- begin theme-panel -->
         <!-- end theme-panel -->
         <!-- begin scroll to top btn -->
         <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
         <!-- end scroll to top btn -->
      </div>
      <!-- end page container -->
      <!-- ================== BEGIN BASE JS ================== -->
      <script src="jsep/jquery-1.9.1.min.js"></script>
      
      
      
      <script src="jsep/jquery-migrate-1.1.0.min.js"></script>
      <script src="jsep/jquery-ui.min.js"></script>
      <script src="jsep/bootstrap.min.js"></script>
      <!--[if lt IE 9]>
      <script src="assets/crossbrowserjs/html5shiv.js"></script>
      <script src="assets/crossbrowserjs/respond.min.js"></script>
      <script src="assets/crossbrowserjs/excanvas.min.js"></script>
      <![endif]-->
      <script src="jsep/jquery.slimscroll.min.js"></script>
      <script src="jsep/jquery.cookie.js"></script>
      <!-- ================== END BASE JS ================== -->
      <!-- ================== BEGIN PAGE LEVEL JS ================== -->
      <script src="jsep/apps.min.js"></script>
      
          <script src="jsep/jquery.colorbox.js"></script>
      
      <!-- ================== END PAGE LEVEL JS ================== -->
      <script>
         $(document).ready(function() {
         	App.init();
         });
      </script>
      
      <script>
        $(document).ready(function(){
            //Examples of how to assign the Colorbox event to elements
            $(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
            
        });
    </script>
      
      
      
      
   </body>
</html>