<!-- for upload file 
<script src="js/jquery-1.8.3.js"></script>-->
<script type="text/javascript" src="js/ajaxupload.3.5.js" ></script>
<script type="text/javascript" >
	$(function(){
		var btnUpload=$('#me');
		var mestatuserror=$('#mestatuserror');
		var mestatus=$('#mestatus');
		var files=$('#files');
		new AjaxUpload(btnUpload, {
			action: 'uploadassignments.php?doctype=app&selectedCourse=<?=$_GET['selectedCourse']?>',
			multiple:true,
			name: 'uploadfile',
			onSubmit: function(file, ext){
				 if (! (ext && /^(pdf)$/.test(ext))){
					// extension is not allowed
					$('#mestatus').html("");
					$('#files').html("");
					$('#prefiles').html("");
					mestatuserror.text('Only .pdf files are allowed');
					return false;
				}
				mestatus.html('<img src="ajax-loader.gif" height="16" width="16">');
			},
			onComplete: function(file, response){
				//On completion clear the status
				$('#mestatuserror').html("");
				mestatus.text('File Uploaded Sucessfully!');
				//On completion clear the status
				files.html('');
				//Add uploaded file to list
				if(response==="success"){
					$('#prefiles').html("");
					$('<br><textarea name="assname2" id="assname2" style="resize: none;" readonly="readonly"></textarea>').appendTo('#files').html(file).addClass('success');
				} else{
					$('<span></span>').appendTo('#files').text(file).addClass('error');
				}
				$("#assname2").val(file);
				
			}
		});
		
	});
</script>