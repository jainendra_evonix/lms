<?php include "eportfoli_header.php";   
   require_once("mainFunctions.php");
   ?>

<style type="text/css">
   .liststyle li{  list-style: none;
   line-height:33px;
   font-size: 14px;
   color: #333;}
   .liststyle {  margin: 0;
   padding: 0;
   font-family: roboto;}
   .ui-accordion .ui-accordion-icons {
   padding-left: 2.2em;
   color: rgb(74, 39, 39);
   font-weight: bold;
   font-size: 14px;
   }
   #ui-id-2{ height:400px !important}
   #accordion-resizer {
   padding: 10px;
   width: 300px;
   height:150px;
   }
</style>
<?php 
   // Check session 
     if ($_GET['selectedCourse']==""){
   
     if($_SESSION['courseID'] != "") $_GET['selectedCourse'] = $_SESSION['courseID'];
   
   } elseif ($_GET['selectedCourse']!=""){
   
     $_SESSION['courseID'] = $_GET['selectedCourse'];
   
   }
   
   ?>
<link rel="stylesheet" href="/resources/demos/style.css">
<div id="content" class="content">
  
   <div class="row">
      <!-- begin col-6 -->
      <div class="col-md-12 " style="min-height:500px;background: #fff;  border-radius: 3px;">
         <div style="display:none">
            <div id="data"></div>
         </div>
         <?php
            if($_GET['selectedCourse']=="")
            
            {
            
            ?>
         <div class="yui3-g" style="margin-top:25px;">
            <div class="box-shadow" >
               <div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
                  <!--Please select a course from the collapsible pane on the left of the screen.-->
                  Program List
               </div>
               <div class="yui3-g" style="float: left; width: 71%; margin-top:1px;">
                  <table width="100%">
                     <?php getAllCoursesListInDiv("project.php"); ?>
                  </table>
               </div>
            </div>
         </div>
         <?php
            }
            
            else
            
            {
            
            ?>
         <div class="col-md-12">
         
            <div id="leftmenu" class="col-md-3 list-group ">   </div>
          
           
            
            
            <div id="mainform" class=" col-md-9" >
               <div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
                  <?php get_Subject_Name($_SESSION['courseID']) ?> Assignment Details
                  <a href="unsetassigndata.php" class="grid-button-edit yellow-button"
                   style="float: right; font-size: 15px; cursor: pointer; text-decoration:none;">
                   <span style="padding:10px;">Click Here To Change Program</span></a>
               </div>
               <div class="yui3-g" class="box-header" style="height:450px;overflow:auto;">
                  <div class="box-header" style="height:150px">
                     Click on the Assignment list on the left to load Assignment details here.
                  </div>
               </div>
            </div>
         </div>
        
      </div>
   </div>
</div>
<!-- end row -->
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
<i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->
<!-- ================== BEGIN BASE JS ================== -->
<script src="jsep/jquery-1.9.1.min.js"></script>
<script src="jsep/jquery-migrate-1.1.0.min.js"></script>
<script src="jsep/jquery-ui.min.js"></script>
<script src="jsep/bootstrap.min.js"></script>
<!--[if lt IE 9]>
<script src="assets/crossbrowserjs/html5shiv.js"></script>
<script src="assets/crossbrowserjs/respond.min.js"></script>
<script src="assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="jsep/jquery.slimscroll.min.js"></script>
<script src="jsep/jquery.cookie.js"></script>

<!-- ================== END BASE JS ================== -->
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="jsep/apps.min.js"></script>
<script>
   $(document).ready(function() {
   	App.init();
   	
       $("#assingment_id").addClass('activeMenu');         	
   	
   });
</script>

<script>



		function loadAllAssignments()
       {
              
          
			$.ajax({
        	url: 'student_handler.php?action=getAssignmentForMenu&selectedCourse=<?=$_GET['selectedCourse']?>&selectedBatchID=<?=$_GET['selectedBatchID']?>',
      	success: function(data) {

					$('#leftmenu').html(data);

				}

			});

		}

		function loadTopicAssignments(topicid)

		{

			$.ajax({

				url: 'student_handler.php?action=getAssignmentForMenu&selectedCourse=<?=$_GET['selectedCourse']?>-'+topicid,

				success: function(data) {

					$('#leftmenu').html(data);

				}

			});

		}

		function setAssignment(assid,bid)

		{

			$.ajax({

				url: 'student_handler.php?action=getAssignment&assid='+assid+'&batchid='+bid,

				success: function(data) {

					$('#mainform').html(data);

				}

			});

		}

		function startTest(assid)

		{

			$.ajax({

				url: 'student_handler.php?action=getQestion&assid='+assid,

				success: function(data) {

					$('#data').html(data);

				}

			});			

		}

		function saveQuestion(assid,questionid)

		{

			var op="";

			var tID=$('#totalID').val();

			

			for(i=1;i<tID;i++)

			{

				iid="#option"+i;

				if(iid!="")

				{

					if($(iid).is(':checked'))

					{

						op+=$(iid).val()+",";

					}	

				}

			}

			

			if(op=="")

			{

				alert("Please select option to submit question");

				return;

			}

			

			$.ajax({

				url: 'student_handler.php?action=saveAnswer&assid='+assid+'&questionid='+questionid+'&options='+op,

				success: function(data) {

					startTest(assid);

				}

			});

		}

	</script>
	
	   <?php
        if(!isset($_GET['topicid']))
            
            {
            
            ?>
         <script>
            loadAllAssignments();
          
            
         </script>
         <?php 
            }
            
            else if(isset($_GET['topicid']))
            
            {
            
            ?>
         <script>
            loadTopicAssignments(<?=$_GET['topicid'] ?>);
            
         </script>
         <?php 
            }
             }
            ?>

</body>
</html>