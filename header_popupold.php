<?php
session_start();
require_once("mainFunctions.php");
?>
<html>
<head>
<link rel="stylesheet" href="css/cupertino/jquery-ui.css">
  <!--<link rel="stylesheet" href="css/cupertino/jquery.ui.theme.css">-->
  <link rel="stylesheet" href="css/customStyler.css">
  <link rel="stylesheet" href="css/ui.jqgrid.css">
  <link rel="stylesheet" href="css/ui.multiselect.css">
  <link rel="stylesheet" href="fancybox/jquery.fancybox.css">
  <link rel="stylesheet" href="css/yui.gridlayout.min.css"/>
	
  
  <script src="js/jquery-1.8.3.js"></script>
  <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="js/grid.locale-en.js"></script>
  <script src="js/jquery.jqGrid.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.validate.js"></script>
  <script src="js/jquery-ui-timepicker-addon.js"></script>
  <script src="js/jquery-ui-sliderAccess.js"></script>
  <script src="js/jsTree/_lib/jquery.cookie.js"></script>
  <script src="js/jsTree/_lib/jquery.hotkeys.js"></script>
  <script src="js/jsTree/jquery.jstree.js"></script>
  <script src="fancybox/jquery.fancybox.pack.js"></script>
  <script src="fancybox/jquery.mousewheel-3.0.6.pack.js"></script>

  <script>
  /*** CESAR JUAREZ - OPEN COMET ***/
  function start_test(url){

  window.open(url,'Exam');
}

function close_test(){
  if(confirm('Are you sure of submit the test?')){
	opener.location.href='student_marks.php'; 
	window.close();
  }
}

function cuentaRegresiva(){
  var min = document.fcuentareg.tiempo_min.value;
  var sec = document.fcuentareg.tiempo_sec.value;
  if(sec > 0){
	sec--;
  }else{
	if(min > 0){
	  min--;
	  sec = 59;
	}else{
	  alert("Your assignment time is over, the questions you have marked are recorded successfully.");
	  opener.location.href='student_marks.php'; 
	  window.close();
	}
  }
  document.fcuentareg.tiempo_min.value=min;
  document.fcuentareg.tiempo_sec.value=sec;

  if(typeof document.formsend != "undefined"){
  document.formsend.guarda_min.value=min;
  document.formsend.guarda_sec.value=sec;
  }
  if(typeof document.formsendback != "undefined"){
	document.formsendback.guarda_min_back.value=min; /** valores para que retroceda **/
	document.formsendback.guarda_sec_back.value=sec; /** valores para que retroceda **/
  }
  if(typeof document.form_review != "undefined"){
	document.form_review.guarda_min_review.value=min; /** valores para que retroceda **/
	document.form_review.guarda_sec_review.value=sec; /** valores para que retroceda **/
  }

  setTimeout("cuentaRegresiva()",1000);
}

/**********************************************************************************************/

$(document).ready(
				  function() { 
		  	
					$('#activateFrom').datetimepicker({ dateFormat: 'yy-mm-dd' });
					$("#inline").fancybox({
					  'transitionIn'	:	'elastic',
						  'transitionOut'	:	'elastic',
						  'speedIn'		:	600, 
						  'speedOut'		:	200, 
						  'overlayShow'	:	false,
						  ajax : {
						url: 'student_handler.php?action=getQestion'
							}
					  });
			
				  }
				  );
$(function() {
	$( "#radio" ).buttonset();
  });
/*$(function() {
  $( "input[type=submit], #aa, button" )
  .button()
  });*/
$(function() {
	$( document ).tooltip({
	  track: true
		  });
			
  });
$(function() {
	$( "#dialog-message" ).dialog({
	  modal: true,
		  buttons: {
		Ok: function() {
			$( this ).dialog( "close" );
		  }
		}
	  });
  });
		
</script>
<style>
body { margin:0px; font-family: calibri,sans-serif; font-size: 12pt; background-color: #F0F0F1; line-height: 1.5em; overflow-x: hidden;}
.page-wrap {
position: relative; 
width: 1000px;
margin: 0px auto; 
padding: 0px; 
 }
.menu-holder {
height: 40px;
position: relative; 
margin: 0px auto;
padding:0px 0px 2px 0px;
width: 680px;
 }
.topbackground{
background:url(images/topbar-bg.jpg) repeat-x;
height: 92px;
position: absolute;
top: 0px;
width:100%;
 }
.ui-tooltip
{
  font-size:10pt;
}
#menu {padding:0; margin:0; list-style:none;}
#menu li {float:left; margin-left:1px;}
#menu li a {display:block; height:40px; line-height:40px; padding:0 20px; float:left; background:#ddd; color:#000; text-decoration:none;font-family: arial,sans-serif; font-size:10pt;}
#menu li a b {text-transform:uppercase;}
#menu li a:hover, #menu li a.active
{background: #fff url(images/arrow.gif) no-repeat center bottom; color:#e60;}
.box-shadow {
background: #fff;
border:solid 1px #dddddd;
  -moz-border-radius: 5px;
  -webkit-border-radius:5px;
  border-radius:5px;
  -moz-box-shadow: 0px 0px 5px #000000;
  -webkit-box-shadow: 0px 0px 5px #000000;
  box-shadow: 0px 0px 5px #000000;
 }
.box-header{
padding: 5px 3px;
  font-size: 16pt;
color: #D04646;
  text-align:center;
 }
.big-number-text{
  line-height:1.5em;
  font-size: 30pt;
color: #45494A;
 }
.grid-small-text{
padding:5px;
  font-size:10pt;
  text-align:center;
  vertical-align:middle;
 }
.green-button{
  background-color: #8CCB63;
border: solid 1px #43aa00;
 }
.yellow-button{
width:80px !important;
height:25px !important;
margin:0px auto;
  background-color: #F7B921;
border: solid 1px #e6b200;
 }
.grid-button-edit{
width: 50px; 
height: 20px;
color:white;
  -moz-border-radius: 5px;
  -webkit-border-radius:5px;
  border-radius:5px;
 }
.grid-button-edit:hover{
  background-color: #6ac72e;
 }
.row-items{
  border-bottom: solid 1px #ddd;
 }
.ui-jqgrid tr.jqgrow td {font-size:0.8em}
.ui-jqgrid-labels {font-size:0.8em }
.ui-jqgrid-pager{font-size:0.7em !important}
</style>
<title>Pre-Induction Portal</title>
</head>
<body>
<div class="topbackground">&nbsp;</div>
<div class="page-wrap">