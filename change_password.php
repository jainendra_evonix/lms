<?php
	require_once("header.php");
?>

<?php

if($_GET['action'] == "changepassword")
{
  $usr = mysql_query("SELECT * FROM user WHERE ID = '". $_SESSION['userid'] ."'");

  $rec_usr = mysql_fetch_array($usr);
  if($_POST['oldPassword'] != $rec_usr['Password'] || $_POST['newPassword'] != $_POST['confNewPassword']){
?>
<script>
	  alert('the old password is not correct or the new password confirmation is incorrect');
	window.location='./change_password.php';
</script>
<?php
	}else{
	mysql_query("UPDATE user SET Password = '". $_POST['newPassword'] ."' WHERE ID = ". $_SESSION['userid']);
	mysql_query("insert into user_login_logout_status(userid,status) values('".$_SESSION['userid']."',2)");
?>
<script>
	  alert('Your password was successfully updated');
	window.location='./index.php';
</script>
<?php
  }

}

if($_GET['action'] == "")
  {
?>

<div class="yui3-g" style="margin-top:25px;">

<div class="box-shadow"  style="width:75%;margin:0px auto;">
			<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
				Change Password
			</div>
			<div class="yui3-g">

<form name="chngPassword" action='./change_password.php?action=changepassword' method='post'>
				<table width="100%" align="center">
<tr>
<td style='padding-left: 15px;'>Old Password</td>
<td><input type='password' name='oldPassword' size='30' /></td>
</tr>
<tr>
<td style='padding-left: 15px;'>New Password</td>
<td><input type='password' name='newPassword' size='30' maxlength='25' /></td>
<div class="form-error" id='chngPassword_newPassword_errorloc' ></div>
</tr>
<tr>
<td style='padding-left: 15px;'>Confirm New Password</td>
<td><input type='password' name='confNewPassword' size='30' maxlength='25' /></td>
<div class="form-error" id='chngPassword_confNewPassword_errorloc' ></div>
</tr>
<tr>
<td></td>
<td>
<input type='submit' value='Accept'/>
</td>
</tr>
				</table>
</form>
	<script language="JavaScript" type="text/javascript">
		//You should create the validator only after the definition of the HTML form
		  var frmvalidator  = new Validator("chngPassword");
		 frmvalidator.EnableOnPageErrorDisplay();
		frmvalidator.EnableMsgsTogether();
		 
		  frmvalidator.addValidation("newPassword","regexp=^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{5,25}$","New Password should have atleast 1 digit and 1 Special character(!@#$%^&*) and should be between 5 and 25 characters.");
		  frmvalidator.addValidation("confNewPassword","eqelmnt=newPassword", "The confirmed password is not same as New Password");
		  frmvalidator.addValidation("newPassword","neelmnt=oldPassword", "The New password should not be same as Old Password");
		  
	</script>
			</div>
		</div>

</div>
<script>
	$(document).ready(function(){
		$("li#menu-password a").addClass("active");
	});			
</script>
<?php
	  }
	require_once("footer.php");
?>