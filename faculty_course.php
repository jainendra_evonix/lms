<?php
	require_once("header.php");
	require_once("mainFunctions.php");
	?>
	<div class="yui3-g" style="margin-top:25px;">
		<div class="box-shadow"  style="width:75%;margin:0px auto;">
			<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
				Program List
			</div>
			<div class="yui3-g">
				<table width="100%" align="center">
					<?php get_Faculty_Course_Subjects(); ?>
				</table>
			</div>
		</div>
	</div>
	<script>
			$(document).ready(function(){
				$("li#menu-course a").addClass("active");
			});			
		</script>
	<?php
	require_once("footer.php");
?>