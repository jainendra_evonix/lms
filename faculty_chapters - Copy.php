<?php
	require_once("header.php");
	require_once("mainFunctions.php");
	?>
	<nav>
		<ul>
			<?php getAllCoursesList("faculty_chapters.php"); ?>
		</ul>
	</nav>
	<?php


	 /** CESAR JUAREZ - OPEN COMET **/

	if ($_GET['selectedCourse']==""){
	  if($_SESSION['courseID'] != "") $_GET['selectedCourse'] = $_SESSION['courseID'];
	} elseif ($_GET['selectedCourse']!=""){
	  $_SESSION['courseID'] = $_GET['selectedCourse'];
	}

	echo $_SESSION['selectedCourse'];

	if($_GET['action']=="")
	{
		?>
		<script>
			function confirmDelete(topicid)
			{
				if(confirm("Deleting this topic would delete the topic, related sub-topics and all the associated assignments.\nAre you sure you want to dete this Topic?"))
				{
					//alert('faculty_handler.php?action=deleteTopic&topicID='+topicid);
					$.ajax({
						url: 'faculty_handler.php?action=deleteTopic&topicID='+topicid,
						success: function(data) {
							window.location.href = './faculty_chapters.php';
						}
					});
				}
			}
			function topid(id)
			{
				$.ajax({
						url: 'faculty_handler.php?action=getTopic&topicID='+id,
						success: function(data) {
							$('#brief').html(data);
						}
					});
					
				$.ajax({
						url: 'faculty_handler.php?action=getSubTopics&selectedCourse=<?=$_GET['selectedCourse']?>&topicID='+id,
						success: function(data) {
							$('#subtopic').html(data);
						}
					});
					
				$.ajax({
						url: 'faculty_handler.php?action=getAssignments&topicID='+id,
						success: function(data) {
							$('#assignment').html(data);
						}
					});
				
			}
		</script>
		
		<div class="yui3-g" style="margin-top:25px;">
			<div class="yui3-u-1-5 box-shadow"  style="float:left;">
				<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
					Module List
				</div>
				<div class="yui3-g" style="height:420px;overflow:auto;">
						<table width="100%">
							<?php get_All_Topics($_GET['selectedCourse']) ?>
						</table>
				</div>
			<?php if(isset($_GET['selectedCourse']) || $_GET['selectedCourse']!=""){ ?>
				<div class="yui3-g" style="height:28px;overflow:auto;">
<a href='./faculty_chapters.php?action=addNew&selectedCourse=<?=$_GET['selectedCourse']?>' style="text-decoration:none;">
											<div class="grid-button-edit yellow-button" style="text-align: center;">
												Add Chapter
											</div>
										</a>
				</div>
																					 <?php } ?>
			</div>	
			<div  style="width:2%;float:left;"> &nbsp; </div>
			<div class="box-shadow"  style="width:75%;float:left;">
				<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
					Manage Chapters
				</div>
				<div class="yui3-g" style="height:450px;overflow:auto;">
					<table width="100%">
						<tr>
							<td colspan="2" style="padding-left:20px">
								<div id="brief" style="height:150px">
									<div style="width:40%;float:left; border-right:solid 1px #ddd; height:60px;padding:20px;">
										Click on the Chapter list on the left to load details for the chapter here.
									</div>
									<div style="width:45%;float:left;text-align:center;padding:20px;">
										Create a new Chapter
										<a href='./faculty_chapters.php?action=addNew&selectedCourse=<?=$_GET['selectedCourse']?>' style="text-decoration:none;">
											<div class="grid-button-edit yellow-button" style="text-align: center;width:80px;">
												Create
											</div>
										</a>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td width="40%" style="padding-left: 5%; padding-right:5% "><div id="subtopic"></div></td>
							<td width="40%" style="padding-left: 5%; padding-right:5% "><div id="assignment"></div></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
	
	if($_GET['action']=="edit")
	{
		if($_GET['option']=="update")
		{
			$istopic="0";
			$ptopicid="0";
			if($_POST['issubtopic']=="1")
			{			/**
				if($_POST['stopicid']!="")
				{
					$istopic="1";
					$ptopicid=$_POST['stopicid'];
				}**/
 if($_POST['']!="select_combosubtopics")
				{
					$istopic="1";
					$ptopicid=$_POST['select_combosubtopics'];
				}

			}
			/*if(function_exists('date_default_timezone_set')) 
				{date_default_timezone_set('Asia/Calcutta');}
			$date = date('d-m-Y H:i:s', time());*/
			
			$cleanData = normalize_str($_POST['topiccontent']);
			$headingData = addEscapeSeq($_POST['heading']);
			$topicBriefData = addEscapeSeq($_POST['topicbrief']);
			mysql_query("update topics set Heading='".$headingData."',IsSubTopic=".$istopic.",TopicID='".$ptopicid."',ContentData='".$cleanData."',ActivateFrom='".$_POST['activateFrom']."',TopicBrief='".$topicBriefData."' where ID=".$_GET['topicid']);
			
			?>
			<script>
				window.location='./faculty_chapters.php?selectedCourse=<?=$_GET['selectedCourse']?>&action=addNew&msg=Chapter Successfully Updated.';	
			</script>
			<?php
		}
		?>
		<script src="js/CKEditor/ckeditor.js"></script>
		<script>
			function checkbox()
			{
				if($('#issubtopic').is(':checked'))
				{
					loadData();
				}
				else
				{
				  $('#combosubtopics').html("<div id='combosubtopics'></div>");
				  //$('#gridcontainer').html("<table id='list2'></table><div id='pager2'></div>");
				}
			}
			function loadData()
			{
				if(!$('#issubtopic').is(':checked'))
				{
					return;
				}


				$.ajax({
				  url: 'faculty_handler.php?action=getSubTopics_facutly&courseid=' + $('#course').val(),
					  success: function(data) {
					  $('#combosubtopics').html(data);
					  $('#select_combosubtopics').val($('#stopicid').val());
					  //alert($('#stopicid').val());
					  //$('#stopicid').val($('#select_combosubtopics').val());
					}
				  });


				/*$('#gridcontainer').html("<table id='list2'></table><div id='pager2'></div>");
				jQuery("#list2").jqGrid({
					url:'faculty_handler.php?action=get_topics_all&courseid='+$('#course').val(),
					datatype: "json",
					colNames:['ID','Heading'],
					colModel:[
						{name:'id',index:'id', width:55},
						{name:'Heading',index:'Name', width:90}
					],
					rowNum:10,
					rowList:[10,20,30],
					pager: '#pager2',
					sortname: 'id',
					loadonce : false,
					viewrecords: true,
					sortorder: "desc",
					multiselect: false,
					altRows: true,	
					width: 400,
					height: 50,
					rownumbers: true,
					rownumWidth: 40,
					caption:"List of Topics",
					onSelectRow: function (id) 
					{
						$('#stopicid').val(id);
					}
				});
				jQuery("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false});			*/
			}

			function verifica_edit(){
			  if($('#topicbrief').val()=="" || $('#heading').val()=="" || $('#activateFrom').val()==""){
				alert("Please fill in the details");
							   return false;
			  }
			}

		</script>
		<form action="./faculty_chapters.php?selectedCourse=<?=$_GET['selectedCourse']?>&action=<?=$_GET['action']?>&option=update&topicid=<?=$_GET['topicid']?>" method="post" name="formupdate" onsubmit="return verifica_edit()">
		<input type="hidden" id="course" value="<?=$_GET['selectedCourse']?>"  />
		
		<div class="yui3-g" style="margin-top:25px;">
			<div class="yui3-u-1-5 box-shadow"  style="float:left;">
				<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
					Chapter List
				</div>
				<div id="topicTree" class="yui3-g demo" style="height:450px;overflow:auto;">
						
					<script type="text/javascript" class="source below">
					var treeloaded=0;
					$(function () {
						$("#topicTree")
							.jstree({
								"json_data" : {
									"ajax" : { "url" : "./faculty_handler.php?action=get_tree_topics&selectedCourse=<?=$_GET['selectedCourse']?>" }
								},
								"themes" : {
												"theme" : "apple",
												"icons" : false
											},
								"plugins" : ["themes","json_data","ui"]
							})
							.bind("select_node.jstree", function (e, data) {
								if(treeloaded)
								{	
									window.location='./faculty_chapters.php?action=edit&selectedCourse=<?=$_GET['selectedCourse']?>&topicid='+data.rslt.obj.attr("id");
								}
							});
							
							$('#6').addClass('jstree-open');
					});
					
					setTimeout(function () { $("#topicTree").jstree("set_focus"); }, 500);
					setTimeout(function () { $.jstree._focused().select_node("#<?=$_GET['topicid']?>"); treeloaded=1; }, 1000);
					
					
				
					</script>
				
				</div>
			</div>
			<div  style="width:2%;float:left;"> &nbsp; </div>
			<div class="box-shadow"  style="width:75%;float:left;">
				<?php
					$topic=mysql_query("select * from topics where ID=".$_GET['topicid']);
					$topic=mysql_fetch_array($topic);
				?>
				<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
					Edit Chapter / SubChapter
				</div>
				<div style="margin:10px 0px; min-height:430px;">

					<table width="100%" style="line-height:1.5em;">
					
						<tr class="form-row"><td>Chapter Name</td><td>
							<input type="text" name="heading" id="heading" title="Enter Name For The Chapter" style="width:400px" value="<?=htmlspecialchars($topic['Heading'])?>" />
							<br/><div class="form-error" id='formupdate_heading_errorloc' ></div>
						</td></tr>
						<!--<tr class="form-row"><td>Is-SubTopic</td><td>
							<input type="checkbox" name="issubtopic" id="issubtopic" value="1" title="Check if this is a subtopic" onchange="checkbox();" <?php if($topic['IsSubTopic'] == "1") print(" checked='checked' "); ?>  />
							<input type="hidden" name="stopicid" id="stopicid" value="<?=$topic['TopicID']?>" />

							<div id="combosubtopics"></div>
						<script>
	                         loadData();
			                 checkbox();
							</script>

							<!--<div id="gridcontainer">
							<table id="list2"></table>
							<div id="pager2"></div>
							<script>
							   //loadData();
							   //checkbox();
							</script>
							</div>-->
						<!--</td></tr>-->
						<tr class="form-row"><td>Chapter Brief</td><td>
							<textarea name="topicbrief" id="topicbrief" title="Brief description regarding topic" cols="47" rows="2"><?=htmlspecialchars($topic['TopicBrief'])?></textarea>
							<br/><div class="form-error" id='formupdate_topicbrief_errorloc' ></div>
						</td></tr>
						<tr class="form-row"><td>Content Data</td><td>
							<textarea class="ckeditor" name="topiccontent" title="Actual Topic Content" cols="50" rows="10"><?=$topic['ContentData']?></textarea>
						</td></tr>
						<tr class="form-row"><td>Activate From</td><td><input type="text" name="activateFrom" id="activateFrom" class="activateFrom" style="width:400px" value="<?=$topic['ActivateFrom']?>" />
						<br/><div class="form-error" id='formupdate_activateFrom_errorloc' ></div>
						</td></tr>
						<tr class="form-row"><td colspan="2">
						
						<!--<input class="grid-button-edit yellow-button" style="margin:15px;" id="checkPreview" type="button" value="Preview" />-->
						<input class="grid-button-edit yellow-button" type="submit" value="Update" /></td></tr>
					</table>
				</div>


<div id="preview" style="width:535px; height:420px; display: none;">

	</div>

			</div>
			</div>
		</form>
		<script language="JavaScript" type="text/javascript">
			//You should create the validator only after the definition of the HTML form
			  var frmvalidator  = new Validator("formupdate");
			 frmvalidator.EnableOnPageErrorDisplay();
			frmvalidator.EnableMsgsTogether();
			 
			 frmvalidator.addValidation("activateFrom","req", "Please enter date to Activate the Chapter.");
				
			  frmvalidator.addValidation("heading","req","Please enter Chapter Name");
			  frmvalidator.addValidation("heading","maxlen=50",	"Max length for Chapter Name is 50 characters");
			  							  
			  frmvalidator.addValidation("topicbrief","req","Please enter summary of the Chapter");
			  frmvalidator.addValidation("topicbrief","maxlen=250",	"Max length for chapter brief is 50 characters");
			  							  
			  
			  
			  
		</script>
		<?php
	}
	else if($_GET['action']=="addNew")
	{

	  $flag_evalua = ($_POST['heading'] == "" || $_POST['topiccontent'] == "" || $_POST['activateFrom'] == "" || $_POST['course'] == "" || $_POST['topicbrief'] == "" );

		if($_GET['option']=="add")
		  {

			$istopic="0";
			$ptopicid="0";
			if($_POST['issubtopic']=="1")
			{
			  /**
			  if($_POST['stopicid']!="")
				{
					$istopic="1";
					$ptopicid=$_POST['stopicid'];
				}
			  **/
			  if($_POST['']!="select_combosubtopics")
				{
					$istopic="1";
					$ptopicid=$_POST['select_combosubtopics'];
				}
			}

			if(!$flag_evalua){
			
			if(function_exists('date_default_timezone_set')) 
				{date_default_timezone_set($application_time_zone_text);}
			$date = date('d-m-Y H:i:s', time());
			$cleanData = normalize_str($_POST['topiccontent']);
			$headingData = addEscapeSeq($_POST['heading']);
			$topicBriefData = addEscapeSeq($_POST['topicbrief']);
			
			//mysql_query("insert into topics (Heading,IsSubTopic,TopicID,ContentData,ActivateFrom,CourseID,TopicBrief) values('".$headingData."',".$istopic.",'".$ptopicid."','".$cleanData."','".$_POST['activateFrom']."','".$_POST['course']."','".$topicBriefData."')") or die(mysql_error());
			
			mysql_query("insert into topics (Heading,IsSubTopic,TopicID,ContentData,ActivateFrom,CourseID,TopicBrief) values('a',0,0,'azx',2014-10-16 00:00:00,3,'vbvb')") or die(mysql_error());
			?>
			<script>
			   window.location='./faculty_chapters.php?selectedCourse=<?=$_POST['course']?>&action=addNew&msg=Chapter Successfully added.';	
			</script>
			<?php
				}else{
?>
			<script>
				window.location='./faculty_chapters.php?selectedCourse=<?=$_POST['course']?>&action=addNew&msg=must complete all fields.';	
			</script>
			<?php
			}
		  }
		?>
		<script src="js/CKEditor/ckeditor.js"></script>
		<script>
			function checkbox()
			{
				if($('#issubtopic').is(':checked'))
				{
					loadData();
				}
				else
				{
				  $('#combosubtopics').html("<div id='combosubtopics'></div>")
				  //$('#gridcontainer').html("<table id='list2'></table><div id='pager2'></div>");
				}
			}

		/** CESAR JUAREZ - OPEN COMET
		 * Se creara una funcion con la cual el usuario ya no verá la grilla
		 * simplemente se observará un combobox
		 **/

			function loadData()
			{
				if(!$('#issubtopic').is(':checked'))
				{
					return;
				}
				$.ajax({
				  url: 'faculty_handler.php?action=getSubTopics_facutly&courseid=' + $('#course').val(),
					  success: function(data) {
					  $('#combosubtopics').html(data);

					  //alert($('#select_combosubtopics').val());
					  //$('#stopicid').val($('#select_combosubtopics').val());
					}
				  });

				/*
				$('#gridcontainer').html("<table id='list2'></table><div id='pager2'></div>");
				jQuery("#list2").jqGrid({
					url:'faculty_handler.php?action=get_topics_all&courseid='+$('#course').val(),
					datatype: "json",
					colNames:['ID','Heading'],
					colModel:[
						{name:'id',index:'id', width:55},
						{name:'Heading',index:'Name', width:90}
					],
					rowNum:10,
					rowList:[10,20,30],
					pager: '#pager2',
					sortname: 'id',
					loadonce : false,
					viewrecords: true,
					sortorder: "desc",
					multiselect: false,
					altRows: true,	
					width: 400,
					height: 50,
					rownumbers: true,
					rownumWidth: 40,
					caption:"List of Topics",
					onSelectRow: function (id) 
					{
						$('#stopicid').val(id);
					}
				
				});
				jQuery("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false});**/
			}

			function verifica(){
			  if($('#topicbrief').val()=="" || $('#heading').val()=="" || $('#activateFrom').val()==""){
				alert("no puedes guardar");
							   return false;
			  }
			}

		</script>
		<form action="./faculty_chapters.php?selectedCourse=<?=$_GET['selectedCourse']?>&action=<?=$_GET['action']?>&option=add&flag=1" method="post" name="formtopic" onsubmit="return verifica()">
		<div class="yui3-g" style="margin-top:25px;">
			<div class="yui3-u-1-5 box-shadow"  style="float:left;">
				<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
					Chapter List
				</div>
				<div id="topicTree2" class="yui3-g demo" style="height:450px;overflow:auto;">
					<script type="text/javascript" class="source below">
						$(function () {
							$("#topicTree2")
								.jstree({
									"json_data" : {
										"ajax" : { "url" : "./faculty_handler.php?action=get_tree_topics&selectedCourse=<?=$_GET['selectedCourse']?>" }
									},
								"themes" : {
												"theme" : "apple",
												"icons" : false
											},
								"plugins" : ["themes","json_data","ui"]
								})
								.bind("select_node.jstree", function (e, data) {	
									window.location='./faculty_chapters.php?action=edit&selectedCourse=<?=$_GET['selectedCourse']?>&topicid='+data.rslt.obj.attr("id")
								});
						});
						
					
						</script>
				</div>
			</div>
			<div style="width:2%;float:left;"> &nbsp; </div>
			<div class="box-shadow"  style="width:75%;float:left;">
				<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
					Add Chapter / SubChapter
				</div>
				<div style="margin:10px 0px; min-height:430px;">

					<table width="100%" style="line-height:1.5em;">

						<tr class="form-row"><td width="20%">Course</td><td>
							<select name="course" id="course" title="Choose Course" style="width:400px" onchange="loadData();" >
								<option value="-1">Choose Course</option>
								<?php get_Courses_Select(); ?>
							</select>
							<br/><div class="form-error" id='formtopic_course_errorloc' ></div>
<script>
var seleccion = "<?php echo $_SESSION['courseID']?>"
if(seleccion!=""){
  document.forms['formtopic']['course'].value = seleccion;
}
</script>
						</td></tr>
						<tr class="form-row"><td>Chapter Name</td><td>
							<input type="text" maxlength="50" name="heading" id="heading" title="Enter Chapter Name For The Chapter" style="width: 450px;"/>
							<br/><div class="form-error" id='formtopic_heading_errorloc' ></div>
						</td></tr>
						<!--<tr class="form-row"><td>Is-SubTopic</td><td>
							<input type="checkbox" name="issubtopic" id="issubtopic" value="1" title="Check if this is a subtopic" onchange="checkbox();" />
							<input type="hidden" name="stopicid" id="stopicid" value="" />

<div id="combosubtopics"></div>
						<script>
	                         loadData();
			                 checkbox();
							</script>
							<!-- <div id="gridcontainer"> CESAR JUAREZ - OPEN COMET
							<table id="list2"></table>
							<div id="pager2"></div>
							<script>
	                          //loadData();
	                         //checkbox();
							</script>
							</div> -->

						<!--</td></tr>-->
						<tr class="form-row"><td>Chapter Brief</td><td>
							<textarea id="topicbrief" name="topicbrief" title="Brief description regarding topic" cols="47" rows="2" maxlength="200"></textarea>
							<br/><div class="form-error" id='formtopic_topicbrief_errorloc' ></div>
						</td></tr>
						<!--<tr class="form-row"><td>Content Data</td><td>
							<textarea class="ckeditor" name="topiccontent" title="Actual Topic Content" cols="50" rows="10"></textarea>
						</td></tr>-->
						
						<tr class="form-row"><td>Content Data</td><td>
							<div id="me"><a style="color:#aaa; border-radius: 5px; padding:4px;" class="btn btn-success btn-xs add-txt yellow-button">Browse....</a></div></td>
						</tr>
						<tr class="form-row"><td>&nbsp;</td><td>
							<span id="mestatuserror" class="form-error"></span>
							<span id="mestatus" class="form-error" style="color:#008000;">
							</span><br><span id="files" name="topiccontent"></span></td>
						</tr>
						
						<tr class="form-row"><td>Activate From</td><td><input type="text" name="activateFrom" id="activateFrom" class="activateFrom" style="width:400px" />
						<br/><div class="form-error" id='formtopic_activateFrom_errorloc' ></div>
						</td></tr>
						<tr class="form-row"><td colspan="2">
						<!--<input class="grid-button-edit yellow-button" style="margin:15px;" id="checkPreview" type="button" value="Preview" />-->

<input class="grid-button-edit yellow-button" type="submit" value="Save" /></td></tr>
					</table>
				</div>


<div id="preview" style="width:535px; height:420px; display: none;">

	</div>
			</div>
		</div> 
		</form>
		<script language="JavaScript" type="text/javascript">
			//You should create the validator only after the definition of the HTML form
			  var frmvalidator  = new Validator("formtopic");
			  frmvalidator.EnableOnPageErrorDisplay();
			  frmvalidator.EnableMsgsTogether();
			  frmvalidator.addValidation("activateFrom","req", "Please enter date to Activate the Chapter.");
			  
			  frmvalidator.addValidation("heading","req","Please enter Chapter Name");
			  frmvalidator.addValidation("heading","maxlen=50",	"Max length for Chapter Name is 50 characters");
			  							  
			  frmvalidator.addValidation("topicbrief","req","Please enter summary of the Chapter");
			  frmvalidator.addValidation("topicbrief","maxlen=250",	"Max length for chapter brief is 50 characters");
			  							  
			  
			  frmvalidator.addValidation("course","dontselect=-1", "Please select a Course");
		</script>

		<?php
			
			}
			?>
			<script>
			$(document).ready(function(){
				$("li#menu-topic a").addClass("active");
				
				
				$("#checkPreview").click(function() {
					var oEditor = CKEDITOR.instances.topiccontent.getData();
					//alert(oEditor);
					$.fancybox.open(oEditor,{autoSize:false,height:420,width:535});
				});
			});			
		</script>
<?php
	require_once("footer.php");
?>