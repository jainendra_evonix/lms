<?php
	require_once("headermarks.php");
	require_once("mainFunctions.php");

if(!$_SESSION['login']){?>
		<script>
			window.location='./index.php';
		</script>
<?php
			}

	?>

<?php

	 /** CESAR JUAREZ - OPEN COMET **/

	if ($_GET['selectedCourse']==""){
	  if($_SESSION['courseID'] != "") $_GET['selectedCourse'] = $_SESSION['courseID'];
	} elseif ($_GET['selectedCourse']!=""){
	  $_SESSION['courseID'] = $_GET['selectedCourse'];
	}	
?>
	<script>
		function loadAllEndedAssignments()
		{
			$.ajax({
				url: 'student_handler.php?action=getEndedAssignmentForMenu&selectedCourse=<?=$_GET['selectedCourse']?>',
				success: function(data) {
					$('#leftmenu').html(data);
				}
			});
		}
		function getAssignmentDetails(batchid,assid)
		{
			$.ajax({
			  url: 'student_handler.php?action=getAssignmentDetails&assid='+assid+'&batchid='+batchid,
				success: function(data) {
					$('#mainform').html(data);
				}
			});
		}
	</script>
		<!-- for collapsible pane on the left of the screen -->
	<!--<nav>
		<ul>
			<?php //getAllCoursesList("student_marks.php"); ?>
		</ul>
	</nav>-->
	<?php
		if($_GET['selectedCourse']=="")
	{
		?>
		
		<div class="yui3-g" style="margin-top:25px;">
		<div class="box-shadow"  style="width:75%;margin:0px auto;">
			<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
				<!--Please select a course from the collapsible pane on the left of the screen.-->				Program List
			</div>						<div class="yui3-g">										<table width="100%">						<?php getAllCoursesListInDiv("student_marks.php"); ?>					</table>									</div>
		</div>
		</div>
		<?php
	}
	else
	{
	?>
	<div class="yui3-g" style="margin-top:25px;">
		<div id="leftmenu" class="yui3-u-1-5 box-shadow"  style="float:left;">
		</div>
		
		<div  style="width:2%;float:left;"> &nbsp; </div>
		
		<div id="mainform" class="box-shadow"  style="width:75%;float:left;">
			<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
				Marks								<a href="unsetmarksdata.php" class="grid-button-edit yellow-button" style="float: right; font-size: 15px; cursor: pointer; text-decoration:none;"><span style="padding:10px;">Click Here To Change Program</span></a>
			</div>
			<div class="yui3-g" class="box-header" style="height:450px;overflow:auto;">
				<div class="box-header" style="height:150px">
					Click on the Assignment list on the left to check your performance in it.
				</div>
			</div>
		</div>	
	</div>	
	<script>
		loadAllEndedAssignments();
	</script>
	<?php
	}?>	<script>			$(document).ready(function(){				$("li#menu-marks a").addClass("active");			});					</script><?php
	require_once("footer.php");
?>