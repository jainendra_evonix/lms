<?php
	require_once("header.php");
	require_once("mainFunctions.php");
	if($_GET['action']=="createnotice")
	{
		$rs=mysql_query("select * from notice where NoticeName='".$_POST['noticeName']."'");
		if(mysql_num_rows($rs)>0)
		{
			?>
			<script>
				window.location='./admin_notices.php?msg=Notice Name previously exists, insertion aborted';
			</script>
			<?php
		}
		else
		{
			$enabled=$_POST['enabled'];
			if($enabled=="")
			{
				$enabled="0";
			}
			mysql_query("insert into notice(NoticeName, Enabled) values('".$_POST['noticeName']."','".$enabled."')") or die(mysql_error());
			$_SESSION['noticeid']=mysql_insert_id();
			?>
			<script>
				window.location='./admin_notices.php?response=noticecreated';
			</script>
			<?php
		}
	}
	else if($_GET['action']=="setnotice")
	{
		$_SESSION['noticeid']=$_GET['id'];
		?>
		<script>
			window.location='./admin_notices.php?response=noticecreated';
		</script>
		<?php
	}
	?>
	<div class="yui3-g" style="margin-top:25px;">
		<div class="yui3-u-1-5 box-shadow"  style="float:left;">
			<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
				Notice List
			</div>
			<div class="yui3-g" style="height:420px;overflow:auto;">
				<table width="100%">
					<?=get_All_Notices_Admin()?>
				</table>
			</div>
			<div class="yui3-g" style="height:28px;overflow:auto;">
				<a href="admin_notices.php" style="text-decoration:none;">
					<div class="grid-button-edit yellow-button" style="text-align: center;">
						Add Notice
					</div>
				</a>
			</div>
		</div>
		<div  style="width:2%;float:left;"> &nbsp; </div>

		<div class="box-shadow"  style="width:75%;float:left;">
			<?php
			if($_GET['response']=="")
			{
				?>		
				<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
					Create New Notice
				</div>
				<div id="newForm" style="margin:10px 0px; height:430px;">
					<form id="form" name="form" action="./admin_notices.php?action=createnotice" method="post">
					
					<table width="100%" style="line-height:2em;">
						<tr style="border-bottom:solid 1px #ddd">
							<td style="width:150px;text-align:right; vertical-align:middle; padding:5px;">Notice Name:</td>
							<td style="vertical-align:middle; padding:5px;"><input type="text" style="width:300px;height:30px" name="noticeName" id="noticeName" title="Enter Notice Name"></td>
						</tr>
						<tr>
							<td style="width:150px;text-align:right; vertical-align:middle; padding:5px;">Enabled:</td>
							<td style="vertical-align:middle; padding:5px;"><input type="checkbox"  style="width:20px;height:20px" name="enabled" id="enabled" title="Check if you want this Notice to be enabled" value="1"></td>
						</tr>
						<tr>
							<td></td>
							<td>
								<input type="submit" value="Create Notice" class="grid-button-edit green-button" style="height:30px; width:100px !important;cursor:pointer;">
							</td>
						</tr>
					</table>
					</form>
				</div>
				<script>
					$("#form").validate({
					rules: {
						noticeName: "required"
					},
					messages: {
						noticeName: "Please enter your noticename",
					}
				});
				</script>
				<?php
			}
			else if($_GET['response']=="noticecreated")
			{
				$r=mysql_query("select * from notice where ID=".$_SESSION['noticeid']) or die(mysql_error());
				$r=mysql_fetch_array($r);
				?>	
				<script>
					var loaded=0;
					function loadData()
					{
						jQuery("#list2").jqGrid({
							url:'admin_handler.php?action=get_batch_notice',
							datatype: "json",
							colNames:['Batch ID','Name'],
							colModel:[
										{name:'id',index:'id', width:30},
										{name:'Name',index:'Name', width:100},	
									],
							rowNum:10,
							rowList:[10,20,30],
							pager: '#pager2',
							sortname: 'id',
							viewrecords: true,
							sortorder: "desc",
							multiselect: true,
							altRows: true,	
							width: 700,
							rownumbers: false,
							rownumWidth: 40,
							//caption:"List of Faculties",
							loadComplete: function(data) {
								var userdata = $("#list2").getGridParam('userData');
								if(userdata.selID!=null)
								{
									var spl=userdata.selID.split("|");
									
									for(i=0;i<spl.length;i++)
									{
										if(spl[i]!="")
										{
											jQuery("#list2").setSelection (spl[i], true);
										}
									}
								}
								loaded=1;
							},
							onSelectRow: function (id) 
							{
								if(loaded!=0)
								{
									$.ajax({
										url: 'admin_handler.php?action=updateNoticeBatch&value='+id,
									});
								}
							},
						});
						jQuery("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false});			
					}
					function updateNoticeName()
					{
						if($('#noticeName').val()=="")
						{
							return;
						}
						$.ajax({
						  url: 'admin_handler.php?action=updateNoticeName&value='+$('#noticeName').val(),
						});
					}
					function updateNoticeEnabled()
					{
						$.ajax({
						  url: 'admin_handler.php?action=updateNoticeEnabled&value='+$('#enabled').is(':checked'),
						});
					}
					
				</script>
				<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
								Manage Notice
							</div>
							<div id="editForm" style="margin:10px 0px; height:430px;">
								
								<table width="100%" style="line-height:2em;">
									<tr style="border-bottom:solid 1px #ddd">
										<td style="width:150px;text-align:right; vertical-align:middle; padding:5px;">Notice Name:</td>
										<td style="vertical-align:middle; padding:5px;"><input type="text" style="width:300px;height:30px;font-size:14px;" name="noticeName" id="noticeName" title="Enter Notice Name" value="<?=$r['NoticeName']?>" onBlur="updateNoticeName();"></td>
									</tr>


									<tr style="border-bottom:solid 1px #ddd">
										<td style="width:150px;text-align:right; vertical-align:middle; padding:5px;">Enabled:</td>
										<td style="vertical-align:middle; padding:5px;"><input type="checkbox" name="enabled" id="enabled" <?php if ($r['Enabled']=="1") print("checked='checked'"); ?> value="1"  title="Check if you want this Notice to be enabled" onChange="updateNoticeEnabled();">
										</td>
									</tr>
									<tr style="border-bottom:solid 1px #ddd">
										<td style="width:150px;text-align:right; vertical-align:top; padding:5px;">
											Batches Handling the notice:
										</td>
										<td style="vertical-align:middle; padding:5px;font-size:11pt;line-height:1.2em !important;">Please select batches to be added for the notice from the list below. A notice can be handled by more than one batch
										</td>
									</tr>
									<tr>
										<td colspan="2" style="padding:20px;">
											<table id="list2"></table>
											<div id="pager2"></div>
										</td>
									</tr>
									<!--<tr>
										<td></td>
										<td>
											<input type="submit" value="Create Batch" class="grid-button-edit green-button" style="height:30px; width:100px !important;cursor:pointer;">
										</td>
									</tr>-->
									<tr>
										<td colspan="2" style="padding-top:5px;">
											<a href="./admin_notices.php?action=setnotice&id=<?=$r['ID']?>" style="text-decoration:none;"><div class="grid-button-edit yellow-button" style="width:70px;text-align: center;">Save</div></a>
										</td><!-- CESAR JUAREZ : OPEN COMET -->
										
									</tr>
								</table>
							</div>
				<script>
					loadData();
				</script>
				<?php
			}
			?>
		</div>
	</div>		
				
				
				
	<script>
		$(document).ready(function(){
			$("li#menu-notice a").addClass("active");
		});			
	</script>
	<?php
	require_once("footer.php");
?>