 <?php include "eportfoli_header.php"; ?>
<link rel="stylesheet" href="cssep/colorbox.css" />
<!-- start fancybox video gallery -->
	 <link rel="stylesheet" href="css/main.css">
    <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
 <!-- end fancybox video gallery -->
 
  <style type="text/css">
  .clr{  border: 1px solid #ccc;
  padding: 4px;
  vertical-align: top;
  font-size: 13px;
  font-weight: bold;}
  
  .lk{font-family: Cambria, serif;}
  
  .borde{border: 1px solid #ccc;  vertical-align: top;}
  td{padding: 5px;}
  </style>

 
 
          <div id="content" class="content">
            <!-- begin breadcrumb -->
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Videos</h1>
            <!-- end page-header -->
            <!-- begin row -->
            <div class="row">
               <!-- begin col-6 -->
               <div class="col-md-12 " style="min-height:500px;background: #fff;  border-radius: 3px;">
               
               <br><br><br>
               
              
<section class="second clearfix">

      <article class="video">
        <figure>
        <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/QOP3b-1wFck"><img class="videoThumb" src="http://img.youtube.com/vi/QOP3b-1wFck/1.jpg"></a>
        </figure>
        <h2 class="videoTitle">Key Note Address:<br> Pre Conference Symposium</h2>
      </article>
      
      <article class="video">
        <figure>
        <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/sHcT2tx8iPs"><img class="videoThumb" src="http://img.youtube.com/vi/sHcT2tx8iPs/1.jpg"></a>
        </figure>
        <h2 class="videoTitle">Successful Healthcare<br> Models: Hospital- Part 1</h2>
      </article>
      
      <article class="video">
        <figure>
        <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/Kfm2_AXDJJI"><img class="videoThumb" src="http://img.youtube.com/vi/Kfm2_AXDJJI/1.jpg"></a>
        </figure>
        <h2 class="videoTitle">Successful Healthcare<br> Models: Hospital- Part 2</h2>
      </article>
      
      <article class="video">
        <figure>
        <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/k5jyqsM7_I4"><img class="videoThumb" src="http://img.youtube.com/vi/k5jyqsM7_I4/1.jpg"></a>
        </figure>
        <h2 class="videoTitle">Successful Healthcare<br> Models: Pharma</h2>
      </article>

    </section>
                
                  </div>
               </div>
               <!-- end col-6 -->
               <!-- begin col-6 -->
               <!-- end col-6 -->
            </div>
            <!-- end row -->
         </div>
         <!-- end #content -->
         <!-- begin theme-panel -->
         <!-- end theme-panel -->
         <!-- begin scroll to top btn -->
         <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
         <!-- end scroll to top btn -->
      </div>
      <!-- end page container -->
      <!-- ================== BEGIN BASE JS ================== -->
      <script src="jsep/jquery-1.9.1.min.js"></script>
      
      
      
      <script src="jsep/jquery-migrate-1.1.0.min.js"></script>
      <script src="jsep/jquery-ui.min.js"></script>
      <script src="jsep/bootstrap.min.js"></script>
      <!--[if lt IE 9]>
      <script src="assets/crossbrowserjs/html5shiv.js"></script>
      <script src="assets/crossbrowserjs/respond.min.js"></script>
      <script src="assets/crossbrowserjs/excanvas.min.js"></script>
      <![endif]-->
      <script src="jsep/jquery.slimscroll.min.js"></script>
      <script src="jsep/jquery.cookie.js"></script>
      <!-- ================== END BASE JS ================== -->
      <!-- ================== BEGIN PAGE LEVEL JS ================== -->
      <script src="jsep/apps.min.js"></script>
      
          <script src="jsep/jquery.colorbox.js"></script>
      
      <!-- ================== END PAGE LEVEL JS ================== -->
      <script>
         $(document).ready(function() {
         	App.init();
         });
      </script>
      
      <script>
        $(document).ready(function(){
            //Examples of how to assign the Colorbox event to elements
            $(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
            
        });
    </script>
      
      <!-- start fancybox video gallery -->
      <script src="fancybox/jquery.fancybox.js"></script>
      <script src="js/global.min.js"></script>
      <!-- end fancybox video gallery -->
      
      
   </body>
</html>