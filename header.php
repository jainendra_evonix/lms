<?php @session_start();
error_reporting(E_ALL & ~E_NOTICE);
require_once("mainFunctions.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="css/cupertino/jquery-ui.css">
	<!--<link rel="stylesheet" href="css/cupertino/jquery.ui.theme.css">-->
	<link rel="stylesheet" href="css/customStyler.css">
	<link rel="stylesheet" href="css/ui.jqgrid.css">
	<link rel="stylesheet" href="css/ui.multiselect.css">
	<link rel="stylesheet" href="fancybox/jquery.fancybox.css">
	<link rel="stylesheet" href="css/yui.gridlayout.min.css"/>
	
	<!-- for flipbook -->
	<link rel="stylesheet" type="text/css" href="flipbook/css/flipbook.style.css">
	<link rel="stylesheet" type="text/css" href="flipbook/css/font-awesome.css">
	
	<script src="js/jquery-1.8.3.js"></script>
	<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="js/grid.locale-en.js"></script>
	<script src="js/jquery.jqGrid.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/jquery-ui-timepicker-addon.js"></script>
	<script src="js/jquery-ui-sliderAccess.js"></script>
	<script src="js/jsTree/_lib/jquery.cookie.js"></script>
	<script src="js/jsTree/_lib/jquery.hotkeys.js"></script>
	<script src="js/jsTree/jquery.jstree.js"></script>
	<script src="fancybox/jquery.fancybox.pack.js"></script>
	<script src="fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
	<script src="js/gen_validatorv4.js" type="text/javascript"></script>

	<!-- for flipbook -->
	<script src="flipbook/js/flipbook.min.js"></script>

	<script>
	  /*** CESAR JUAREZ - OPEN COMET ***/
	  function start_test(url){
		$("#btnStartTest").remove();
	  window.open(url,'Exam','width='+screen.width+', height='+screen.height);
	}

function justNumbers(evt){

var keyPressed = (evt.which) ? evt.which : event.keyCode
  return !(keyPressed > 31 && (keyPressed < 48 || keyPressed > 57));
}


var qid=0;
function save_Answer(){
$.post( "./faculty_handler.php?action=saveAns", {questionID:qid, ans: $('#ans').val()} , 
				 function (data) {
					$.fancybox.close();
				 });
}


		$(document).ready(
		  function() { 
		  	
			$('#activateFrom').datetimepicker({ dateFormat: 'yy-mm-dd' });
			$("#inline").fancybox({
				'transitionIn'	:	'elastic',
				'transitionOut'	:	'elastic',
				'speedIn'		:	600, 
				'speedOut'		:	200, 
				'overlayShow'	:	false,
				 ajax : {
						url: 'student_handler.php?action=getQestion'
					}
			});
			
		  }
		);
		$(function() {
			$( "#radio" ).buttonset();
		});
		/*$(function() {
			$( "input[type=submit], #aa, button" )
				.button()
		});
		$(function() {
			$( document ).tooltip({
				track: true
			});
			
		});*/
		$(function() {
			$( "#dialog-message" ).dialog({
				modal: true,
				buttons: {
					Ok: function() {
						$( this ).dialog( "close" );
					}
				}
			});
		});
		
	</script>
	<style>
		body { margin:0px; font-family: calibri,sans-serif; font-size: 12pt; background-color: #FFF5E8; line-height: 1.5em; }
		.page-wrap {
			position: relative; 
			width: 1000px;
			margin: 0px auto; 
			padding: 0px; 
		}
		.menu-holder {
			height: 40px;
			position: relative; 
			margin: 0px auto;
			padding:0px 0px 2px 0px;
			width: 986px;
		}
		.topbackground{
			background:url(images/topbar-bg.jpg) repeat-x;
			height: 92px;
			position: absolute;
			top: 0px;
			width:100%;
		}
		.ui-tooltip
		{
			font-size:10pt;
		}
		#menu {padding:0; margin:0; list-style:none;}
		#menu li {float:left; margin-left:1px;}
		#menu li a {display:block; height:40px; line-height:40px; padding:0 15px; float:left; background:#EDDCC8; color:#000; text-decoration:none;font-family: arial,sans-serif; font-size:10pt;}
		#menu li a b {text-transform:uppercase;}
		#menu li a:hover, #menu li a.active
		{background: #F6A139 url(images/arrow.gif) no-repeat center bottom; color:#fff; font-weight:bold;}
		.box-shadow {
			background: #fff;
			border:solid 1px #dddddd;
			-moz-border-radius: 5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			-moz-box-shadow: 0px 0px 5px #000000;
			-webkit-box-shadow: 0px 0px 5px #000000;
			box-shadow: 0px 0px 5px #000000;
		}
		.box-header{
			padding: 5px 3px;
			font-size: 16pt;
			color: #D04646;
			text-align:center;
		}
		.big-number-text{
			line-height:1.5em;
			font-size: 30pt;
			color: #45494A;
		}
		.grid-small-text{
			padding:5px;
			font-size:10pt;
			text-align:center;
			vertical-align:middle;
		}
		.green-button{
			background-color: #D04646;
			border: solid 1px #D04646;
		}
		.yellow-button{
			min-width:80px !important;
			height:25px !important;
			margin:0px auto;
			background-color: #31A3DD;
			border: solid 1px #31A3DD;
			color:white !important;
			font-weight:bold;
		}
		.grid-button-edit{
			min-width: 50px; 
			height: 20px;
			color:white;
			-moz-border-radius: 5px;
			-webkit-border-radius:5px;
			border-radius:5px;
		}
		.grid-button-edit:hover{
			background-color: #F6A139;
			color:#fff !important;
			border-color:#F6A139;
		}
		.row-items{
			border-bottom: solid 1px #ddd;
		}
		.ui-jqgrid tr.jqgrow td {font-size:0.8em}
		.ui-jqgrid-labels {font-size:0.8em }
		.ui-jqgrid-pager{font-size:0.7em !important}

.topics_subtopics{
overflow-y: auto;
height: 480px;
		width: 580px;
}
.topics_subtopics_notes{
overflow-y: auto;
height: 480px;
		width: 207px;
}

.form-row{
	border-bottom:solid 1px #ddd;
	line-height: 2.5em;
	vertical-align: middle;
}

.anchor-style{
	text-decoration:underline;
	cursor:pointer;
	color:blue;
}

.list-link{
	text-decoration:none;
	cursor:pointer;
	color: black;
}
.list-link:hover{
	color: #3399FF  !important;
}
.ui-pg-table,.ui-paging-info{
	font-size:11px !important;
}
.form-error{
	font-size:10pt;
	color:red;
}
.ui-widget {
    font-size: 10pt !important;
}

.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; }
.ui-timepicker-rtl dl dd { margin: 0 65px 10px 10px; }
#footer-wrap {
			position: relative; 
			width: 900px;
			text-align:right;
			margin: 5px auto; 
			padding: 0px;
			color:#000;
			font-size:12px;
			height:15px;
		}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 135px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    padding: 0px;
    margin-top: 40px;
	z-index: 99;
}

.dropdown:hover .dropdown-content {
    display: block;
    list-style: none;
}
.dropdown:hover .dropdown-content li {
	display: block;
	width: 100%;
}
.dropdown:hover .dropdown-content li a {
	display: block;
	width: 100%;
	/*padding: 0px !important;*/
}
.dropdown:hover .dropdown-content li a:hover {
	background: #F6A139;
	background-image: none !important;
}
	</style>
	<title>SCHC SYMB - ians</title>
</head>
<body style="background: none;">
<div class="topbackground" style="top: 35px;">&nbsp;</div>
<div class="page-wrap">
<?php
	require_once("./db.php");
	if($_GET['msg']!="")
	{
		?>
		<div id="dialog-message" title="Message">
			<?=$_GET['msg']?>
		</div>
		<?php
	}
	if($_SESSION['username']!="")
	{
		?>
		
		<div class="yui3-g" style="text-align:right;height: 85px; color: black;">
			<div style="width:55%;float:left;text-align:left;">
				<img src="images/symbi-soc-logo.png" alt="" height="80" style="float: left;">
			</div>
			Hi, <?=$_SESSION['username']?> |  
			<?php
			if($_SESSION['userrole']=="admin")
			{?>
			<a href="change_password.php" style="text-align: right; height: 50px; color: black;">Change Password</a> |  
			<?php
			}?>
			<a href="./index.php?action=signout" class="grid-button-edit yellow-button" style="text-decoration:none;"><span style="padding:10px;">Sign Out</span></a>
		</div>
		
		<div class="menu-holder" style="width:1244px;">
			
		
		<?php
		if($_SESSION['userrole']=="admin")
		{
			?>
			<ul id="menu">
				<li id="menu-home"><a href="admin.php">Home</a></li>
				<li id="menu-batch"><a href="admin_batches.php">Manage Batches</a></li>
				<li id="menu-student"><a href="admin_students.php">Manage Students</a></li>
				<li id="menu-faculty"><a href="admin_faculties.php">Manage Faculties</a></li>
				<li id="menu-subfaculty"><a href="admin_sub_faculties.php">Manage Sub-faculties</a></li>

				<li id="menu-subject"><a href="admin_subjects.php">Manage Programs</a></li>
				<li id="menu-notice"><a href="admin_notices.php">Manage Notice</a></li>
				<li id="menu-report"><a href="admin_reports.php">Report</a></li>
				<!--<li id="menu-password"><a href="change_password.php">Change Password</a></li>-->
			</ul>
						
			<?php
		}
		else if($_SESSION['userrole']=="faculty")
		{
		  //$id= get_First_Course_ID();
	 /** CESAR JUAREZ - OPEN COMET **/
	if ($_GET['selectedCourse']==""){
	  if($_SESSION['courseID'] != "") $_GET['selectedCourse'] = $_SESSION['courseID'];
	} elseif ($_GET['selectedCourse']!=""){
	  $_SESSION['courseID'] = $_GET['selectedCourse'];
	  // echo $_SESSION['courseID'];exit;
	}
			?>
			<ul id="menu">
				<li id="menu-home"><a href="faculty.php">Home</a></li>
				<li id="menu-course"><a href="faculty_course.php">Manage Program</a></li>
				<li id="menu-topic"><a href="faculty_topics.php">Manage Modules</a></li>
				<li id="menu-assignment"><a href="faculty_assignment.php">Manage Assignments</a></li>
				<li id="menu-marks"><a href="faculty_checkmarks.php">Student Marks</a></li>
				<!--<li id="menu-password"><a href="change_password.php">Change Password</a></li>-->
				<li id="menu-sub_faculities"><a href="admin_sub_faculities.php">Manage Sub-Faculities</a></li>
				<li id="menu-modules"><a href="assign_modules.php">Assign Modules</a></li>
				


				<li id="menu" class="dropdown">
				  <span><a href="#">Others</a></span>
				  <ul class="dropdown-content">
				  	<li id="menu-uploadPPT"><a target="_blank" href="uploadPPT.php">Upload PPT</a></li>
				    <li id="menu-uploadVideo"><a target="_blank" href="uploadLectures.php">Upload Lectures</a></li>
                	<li id="menu-uploadTitle"><a target="_blank" href="uploadTitles.php">Upload Titles</a></li>
				  </ul>
				</li>

				
			</ul>

			
			<?php
		}
		else if($_SESSION['userrole']=="student")
		{
			?>
			<ul id="menu" style="margin-left:130px;">
				<!--<li id="menu-home"><a href="student.php">Home</a></li>-->
				<li id="menu-subject"><a href="student_course.php">Programs</a></li>
				<li id="menu-topic"><a href="student_topics.php">Modules</a></li>
				<!--<li id="menu-subtopic"><a href="student_subtopics.php?selectedCourse=0">View Topics</a></li>
				<li id="menu-assignment"><a href="student_assignment.php">Assignments</a></li>
				<li id="menu-marks"><a href="student_marks.php">Marks</a></li>-->
				<!--<li id="menu-queries"><a href="student_queries.php">Queries</a></li>
				<li id="menu-password"><a href="change_password.php">Change Password</a></li>-->
				<li id="menu-contact"><a href="contact_us.php">Contact Us</a></li>
				
			</ul>
			
			<?php
		}
		else if($_SESSION['userrole']=="sub-faculity")
		{
		  //$id= get_First_Course_ID();
	 /** CESAR JUAREZ - OPEN COMET **/
	if ($_GET['selectedCourse']==""){
	  if($_SESSION['courseID'] != "") $_GET['selectedCourse'] = $_SESSION['courseID'];
	} elseif ($_GET['selectedCourse']!=""){
	  $_SESSION['courseID'] = $_GET['selectedCourse'];
	}
			?>
			<ul id="menu">
				<li id="menu-home"><a href="sub_faculty.php">Home</a></li>
				<!-- <li id="menu-course"><a href="faculty_course.php">Manage Program</a></li>
				<li id="menu-topic"><a href="faculty_topics.php">Manage Modules</a></li>
				<li id="menu-assignment"><a href="faculty_assignment.php">Manage Assignments</a></li> -->
				<li id="menu-marks"><a href="subfaculty_checkmarks.php">Student Marks</a></li>
				<!--<li id="menu-password"><a href="change_password.php">Change Password</a></li>-->
				<!-- <li id="menu-marks"><a href="admin_sub_faculities.php">Manage Sub-Faculities</li> -->
			</ul>
			
			<?php
		}
		?>
		
		
			
	</div>
	<?php
	}
		else{
	?>
	<script>
		//alert('Your current session has expired, you would now be redirected to the Login screen.');
		window.location='./index.php';
	</script>
	<?php
	}
?>