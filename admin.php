<?php

	require_once("header.php");

	?>
	<style>
	.yellow-button{font-size:13px}
	</style>

	<div class="yui3-g" style="margin-top:25px;">



			

					<div class="yui3-u-7-24" style="float:left;">

						<div class="box-shadow"  style="width:100%;float:left;">

							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">

								Admin Profile

							</div>

							<div class="yui3-g">

								<div class="yui3-u-1-3" style="float:left;">

								&nbsp;

								</div>

								<div class="yui3-u-2-3" style="float:left;">

									<?=$_SESSION['username']?><br /><?=ucwords($_SESSION['userrole'])?><br />

Full Time<br />

<!--

<a class="list-link" href="./change_password.php">

		  <div class="form-row">Change Password</div>

		</a>-->

								</div>

							</div>

						</div>

						<div class="yui3-g"> &nbsp; </div> 

						<div class="box-shadow"  style="width:47%;float:left;">

							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">

								Batches

							</div>

							<div class="yui3-u" style="float:left;width:100%;text-align:center;padding-bottom:10px;">

								Number of Batches<br /><span class="big-number-text"><?=get_Admin_Batches()?></span><br /><a href="./admin_batches.php" style="text-decoration:none;"><div style="width:80px;" class="grid-button-edit yellow-button">Manage All</div></a>

							</div>

						</div>

						<div  style="width:4%;float:left;"> &nbsp; </div>

						<div class="box-shadow"  style="width:47%;float:left;">

							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">

								Programs

							</div>

							<div class="yui3-u" style="float:left;width:100%;text-align:center;padding-bottom:10px;">

								Number of Programs<br /><span class="big-number-text"><?=get_Admin_Subjects()?></span><br /><a href="./admin_subjects.php" style="text-decoration:none;"><div style="width:80px;" class="grid-button-edit yellow-button">Manage All</div></a>

							</div>

						</div>

						<div class="yui3-g"> &nbsp; </div> 

						<div class="box-shadow"  style="width:47%;float:left;">

							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">

								Faculties

							</div>

							<div class="yui3-u" style="float:left;width:100%;text-align:center;padding-bottom:10px;">

								Number of Faculties<br /><span class="big-number-text"><?=get_Admin_Faculties()?></span><br /><a href="./admin_faculties.php" style="text-decoration:none;"><div style="width:80px;" class="grid-button-edit yellow-button">Manage All</div></a>

							</div>

						</div>

						<div  style="width:4%;float:left;"> &nbsp; </div>

						<div class="box-shadow"  style="width:47%;float:left;">

							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">

								Students

							</div>

							<div class="yui3-u" style="float:left;width:100%;text-align:center;padding-bottom:10px;">

								Number of Students<br /><span class="big-number-text"><?=get_Admin_Students()?></span><br /><a href="./admin_students.php" style="text-decoration:none;"><div style="width:80px;" class="grid-button-edit yellow-button">Manage All</div></a>

							</div>

						</div>

					</div>

					<div width="4%" style="float:left;">

					&nbsp;&nbsp;&nbsp;

					</div>

					<div class="yui3-u-2-5" style="float:left;width:38%;">

						<div class="box-shadow"  style="width:100%;float:left;">

							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">

								Recent Modules Added

							</div>

							<div class="yui3-g">

								<?=get_Recent_Topics_Admin();?>

							</div>

							<div class="yui3-g" style="text-align:center;height:40px;padding-top:10px; width:100px; margin: 0px auto;">

								<a href="./admin_topics.php" target="_blank" style="text-decoration:none;"><div class="grid-button-edit yellow-button" style="float:left;">View All</div></a>

							</div>

							

						</div>

						

						<div class="yui3-g"> &nbsp; </div> 

						

						<div class="box-shadow"  style="width:100%;float:left;">

							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">

								Recent Assignment Added

							</div>

							<div class="yui3-g">

								<?=get_Recent_Assignment_Admin();?>

							</div>

							<div class="yui3-g" style="text-align:center;height:40px;padding-top:10px; width:100px; margin: 0px auto;">

								<a href="./admin_assignment.php" target="_blank" style="text-decoration:none;"><div class="grid-button-edit yellow-button" style="float:left;">View All</div></a>

							</div>

						</div>

						

					</div>

					<div width="2%" style="float:left;">

					&nbsp;&nbsp;&nbsp;

					</div>

					<div class="yui3-u-7-24" style="float:left;">

						<div class="box-shadow"  style="width:100%;float:left;">

							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">

								Notifications

							</div>

							<div class="yui3-g" style="padding:8 px">

<table width="100%">

								<div class="vticker">
<ul>
								<?=printAllNotices_admin();?>
</ul>
</div>

</table>

							</div>

							<!--<div class="yui3-g" style="text-align:center;height:40px;padding-top:10px; width:100px; margin: 0px auto;">

								<a href="./admin_queries.php" target="_blank" style="text-decoration:none;"><div class="grid-button-edit yellow-button" style="float:left;">View All</div></a>

							</div>-->

						</div>

					</div>

			

		</div>

<script type="text/javascript" src="js/jquery.easy-ticker.min.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			
			var dd = $('.vticker').easyTicker({
				direction: 'up',
				speed: 'slow',
				interval: 5000,
				height: 'auto',
				visible: 5,
				mousePause: 0,
				controls: {
					up: '.up',
					down: '.down',
					toggle: '.toggle',
					stopText: 'Stop !!!'
				}
			}).data('easyTicker');
		});
	</script>

		<script>

			$(document).ready(function(){

				$("li#menu-home a").addClass("active");

			});			

		</script>

	<?php

	require_once("footer.php");

?>