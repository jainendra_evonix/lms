<?php
	require_once("header.php");
	require_once("mainFunctions.php");
	require_once("./asd.php");

	if($_GET['action']=="addstudent")
	{
		$strs=mysql_query("select * from user where EmailID='".$_POST['studentemail']."' or UserID = '". $_POST['studentid'] ."' and Role='student'");
		if(mysql_num_rows($strs)>0)
		{
			?>
			<script>
				window.location='./admin_students.php?msg=Student email address or StudentID previously exists, insert aborted.';
			</script>
			<?php
		}
		else
		{
			if($_POST['studentenabled']!='1')
				$_POST['studentenabled']='0';
			
			mysql_query("insert into user (Name,Gender,EmailID,ContactNo,Enabled,Password,Role,UserID) values('".$_POST['studentname']."','".$_POST['studentgender']."','".$_POST['studentemail']."','".$_POST['studentcontact']."',".$_POST['studentenabled'].",'".$_POST['studentid'].$_POST['studentemail']."','student','".$_POST['studentid']."')") or die(mysql_error());
			$stdid=mysql_insert_id();
			
			if(count($_POST['studentbatch'])>0)
			{
				foreach($_POST['studentbatch'] as $sel)
				{
					mysql_query("insert into batches_student(BatchID,StudentID) values(".$sel.",".$stdid.")");
				}
			}
			?>
			<script>
				window.location='./admin_students.php';
			</script>
			<?php
		}
	}
	
	?>
	<script>
var id_std;		
		function loadData()
		{
			jQuery("#list2").jqGrid({
				url:'admin_handler.php?action=get_student_all',
				datatype: "json",
				  colNames:['ID','UserID','Name', 'Gender', 'Email Id','Contact Number'],
				colModel:[
						  {name:'id',index:'id', width:30, hidden:true},
						  {name:'UserID',index:'UserID', width:30}, /** CESAR JUAREZ - OPEN COMET **/
							{name:'Name',index:'Name', width:100},
							{name:'Gender',index:'Gender', width:50},
							{name:'EmailID',index:'EmailID', width:100, align:"right"},
							{name:'ContactNo',index:'ContactNo', width:80, align:"right"},		
						],
				rowNum:50, /** CESAR JUAREZ - OPEN COMET **/
				rowList:[50,100,200],
				pager: '#pager2',
				  sortname: 'id', /** CESAR JUAREZ **/
				viewrecords: true,
				sortorder: "desc",
				multiselect: false,
				altRows: true,
				  height: 750,	/** CESAR JUAREZ - OPEN COMET **/
				width: 700,
				rownumbers: false,
				  rownumWidth: 40,
				//caption:"List of students",
				onSelectRow: function (id) 
				{
					//$("#list2").jqGrid('resetSelection');
					//$("#list2").setSelection (id, true);
					$.ajax({
						url: 'admin_handler.php?action=getBatchInformation&studentID='+id,
						success: function(data) {
							var info = data.split(",");
							$("#name").val(info[0]);
							$("#studentid").val(info[5]); /** CESAR JUAREZ - OPEN COMET **/
							$("#email").val(info[0]);
							if(info[1]=="male")
							{
								$("#genderm").prop('checked', true);
							}
							else
							{
								$("#genderf").prop('checked', true);
							}
							$("#email").val(info[2]);
							$("#contact").val(info[3]);
							if(info[4]=="1")
							{
								$("#enabledt").prop('checked', true);
							}
							else
							{
								$("#enabledt").prop('checked', false);
							}
							$("#batch").val(info[5].split('|'));
							$("#stdid").val(id);
							$("#newform").hide();
							$("#editform").show();
							save_id_student(id);
						}
					});					
				},
			});
			jQuery("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false});			
		}

function loadAssignments()
{
  $.ajax({
	url: 'faculty_handler.php?action=getCourseAssignment&courseid='+$("#course").val(),
		success: function(data) {
					
					$("#selAssignment").html(data);
	  }
	});

}
function loadBatchesAssigned()
{
  $.ajax({
	url: 'faculty_handler.php?action=getBatchesAssigned&assignmentid='+$("#selAssignment").val(),
		success: function(data) {
					//alert(data);
					$("#selBatch").html(data);
	  }
	});

}
function loadBatchesAssignedResult(a)
{
//alert(a);
  $.ajax({
	url: 'faculty_handler.php?action=getBatchesAssignedResult&assignmentid='+a,
		success: function(data) {
					//alert(data);
					$("#selBatchResult").html(data);
	  }
	});

}

function loadBatchesAssignedResultSubFaculty(a){

 $.ajax({
	url: 'faculty_handler.php?action=getBatchesAssignedResultSubFaculty&assignmentid='+a,
		success: function(data) {
					//alert(data);
					$("#selBatchResult").html(data);
	  }
	});

}
function getBatchMarks()
{
  $.ajax({
	url: 'faculty_handler.php?action=getBatchMarks&assignmentid='+$("#selAssignment").val()+'&batchid='+$("#selBatch").val(),
		success: function(data) {
					//alert(data);
					$("#divShowSubFaculities").html(data);
	  }
	});

}
function getSubFaculitiesResult(a,b)
{

  $.ajax({
	url: 'faculty_handler.php?action=getSubFaculties&assignmentid='+a+'&batchid='+b,
		success: function(data) {
					//alert(data);
					$("#divShowSubFaculities").html(data);
	  }
	});

}
function getBatchMarksResultFilter(a,b)
{
//alert(b);
//alert($("#studmarks").val());
  $.ajax({
	url: 'faculty_handler.php?action=getBatchMarksResultFilter&assignmentid='+a+'&batchid='+b+'&studid='+$("#studid").val()+'&studname='+$("#studname").val()+'&studmarks='+$("#studmarks").val()+'&studstatus='+$("#studstatus").val(),
		success: function(data) {
					//alert(data);
					$("#divShowStudentMarks").html(data);
	  }
	});

}

function insertSubFacultiesData(a,b){
var id=$("#studstatus").val();

$.ajax({
	url: 'faculty_handler.php?action=insertSubFacultiesDetails&assignmentid='+a+'&batchid='+b+ '&subFacultyId='+id,
	type:'POST',
		success: function(data) {
					//alert(data);
					alert("Sub-Faculty saved successfully");
	  }
	});


}
function submitBatchMarks(studentid,a,b)
{
  //var asd = $("#studmarks_"+studentid).val();
  //alert(asd);
  $.ajax({
	url: 'faculty_handler.php?action=submitBatchMarks&assignmentid='+a+'&batchid='+b+'&studid='+studentid+'&studmarks='+$("#studmarks_"+studentid).val(),
		success: function(data) {
		//alert(data);
					if(data){ alert("Marks Updated"); $('#btnFilter').attr("disabled", true); }
					else{ alert("Error saving marks, please try again."); }
					
				}
	});
}
function submitSubmissionMode(studentid,a,b)
{
  //var asd = $("#mos_"+studentid).val();
  //alert(asd);
  $.ajax({
	url: 'faculty_handler.php?action=submitSubmissionMode&assignmentid='+a+'&batchid='+b+'&studid='+studentid+'&mosvalue='+$("#mos_"+studentid).val(),
data: { }, //your form data to post goes here
		success: function(data) {
		//alert(data);
$('#mossubmit').prop('disabled', true);
					if(data!=1){ alert("Mode of Submission Updated"); }
					else{ alert("Processing..."); }
					
				}
	});
}
function getStudentMarkDetails(assgnid,studentid)
{
  $.ajax({
	url: 'faculty_handler.php?action=getStudentMarkDetails&assignmentid='+assgnid+'&studentid='+studentid,
		success: function(data) {
					alert(data);
					
	  }
	});

}
$(document).ready(function(){
	$("#btnFilter").bind('click',function(){
		getBatchMarks();
	});
});

$(document).ready(function(){
	$(".btnFilterResult").bind('click',function(){
	    //alert('');
		//loadBatchesAssignedResult();
	});
});
</script>

	<div class="yui3-g" style="margin-top:25px;width: 1200px;">
		<div class="box-shadow"  style="width:25%;margin-left:-120px;float:left;">
				<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
					Filter Student Marks
				</div>
				<!--<div class="yui3-g" id="newform">
					
					<table width="100%">
						<tr class="ui-widget-content">
							<td style="vertical-align:middle; padding:7px 20px;font-weight:bold;">Program<br/>
								<select name="course" id="course" title="Choose Program" style="width:200px" onchange="loadAssignments();" >
									<option value="-1">Choose Program</option>
									<?php //get_Courses_Select(); ?>
								</select>
							</td>
						</tr>
						<tr class="ui-widget-content">
							<td style="vertical-align:middle; padding:7px 20px;font-weight:bold;">Assignment<br/>
								<select name="assignment" id="selAssignment" title="Choose Program" style="width:200px" onchange="loadBatchesAssigned();" >
									<option value="-1">Choose Program</option>
									
								</select>
							</td>
						</tr>
						<tr class="ui-widget-content">
							<td style="vertical-align:middle; padding:7px 20px;font-weight:bold;">Batch<br/>
								<select name="batch" id="selBatch" title="Choose Batch" style="width:200px" >
									<option value="-1">Choose Batch</option>
									
								</select>
							</td>
						</tr>
						<tr class="ui-widget-content">
							<td colspan="2" style="vertical-align:middle; padding:7px 20px; text-align:center;"><input id="btnFilter" type="button" value="Filter" class="grid-button-edit green-button" style="height:30px; width:100px !important;cursor:pointer;"/></td>
						</tr>
					</table>

				</div>-->
				<!--<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid; color:#000000">
					Assignments
				</div>-->
				<table width="100%">
						<?=get_All_Assignments_Sub_Faculty()?>
				</table>
				
				<!-- CESAR JUAREZ : OPEN COMET -->
		</div>	
			<div  style="width:2%;float:left;"> &nbsp; </div>
		
		<div class="box-shadow"  style="width: 77%;float:left;">
			<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
				Student Marks
			</div>
			
			<div id="selBatchResult"></div>
				
			<div id="divShowSubFaculities" class="yui3-g" style="margin:15px 5px 10px 15px;min-height:250px;">
				
			</div>

				

		</div>
	</div>

	<script>
		$(document).ready(function(){
			$("li#menu-modules a").addClass("active");
		});			
	</script>
	<?php
	require_once("footer.php");
?>
