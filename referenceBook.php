    <?php include "eportfoli_header.php"; ?>  
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    
    <style type="text/css">
    .liststyle li{  list-style: none;
  line-height:33px;
  font-size: 14px;
  color: #333;}
  
    .liststyle {  margin: 0;
  padding: 0;
  font-family: roboto;}
  
  .ui-accordion .ui-accordion-icons {
  padding-left: 2.2em;
  color: rgb(74, 39, 39);
  font-weight: bold;
  font-size: 14px;
}

#ui-id-2{ height:400px !important}
 
  #accordion-resizer {
    padding: 10px;
    width: 300px;
    height:150px;
  }




    </style>

  <link rel="stylesheet" href="/resources/demos/style.css">
       
    
         <div id="content" class="content">
            <!-- begin breadcrumb -->
            <!-- end breadcrumb -->
            <!-- begin page-header -->
            <h1 class="page-header">Reference Books </h1>
            <!-- end page-header -->
            <!-- begin row -->
            <div class="row">
               <!-- begin col-6 -->
               <div class="col-md-12 " style="min-height:500px;background: #fff;  border-radius: 3px;">
                  
       
               
                 
                 <div id="accordion">
  <h3>Post Graduate Diploma in Health Insurance Management </h3>
  <div>

    
    <ul class="liststyle">
   <li> 1. Dr. L. P. Gupta. (2015). General Insurance Guide - Handbook of General Insurance Policies and Claims.
    Self-Published.</li>
 <li> 2. Dr. L. P. Gupta. (2014). Health Insurance for Rich and Poor in India. Self-Published.</li>
 
 <li> 3. Dr. L. P. Gupta. (2014) India Insurance Guide-Handbook of Insurance Policies, Claims and Law. Self-Published.</li>
 
 <li> 4. Dr. L. P. Gupta. (2013) Insurance Claims Solutions-A Guide to Life and General Insurance Claims. Self-Published.</li>
 
<li> 5. N. E. Coe , M. E. Ogborn.  (2013). The Practice of Life Assurance.</li>
<li> 7. “Fundamentals of Insurance”, Study Course CPAIM-BL-01, Insurance Institute of India Publication. (All Modules)</li>

<li> 8. S Balachandran. (2010). Managing Change. Mumbai:  Sangeeta Associates. (All Modules)</li>

<li> 9. C Gopalakrishna. (2011). Social Security, Insurance and the Law. Mumbai: Shroff Publishers and Distributors.
 (All Modules)</li>
 

<li> 10. M N Mishra. (1982). Insurance: Principles and Practice (English) 18th Edition.  S. Chand. </li>

<li> 11. Paul Hopkin. (2013). Risk Management. New Delhi: Viva Books Pvt Ltd. </li>
    
     </ul>
    
    
    
  </div>
  <h3>Post Graduate Diploma in Hospital & Healthcare Management </h3>
  <div>
   
    
    <ul class="liststyle">
    
    <li>   1. K Aswathappa. (2005). Human Resource and Personnel Management.  Tata Mc-graw Hill Education. </li>
       
    <li>  2. Gupta Joydeep Das.  (2009). Hospital Administration and Management—A Comprehensive Guide. Jaypee Publication.</li>

  <li>  3. Yashpal Sharma, R K Sarma, Libert Anil Gomes. (2013).Hospital Administration Principles and Practice.</li>

 <li>  4. P Gopalakrishnan, M Sundaresan. (1977). Materials Management: An Integrated Approach. Phi Learning Pvt. Ltd. </li>
 <li>  5. Peter F. Drucker. (1993). Management: Tasks, Responsibilities and Practices. </li>
 <li>  6. Purnima Sharma, Sangeet Sharma. (2007). Step by Step Hospital Designing and Planning. Jaypee Publication.</li>
 <li>  7. S. L. Goel.  ( 2002). Management of hospitals: hospital administration in the 21st century. Vol.1. Deep & Deep Publications.</li>

 <li>  8. S. L. Goel, R Kumar (Eds.). (2002). Management of Hospitals - Hospital Administration in the 21st century. Vol.4. Delhi Deep & Deep Publications. </li>

 <li>  9. G. D. Kunders, S. Gopinath, Asoka Katakam. (1997). Hospitals: Planning, Design and Management.  Tata McGraw-Hill Publishing Company.</li>
 
 <li> 10. B M Sakharkar. (2009).  Principles of Hospital Administration and Planning. Jaypee Brothers Publishers.</li>

 <li>  11. Sanskriti Sharma. (2002) Hospital Waste Management and Its Monitoring . </li>
  <li>  12. Dr. Ashok Sahni. (2004). Hospital Clinical Waste, Hazards Management and Infection Control. Indian Society of Health Administrators. </li>
  
<li>13. Himanshu Sekhar Rout, Prasant Kumar Panda. (2007). Health Economics in India.
New Century Publications.</li>

 <li>14. Biswajeet Pattanayak. (2005). Human Resource Management.  Prentice Hall of India (PHI Learning).	</li>

<li> 15. P.N. Blanchard & J.W. Thacker.  Effective Training: Systems, Strategies and Practices by Englewood Cliffs.
  NJ: Prentice Hall.</li>


<li>16. R.S. Dwivedi. Published A Textbook of Human Resource Management. Vikas Publication House Pvt Ltd.</li>

<li> 17. Margie Parikh & Rajen Gupta. (2010). Organizational Behaviour. McGraw Hill Education (India) Private Limited.</li>

<li> 18. R S Dwivedi. (2010). Human Relations and Organizational Behaviour.  Macmillan Publishers India.</li>


<li> 19. Stephen P. Robbins. (2007). Organizational Behaviour. Pearson/Prentice Hall.</li>
 <li> 20. S. A. Kelkar.  (2007). Management Information Systems - A Concise Study.</li>
 <li> 21. L. M. Prasad. Principles and Practice of Management. New Delhi: Sultan Chand and sons Publisher .</li>

  <li> 22. P. C. Shejwalkar & Ghanekar & Bhjvpathaki D. Principles and Practice of Management. Pune: Everest Publishing house.
  </li>



 
      </ul>
   
   
  </div>
  <h3>Post Graduate Diploma in Clinical Research </h3>
  
  <div >
    
    <ul class="liststyle">
    
     <li>  1. Principles and Practice of Clinical Research - Edited by - John Gallin, Frederick Ognibene</li>

       <li> 2. Fundamentals of Clinical Trials - Lawrence M., Friedman, Curt D. Furberg, David L. DeMets</li>

      <li>  3. Introduction to Statistical Methods for Clinical Trials -  Thomas D. Cook, David L DeMets</li>
      
   <li>  4. Clinical Research Made Easy: A Guide to Publishing in Medical Literature - Mohit Bhandari, Parag Sancheti</li>

<li> 5. Basic Principles of Clinical Research and Methodology - SK Gupta</li>

<li> 6. Designing Clinical Research: An Epidemiologic Approach - Stephen B. Hulley, Steven R. Cummings, Warren S. Browner, Deborah Grady, Norman Hearst, Thomas B. Newman </li>

<li> 7. Clinical Trials: A Practical Guide to Design, Analysis, and Reporting - Duolao Wang, Ameet Bakhai</li>

<li> 8. Fundamentals of  Clinical Research – Antonella Bacchieri, Giovanni Della Cioppa</li>

<li> 9. Essentials Of Clinical Research -  Ravindra B. Ghooi, Sachin Itkar</li>

<li> 10. Ethical Issues in Clinical Research: A Practical Guide - Bernard Lo</li>

<li>11. Clinical Research: Practice and Prospects – T.K. Pal </li>

<li> 12. The Oxford Textbook of Clinical Research Ethics - Emanuel, Ezekiel J</li>

<li>13. Writing Clinical Research Protocols : Ethical Considerations - DeRenzo, Evan G. Moss, Joel</li>

<li> 14. Publishing and Presenting Clinical Research - Warren S. Browner</li>

<li>15. Sample Size Calculations in Clinical Research - Shein-Chung Chow, Hansheng Wang, Jun Sha</li>

<li>16. Clinical Research: What it is and how it Works - Jones & Bartlett Learning</li>

<li>17. A Concise Guide to Clinical Trials - Allan Hackshaw</li>

<li>18. Essentials of Clinical Research - Stephen P. Glasser</li>

<li>19. The CRC's Guide to Coordinating Clinical Research - Karen E. Woodin</li>

<li> 20. Clinical Research: From Proposal to Implementation - Robert D. Toto, Michael J. McPhau</li>

<li> 21. Clinical Research and the Law - Patricia M. Tereskerz</li>

<li> 22. The Handbook of Clinical Trials and Other Research - Alan Earl-Slater</li>

<li> 23. Evaluating Clinical Research: All that glitters is not gold - Bengt D. Furberg, Curt D. Furberg</li>

<li>24. Drug Discovery and Clinical Research - SK Gupta</li>

<li> 25.  Understanding Clinical Research - Renato D. Lopes, Robert A. Harrington</li>

<li>26. Core Resources for Clinical Research: A Guide to Information - Helena Korjonen-Close</li>

<li> 27. Writing Clinical Research Protocols: Ethical Considerations - Evan DeRenzo, Joel Mos</li>

<li> 28. Ethics in Clinical Research - Jane Barrett</li>

<li> 29. Statistics in Clinical Research - Trish Parry</li>

<li> 30. Principles and Practice of Clinical Trial Medicine - Richard Chin, Bruce Y Lee</li>

<li> 31. A Guide to Patient Recruitment and Retention - Thomson CenterWatch</li>

<li>32. Clinical Trials: Design, Conduct, and Analysis - Curtis L. Meinert</li>

<li> 33. Research Methodology Simplified: Every Clinician a Researcher - Mahendra N. Parikh, Avijit Hazra, Joydev Mukherjee, Nithya Gogtay</li>

<li> 34. The CRA's Guide to Monitoring Clinical Research - Karen E. Woodin, John C. Schneider</li>

<li> 35. Clinical Trials: What You Should Know Before Volunteering to Be a Research Subject - J. Joseph Giffels </li>

      
      
      
    
    </ul>    
    
  </div>
  
</div>
                 
                 
                     
                  </div>
               </div>
               <!-- end col-6 -->
               <!-- begin col-6 -->
               <!-- end col-6 -->
            </div>
            <!-- end row -->
         </div>
         <!-- end #content -->
         <!-- begin theme-panel -->
         <!-- end theme-panel -->
         <!-- begin scroll to top btn -->
         <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
         <!-- end scroll to top btn -->
      </div>
      <!-- end page container -->
      <!-- ================== BEGIN BASE JS ================== -->
      <script src="jsep/jquery-1.9.1.min.js"></script>
      <script src="jsep/jquery-migrate-1.1.0.min.js"></script>
      <script src="jsep/jquery-ui.min.js"></script>
      <script src="jsep/bootstrap.min.js"></script>
      <!--[if lt IE 9]>
      <script src="assets/crossbrowserjs/html5shiv.js"></script>
      <script src="assets/crossbrowserjs/respond.min.js"></script>
      <script src="assets/crossbrowserjs/excanvas.min.js"></script>
      <![endif]-->
      <script src="jsep/jquery.slimscroll.min.js"></script>
      <script src="jsep/jquery.cookie.js"></script>
      
      
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      
      <!-- ================== END BASE JS ================== -->
      <!-- ================== BEGIN PAGE LEVEL JS ================== -->
      <script src="jsep/apps.min.js"></script>
      
       <script>
  $(function() {
    var icons = {
      header: "ui-icon-circle-arrow-e",
      activeHeader: "ui-icon-circle-arrow-s"
    };
    $( "#accordion" ).accordion({
      icons: icons,
        collapsible: true,
        heightStyle: "fill"
    });
    $( "#toggle" ).button().click(function() {
      if ( $( "#accordion" ).accordion( "option", "icons" ) ) {
        $( "#accordion" ).accordion( "option", "icons", null );
      } else {
        $( "#accordion" ).accordion( "option", "icons", icons );
      }
    });
     $("#accordion-resizer").resizable({
      minHeight: 140,
      minWidth: 200,
      resize: function() {
        $( "#accordion" ).accordion( "refresh" );
      }
    });
    
  });
  </script>
      <!-- ================== END PAGE LEVEL JS ================== -->
      <script>
         $(document).ready(function() {
         	App.init();
         });
      </script>
   </body>
</html>