<?php
	require_once("headerassign.php");
	require_once("mainFunctions.php");

if(!$_SESSION['login']){?>
		<script>
			window.location='./index.php';
		</script>
<?php
			}
	?>

<?php

	 /** CESAR JUAREZ - OPEN COMET **/

	if ($_GET['selectedCourse']==""){
	  if($_SESSION['courseID'] != "") $_GET['selectedCourse'] = $_SESSION['courseID'];
	} elseif ($_GET['selectedCourse']!=""){
	  $_SESSION['courseID'] = $_GET['selectedCourse'];
	}
?>


	<script>
		function loadAllAssignments()
		{
			$.ajax({
				url: 'student_handler.php?action=getAssignmentForMenu&selectedCourse=<?=$_GET['selectedCourse']?>',
				success: function(data) {
					$('#leftmenu').html(data);
				}
			});
		}
		function loadTopicAssignments(topicid)
		{
			$.ajax({
				url: 'student_handler.php?action=getAssignmentForMenu&selectedCourse=<?=$_GET['selectedCourse']?>-'+topicid,
				success: function(data) {
					$('#leftmenu').html(data);
				}
			});
		}
		function setAssignment(assid)
		{
			$.ajax({
				url: 'student_handler.php?action=getAssignment&assid='+assid,
				success: function(data) {
					$('#mainform').html(data);
				}
			});
		}
		function startTest(assid)
		{
			$.ajax({
				url: 'student_handler.php?action=getQestion&assid='+assid,
				success: function(data) {
					$('#data').html(data);
				}
			});			
		}
		function saveQuestion(assid,questionid)
		{
			var op="";
			var tID=$('#totalID').val();
			
			for(i=1;i<tID;i++)
			{
				iid="#option"+i;
				if(iid!="")
				{
					if($(iid).is(':checked'))
					{
						op+=$(iid).val()+",";
					}	
				}
			}
			
			if(op=="")
			{
				alert("Please select option to submit question");
				return;
			}
			
			$.ajax({
				url: 'student_handler.php?action=saveAnswer&assid='+assid+'&questionid='+questionid+'&options='+op,
				success: function(data) {
					startTest(assid);
				}
			});
		}
	</script>	<!-- for collapsible pane on the left of the screen -->
	<!--<nav>
		<ul>
			<?php //getAllCoursesList("student_assignment.php"); ?>
		</ul>
	</nav>-->
	<div style="display:none"><div id="data"></div></div>
	<?php
		if($_GET['selectedCourse']=="")
	{
		?>
		
		<div class="yui3-g" style="margin-top:25px;">
		<div class="box-shadow"  style="width:75%;margin:0px auto;">
			<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
				<!--Please select a course from the collapsible pane on the left of the screen.-->				Program List
			</div>						<div class="yui3-g">										<table width="100%">						<?php getAllCoursesListInDiv("student_assignment.php"); ?>					</table>									</div>
		</div>
		</div>
		<?php
	}
	else
	{
	?>
	<div class="yui3-g" style="margin-top:25px;">
		<div id="leftmenu" class="yui3-u-1-5 box-shadow"  style="float:left;">
		</div>
		
		<div  style="width:2%;float:left;"> &nbsp; </div>
		
		<div id="mainform" class="box-shadow"  style="width:75%;float:left;">
			<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
				<?php get_Subject_Name($_SESSION['courseID']) ?> Assignment Details								<a href="unsetassigndata.php" class="grid-button-edit yellow-button" style="float: right; font-size: 15px; cursor: pointer; text-decoration:none;"><span style="padding:10px;">Click Here To Change Program</span></a>
			</div>
			<div class="yui3-g" class="box-header" style="height:450px;overflow:auto;">
				<div class="box-header" style="height:150px">
					Click on the Assignment list on the left to load Assignment details here.
				</div>
			</div>
		</div>	
	</div>
	<?php
	if(!isset($_GET['topicid']))
	{
	?>
		<script>
			loadAllAssignments();
		</script>
	<?php 
	}
	else if(isset($_GET['topicid']))
	{
	?>
		<script>
			loadTopicAssignments(<?=$_GET['topicid'] ?>);
		</script>
	<?php 
	}
	?>
	<?php
	}?>	<script>			$(document).ready(function(){				$("li#menu-assignment a").addClass("active");			});					</script><?php
	require_once("footer.php");
?>