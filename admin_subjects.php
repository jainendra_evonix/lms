<?php
	require_once("header.php");
	require_once("mainFunctions.php");
	if($_GET['action']=="createcourse")
	{
		$rs=mysql_query("select * from course where CourseName='".$_POST['courseName']."'");
		if(mysql_num_rows($rs)>0)
		{
			?>
			<script>
				window.location='./admin_subjects.php?msg=Program Name previously exists, insertion aborted';
			</script>
			<?php
		}
		else
		{
			$enabled=$_POST['enabled'];
			if($enabled=="")
			{
				$enabled="0";
			}
			mysql_query("insert into course(CourseName, Enabled) values('".$_POST['courseName']."','".$enabled."')") or die(mysql_error());
			$_SESSION['courseid']=mysql_insert_id();
			?>
			<script>
				window.location='./admin_subjects.php?response=coursecreated';
			</script>
			<?php
		}
	}
	else if($_GET['action']=="setcourse")
	{
		$_SESSION['courseid']=$_GET['id'];
		?>
		<script>
			window.location='./admin_subjects.php?response=coursecreated';
		</script>
		<?php
	}

	else if($_GET['action']=="setsubfaculty")
	{
		$_SESSION['courseid']=$_GET['id'];
		?>
		<script>
			window.location='./admin_subjects.php?response=subFacultycreated';
		</script>
		<?php
	}
	?>
	<div class="yui3-g" style="margin-top:25px;">
		<div class="yui3-u-1-5 box-shadow"  style="float:left;">
			<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
				Program List
			</div>
			<div class="yui3-g" style="height:420px;overflow:auto;">
				<table width="100%">
					<?=get_All_Courses_Admin()?>
				</table>
			</div>
			<div class="yui3-g" style="height:28px;overflow:auto;">
				<a href="admin_subjects.php" style="text-decoration:none;">
					<div class="grid-button-edit yellow-button" style="text-align: center;">
						Add Program
					</div>
				</a>
			</div>
		</div>
		<div  style="width:2%;float:left;"> &nbsp; </div>

		<div class="box-shadow"  style="width:75%;float:left;">
			<?php
			if($_GET['response']=="")
			{
				?>		
				<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
					Create New Program
				</div>
				<div id="newForm" style="margin:10px 0px; height:430px;">
					<form id="form" name="form" action="./admin_subjects.php?action=createcourse" method="post">
					
					<table width="100%" style="line-height:2em;">
						<tr style="border-bottom:solid 1px #ddd">
							<td style="width:150px;text-align:right; vertical-align:middle; padding:5px;">Program Name:</td>
							<td style="vertical-align:middle; padding:5px;"><input type="text" style="width:300px;height:30px" name="courseName" id="courseName" title="Enter Program Name"></td>
						</tr>
						<tr>
							<td style="width:150px;text-align:right; vertical-align:middle; padding:5px;">Enabled:</td>
							<td style="vertical-align:middle; padding:5px;"><input type="checkbox"  style="width:20px;height:20px" name="enabled" id="enabled" title="Check if you want this Program to be enabled" value="1"></td>
						</tr>
						<tr>
							<td></td>
							<td>
								<input type="submit" value="Create Program" class="grid-button-edit green-button" style="height:30px; width:100px !important;cursor:pointer;">
							</td>
						</tr>
					</table>
					</form>
				</div>
				<script>
					$("#form").validate({
					rules: {
						courseName: "required"
					},
					messages: {
						courseName: "Please enter your programname",
					}
				});
				</script>
				<?php
			}
			else if($_GET['response']=="coursecreated")
			{
				$r=mysql_query("select * from course where ID=".$_SESSION['courseid']) or die(mysql_error());
				$r=mysql_fetch_array($r);
				?>	
				<script>
					var loaded=0;
					function loadData()
					{
						jQuery("#list2").jqGrid({
							url:'admin_handler.php?action=get_faculty_course',
							datatype: "json",
							colNames:['Faculty ID','Name', 'Gender', 'Email Address','Contact Number'],
							colModel:[
										{name:'id',index:'id', width:30},
										{name:'Name',index:'Name', width:100},
										{name:'Gender',index:'Gender', width:50},
										{name:'EmailID',index:'EmailID', width:100, align:"right"},
										{name:'ContactNo',index:'ContactNo', width:80, align:"right"},		
									],
							rowNum:10,
							rowList:[10,20,30],
							pager: '#pager2',
							sortname: 'id',
							viewrecords: true,
							sortorder: "desc",
							multiselect: true,
							altRows: true,	
							width: 700,
							rownumbers: false,
							rownumWidth: 40,
							//caption:"List of Faculties",
							loadComplete: function(data) {
								var userdata = $("#list2").getGridParam('userData');
								if(userdata.selID!=null)
								{
									var spl=userdata.selID.split("|");
									
									for(i=0;i<spl.length;i++)
									{
										if(spl[i]!="")
										{
											jQuery("#list2").setSelection (spl[i], true);
										}
									}
								}
								loaded=1;
							},
							onSelectRow: function (id) 
							{
								if(loaded!=0)
								{
									$.ajax({
										url: 'admin_handler.php?action=updateCourseFaculty&value='+id,
									});
								}
							},
						});
						jQuery("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false});			
					}
					function updateCourseName()
					{
						if($('#courseName').val()=="")
						{
							return;
						}
						$.ajax({
						  url: 'admin_handler.php?action=updateCourseName&value='+$('#courseName').val(),
						});
					}
					function updateCourseEnabled()
					{
						$.ajax({
						  url: 'admin_handler.php?action=updateCourseEnabled&value='+$('#enabled').is(':checked'),
						});
					}
					
				</script>
				<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
								Manage Programs
							</div>
							<div id="editForm" style="margin:10px 0px; height:430px;">
								
								<table width="100%" style="line-height:2em;">
									<tr style="border-bottom:solid 1px #ddd">
										<td style="width:150px;text-align:right; vertical-align:middle; padding:5px;">Program Name:</td>
										<td style="vertical-align:middle; padding:5px;"><input type="text" style="width:300px;height:30px;font-size:14px;" name="courseName" id="courseName" title="Enter Program Name" value="<?=$r['CourseName']?>" onBlur="updateCourseName();"></td>
									</tr>


									<tr style="border-bottom:solid 1px #ddd">
										<td style="width:150px;text-align:right; vertical-align:middle; padding:5px;">Enabled:</td>
										<td style="vertical-align:middle; padding:5px;"><input type="checkbox" name="enabled" id="enabled" <?php if ($r['Enabled']=="1") print("checked='checked'"); ?> value="1"  title="Check if you want this Program to be enabled" onChange="updateCourseEnabled();">
										</td>
									</tr>
									<!-- <tr style="border-bottom:solid 1px #ddd">
										<td style="width:150px;text-align:right; vertical-align:top; padding:5px;">
											Faculties Handling the program:
										</td>
										<td style="vertical-align:middle; padding:5px;font-size:11pt;line-height:1.2em !important;">Please select faculties to be added for the program from the list below. A program can be handled by more than one faculty
										</td>
									</tr>
									<tr>
										<td colspan="2" style="padding:20px;">
											<table id="list2"></table>
											<div id="pager2"></div>
										</td>
									</tr> -->
									<!--<tr>
										<td></td>
										<td>
											<input type="submit" value="Create Batch" class="grid-button-edit green-button" style="height:30px; width:100px !important;cursor:pointer;">
										</td>
									</tr>-->
									<tr>
										<td colspan="2" style="padding-top:5px;">
											<a href="./admin_subjects.php?action=setcourse&id=<?=$r['ID']?>" style="text-decoration:none;"><div class="grid-button-edit yellow-button" style="width:70px;text-align: center;">Save</div></a>
										</td><!-- CESAR JUAREZ : OPEN COMET -->
										
									</tr>
								</table>
							</div>
				<script>
					loadData();
				</script>
				<?php
			}
			?>
		</div>
	</div>		
				
				
				
	<script>
		$(document).ready(function(){
			$("li#menu-subject a").addClass("active");
		});			
	</script>
	<?php
	require_once("footer.php");
?>