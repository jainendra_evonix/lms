<?php
	require_once("header.php");
	require_once("mainFunctions.php");

if(!$_SESSION['login']){?>
		<script>
			window.location='./index.php';
		</script>
<?php
			}
	?>
		
	<div class="yui3-g" style="margin-top:25px;">

			
					<div class="yui3-u-7-24" style="float:left;"> <!-- BLOQUE DE LA IZQUIERDA -->
						<div class="box-shadow"  style="width:100%;float:left;">
							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
								Student Profile
							</div>
							<div class="yui3-g">
								<div class="yui3-u-1-3" style="float:left;">
								&nbsp;
								</div>
								<div class="yui3-u-2-3" style="float:left;">
									<?=$_SESSION['username']?><br /><?=ucwords($_SESSION['userrole'])?><br /><?=$_SESSION['batchinfo']?>
								</div>
							</div>
						</div> <!-- primer bloque de codigo STUDENT PROFILE -->

						<div class="yui3-g"> &nbsp; </div> <!-- SEPARADOR -->

						<div class="box-shadow"  style="width:100%;float:left;">
							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
								Programs
							</div>
							<div class="yui3-g" style="height: 355px;overflow: auto;">
								<?=get_Student_Subjects()?>
							</div>
						</div> <!-- SEGUNDO bloque de codigo SUBJECTS -->
					</div> <!-- FIN DEL BLOQUE DE LA IZQUIERDA -->


					<div width="4%" style="float:left;">
					&nbsp;&nbsp;&nbsp;
					</div> <!-- SEPARADOR DE PARA SALTAR A LA COLUMNA DE LA IZQUIERDA -->


					<div class="yui3-u-2-5" style="float:left;width:38%;"> <!-- COLUMNA CENTRAL -->
						<div class="box-shadow"  style="width:100%;float:left;">
							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
								Progress Snapshot
							</div>
							<div class="yui3-g" style="padding: 0 10px;">
								<?=get_Student_Course_Subjects_Index();?><!-- Cargando detalle de courses subjects-->
							</div>					
						</div>
						
						<div class="yui3-g"> &nbsp; </div> <!-- salto de linea para la segunda columna central -->
						
						<div class="box-shadow"  style="width:100%;float:left;"> <!-- CARGANDO DETALLES DE ASSIGNMENTS SUBMMITED-->
							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
								Recent Submitted Assignments
							</div>
							<div class="yui3-g">
								<?=get_Student_Submitted_Assignements();?>
							</div>
						</div>
						
					</div>

					<div width="2%" style="float:left;"> <!-- salto para la columna de la derecha -->
					&nbsp;&nbsp;&nbsp;

					</div>
					<div class="yui3-u-7-24" style="float:left;">
						<div class="box-shadow"  style="width:100%;float:left;">
							<div class="yui3-g box-header" style="border-bottom: #ddd 1px solid;">
								Notifications
							</div>
							<div class="yui3-g" style="height:488px; font-size:14pt;color: red;text-align:center;">
							No new topic added recently
								  <!-- commented to hide notifications <?= notificationStudentAssignment();?> --> <!-- CESAR JUAREZ - OPEN COMET -->
<br />
								 <!-- commented to hide notifications  <?= notificationStudentSubject();?> --><!-- CESAR JUAREZ - OPEN COMET -->
							</div>
						</div>
					</div>
			
		</div>
<script>
			$(document).ready(function(){
				$("li#menu-home a").addClass("active");
			});			
		</script>
	<?php
	require_once("footer.php");
?>